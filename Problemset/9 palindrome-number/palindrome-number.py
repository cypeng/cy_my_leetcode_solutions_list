
# @Title: Palindrome Number (Palindrome Number)
# @Author: pcysl
# @Date: 2017-12-20 00:26:47
# @Runtime: 582 ms
# @Memory: N/A

class Solution:
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        idx = 0
        jdx = len(str(x))-1
        nums= str(x)
        k   = 0
        while (idx <= jdx):
            if (nums[idx] == nums[jdx]):
                answer = bool(1)
                k = k+1
            else:
                answer = bool(0)
                break
            idx = idx+1
            jdx = jdx-1
            
            
        return answer
