
# @Title: 前 K 个高频元素 (Top K Frequent Elements)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-28 12:23:02
# @Runtime: 48 ms
# @Memory: 17.6 MB

class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        hashmap = {}
        idx, jdx = 0, len(nums)-1
        while idx <= jdx:
            if nums[idx] in hashmap:
                hashmap[nums[idx]] += 1
            else:
                hashmap[nums[idx]] = 1
            if not idx == jdx:
                if nums[jdx] in hashmap:
                    hashmap[nums[jdx]] += 1
                else:
                    hashmap[nums[jdx]] = 1
            idx += 1
            jdx -= 1
        order = []
        hashmap_count = {}
        for idx, item in enumerate(hashmap):
            if not hashmap[item] in hashmap_count:
                hashmap_count[hashmap[item]] = [item]
            else:
                hashmap_count[hashmap[item]].append(item)
            order.append(hashmap[item])
        order.sort(reverse=True)
        ans, keys = [], []
        for idx in range(0, k):
            val = hashmap_count[order[idx]]
            if not order[idx] in keys:
                ans.extend(val)
            keys.append(order[idx])
        #print(hashmap,order)
        return ans
