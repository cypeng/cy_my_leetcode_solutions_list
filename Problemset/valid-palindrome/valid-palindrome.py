
# @Title: 验证回文串 (Valid Palindrome)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-24 00:12:16
# @Runtime: 96 ms
# @Memory: 15.9 MB

class Solution:
    def isPalindrome(self, s: str) -> bool:
        s = ''.join([i for i in s if i not in string.punctuation])
        s = ''.join([i for i in s if i not in string.whitespace])
        s = s.lower()
        #print(s) 
        idx, jdx = 0, len(s)-1
        while idx <= jdx:
            if not s[idx] == s[jdx]:
                return False
            idx += 1
            jdx -= 1
        return True
