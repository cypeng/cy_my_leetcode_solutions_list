
| [English](README_EN.md) | 中文 |

# [125. 验证回文串](https://leetcode-cn.com/problems/valid-palindrome/)

## 題目描述

<p>给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。</p>

<p><strong>说明：</strong>本题中，我们将空字符串定义为有效的回文串。</p>

<p> </p>

<p><strong>示例 1:</strong></p>

<pre>
<strong>输入:</strong> "A man, a plan, a canal: Panama"
<strong>输出:</strong> true
<strong>解释：</strong>"amanaplanacanalpanama" 是回文串
</pre>

<p><strong>示例 2:</strong></p>

<pre>
<strong>输入:</strong> "race a car"
<strong>输出:</strong> false
<strong>解释：</strong>"raceacar" 不是回文串
</pre>

<p> </p>

<p><strong>提示：</strong></p>

<ul>
	<li><code>1 <= s.length <= 2 * 10<sup>5</sup></code></li>
	<li>字符串 <code>s</code> 由 ASCII 字符组成</li>
</ul>


## 相關題目

- [双指针](https://leetcode-cn.com/tag/two-pointers)
- [字符串](https://leetcode-cn.com/tag/string)

## 相似題目

- [回文链表](../palindrome-linked-list/README.md)
- [验证回文字符串 Ⅱ](../valid-palindrome-ii/README.md)
