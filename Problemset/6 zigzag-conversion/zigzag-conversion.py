
# @Title: ZigZag Conversion (ZigZag Conversion)
# @Author: pcysl
# @Date: 2018-07-13 12:54:51
# @Runtime: 108 ms
# @Memory: N/A

class Solution:
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        if ((len(s) > numRows) and (2*numRows-2 > 0)):
            return ZigZagCut(s, numRows)
        return s

def ZigZagCut(s, numRows):
    SubStr = ['']*numRows
    Ans = ''
    idx = 0
    groupNo = 2*numRows-2
    while (idx <= len(s)-1):
        if (idx%groupNo < numRows):
            SubStr[idx%groupNo] = SubStr[idx%groupNo] + s[idx]
        else:
            thisIdx = (2*numRows-2) - idx%groupNo
            SubStr[thisIdx] = SubStr[thisIdx] + s[idx]
        idx += 1
    
    for idx in range(0, len(SubStr)):
        Ans = Ans + SubStr[idx]        

    return Ans
