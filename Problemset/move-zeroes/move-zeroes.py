
# @Title: 移动零 (Move Zeroes)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-16 21:40:18
# @Runtime: 896 ms
# @Memory: 15.3 MB

class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        idx = 0
        while idx <= len(nums)-2:
            while idx <= len(nums)-2 and nums[idx] != 0:
                  idx += 1
            jdx = idx +1
            while jdx <= len(nums)-1:
                  if nums[idx] != 0:
                     break
                  nums[idx] = nums[jdx]
                  nums[jdx] = 0
                  jdx+=1
            idx += 1
