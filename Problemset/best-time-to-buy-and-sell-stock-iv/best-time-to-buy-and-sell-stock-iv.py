
# @Title: 买卖股票的最佳时机 IV (Best Time to Buy and Sell Stock IV)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-26 23:05:09
# @Runtime: 72 ms
# @Memory: 14.9 MB

class Solution:
    def maxProfit(self, k: int, prices: List[int]) -> int:
        buy = [-10000]*(k+1)
        sell = [0]*(k+1)
        for price in prices:
            for idx in range(1,k+1):
                buy[idx] = max(buy[idx], sell[idx-1]-price)
                sell[idx] = max(sell[idx], buy[idx]+price)
        return sell[-1]
