
| English | 中文|

# [0106. Construct Binary Tree from Inorder and Postorder Traversal](https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/)

## Description

<p>Given inorder and postorder traversal of a tree, construct the binary tree.</p>

<p><strong>Note:</strong><br />
You may assume that duplicates do not exist in the tree.</p>

<p>For example, given</p>

<pre>
inorder =&nbsp;[9,3,15,20,7]
postorder = [9,15,7,20,3]</pre>

<p>Return the following binary tree:</p>

<pre>
    3
   / \
  9  20
    /  \
   15   7
</pre>


## Related Topics

- [Array](https://leetcode.com/tag/array)
- [Tree](https://leetcode.com/tag/tree)
- [Depth-first Search](https://leetcode.com/tag/depth-first-search)

## Similar Questions

- [Construct Binary Tree from Preorder and Inorder Traversal](../construct-binary-tree-from-preorder-and-inorder-traversal/README_EN.md)
