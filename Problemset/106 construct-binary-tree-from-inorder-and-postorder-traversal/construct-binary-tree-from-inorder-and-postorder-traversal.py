
# @Title: Construct Binary Tree from Inorder and Postorder Traversal (Construct Binary Tree from Inorder and Postorder Traversal)
# @Author: pcysl
# @Date: 2018-02-15 18:38:25
# @Runtime: 349 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def buildTree(self, inorder, postorder):
        """
        :type inorder: List[int]
        :type postorder: List[int]
        :rtype: TreeNode
        """
        return self.ToBuildTree(inorder, postorder)
        
        
    def ToBuildTree(self, inorder, postorder):
        if (len(postorder) > 0):
            root = TreeNode(postorder[-1])
            idx = inorder.index(postorder[-1])
            a, b = idx, len(postorder)-idx-1
            if (a > 0):
                root.left = self.ToBuildTree(inorder[0:a],postorder[0:a]) 
            if (b > 0):
                root.right= self.ToBuildTree(inorder[-b:],postorder[-b-1:-1])
        else:
            root = None
            
        return root
