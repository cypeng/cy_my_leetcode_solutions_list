
# @Title: 买卖股票的最佳时机 (Best Time to Buy and Sell Stock)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-19 18:47:32
# @Runtime: 136 ms
# @Memory: 23.5 MB

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        optprofit = 0
        minprice = 100000
        idx, jdx = 0, len(prices)-1
        for idx in range(0, len(prices)):
            if (prices[idx] < minprice):
                minprice = prices[idx]
            elif (prices[idx] - minprice > optprofit): 
                optprofit = prices[idx] - minprice
        return optprofit
