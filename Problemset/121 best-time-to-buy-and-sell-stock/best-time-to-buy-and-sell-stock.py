
# @Title: Best Time to Buy and Sell Stock (Best Time to Buy and Sell Stock)
# @Author: pcysl
# @Date: 2018-12-06 12:46:35
# @Runtime: 168 ms
# @Memory: 7.6 MB

def find_max_profit_three_day(prices):
    min_value = prices[0]
    max_value = prices[-1]
    opt_interval = max_value - min_value
    if (opt_interval < 0):
        opt_interval = 0
    if (len(prices) == 3):
        if (prices[1] - min_value > opt_interval):
            opt_interval = prices[1] - min_value
        if (max_value - prices[1] > opt_interval):
            opt_interval = max_value - prices[1]
    return opt_interval

def find_max_profit(prices):
    idx = 1
    #min_value = prices[0]
    opt_interval = find_max_value(prices,0)
    #opt_min = min_value
    last_slop, next_slop = 0, 0
    while (1):
        if (idx < len(prices)-1):
            if (prices[idx] < prices[idx-1]):
                last_slop = -1
            elif (prices[idx] > prices[idx-1]):
                last_slop = 1
            if (prices[idx] < prices[idx+1]):
                next_slop = 1
            elif (prices[idx] > prices[idx+1]):
                next_slop = -1
            
            if ((last_slop == -1) and (next_slop == 1)):
                interval = find_max_value(prices,idx)
                if (interval > opt_interval):
                    opt_interval = interval
                    #min_value = prices[idx]
            """
            if ((last_slop == 1) and (next_slop == -1)):
                interval = prices[idx] - min_value
                if (interval > opt_interval):
                    opt_interval = interval
            """
        
        idx += 1
        if (idx == len(prices)-1):
            return opt_interval

def find_max_value(prices,idx):
    jdx = len(prices)-1
    interval = prices[jdx] - prices[idx]
    opt_max = prices[jdx]
    jdx -= 1
    while (jdx > idx):
        if (prices[jdx] > opt_max) or ((prices[jdx] > prices[jdx+1]) and (prices[jdx] > prices[jdx-1])):
            this_interval = prices[jdx] - prices[idx]
            if (this_interval > interval):
                interval = this_interval
                opt_max = prices[jdx]
        jdx -= 1
        if (idx==jdx):
            break
        
    if (interval < 0):
        return 0
    return interval

class Solution:
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        if (len(prices) > 3):
            return find_max_profit(prices)
        elif (len(prices) > 1) and (len(prices) <= 3):
            return find_max_profit_three_day(prices)
        else:
            return 0
        
