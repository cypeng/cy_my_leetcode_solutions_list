
# @Title: Happy Number (Happy Number)
# @Author: pcysl
# @Date: 2018-01-08 09:56:28
# @Runtime: 42 ms
# @Memory: N/A

class Solution(object):
    def isHappy(self, n):
        """
        :type n: int
        :rtype: bool
        """
        Hash = {}
        while (n != 1):
            n = self.GetHappyNum(n)
            if (not str(n) in Hash):
                Hash[str(n)] = 1
            else:
                return bool(0)
            
        return bool(1)
    
    def GetHappyNum(self,n):
        sumnum = 0
        while (n > 0):
            sumnum = sumnum + (n%10)**2
            n = n//10
        
        return sumnum    
