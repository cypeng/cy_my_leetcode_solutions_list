
# @Title: Climbing Stairs (Climbing Stairs)
# @Author: pcysl
# @Date: 2019-03-04 16:36:46
# @Runtime: 52 ms
# @Memory: 13.3 MB

def main2(n):
    if (n < 3):
        return n

    map_cost = [0]*n

    for idx in range(0, n):
        if (idx == 0):
            map_cost[0] = 1
        else:
            if (idx == 1):
                map_cost[1] = 2
            else:
                map_cost[idx] = map_cost[idx-1]+map_cost[idx-2]

    return map_cost[-1]

class Solution:
    def climbStairs(self, n: int) -> int:
        return main2(n)
