
# @Title: Maximum Depth of Binary Tree (Maximum Depth of Binary Tree)
# @Author: pcysl
# @Date: 2018-02-14 14:00:33
# @Runtime: 87 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def maxDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.level = []
        rootlevel = 0
        if (root):
            rootlevel = 0
            self.level.append(root)
            while (self.level):
                thislevel = []
                thislevel = self.level
                self.level = []
                if (len(thislevel)>0):
                    for idx in range(0,len(thislevel)):
                        root = thislevel[idx]
                        self.leveltree(root)
                    rootlevel = rootlevel + 1
        return rootlevel
            
    def leveltree(self, root):
        if (root.left):
            self.level.append(root.left)
        if (root.right):
            self.level.append(root.right)
