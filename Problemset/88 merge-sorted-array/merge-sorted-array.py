
# @Title: Merge Sorted Array (Merge Sorted Array)
# @Author: pcysl
# @Date: 2018-11-09 12:53:10
# @Runtime: 84 ms
# @Memory: N/A

def num2_copy_to_nums1(this_index,m,nums1,nums2):
    #print(this_index,m)
    if (this_index > m-1):
        nums1[this_index] = nums2[this_index-m]

def swap(newnums,idx,jdx):
    temp = newnums[idx] 
    newnums[idx] = newnums[jdx]
    newnums[jdx] = temp
    
class Solution:
    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: void Do not return anything, modify nums1 in-place instead.
        """
        if (len(nums1) >= m+n):
            for idx in range(m,m+n):
                num2_copy_to_nums1(idx,m,nums1,nums2)
            
        for idx in range(0, m+n):
            for jdx in range(idx+1,m+n):
                if (nums1[idx] > nums1[jdx]):
                    swap(nums1,idx,jdx)
            
            
