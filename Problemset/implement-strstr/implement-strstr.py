
# @Title: 实现 strStr() (Implement strStr())
# @Author: pcysl@hotmail.com
# @Date: 2021-06-23 13:52:44
# @Runtime: 68 ms
# @Memory: 15.2 MB

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        if needle == "":
            return 0
        size_h, size_n = len(haystack), len(needle)
        idx, jdx, kdx = 0, 0, 0
        next_idx = []
        while (idx <= size_h-1) and (jdx <= size_n-1):
            h_char, n_char, k_char = haystack[idx], needle[jdx], needle[kdx]
            if jdx > 0 and kdx < 1:
                if h_char == k_char:
                    kdx+=1
                    next_idx = [idx, kdx] 
                else:
                    kdx = 0
            if h_char == n_char:
                jdx+=1
                #print('c',idx,jdx)
            else:
                if len(next_idx) > 0:
                    idx, jdx = next_idx[0], next_idx[1]
                    #print(idx,jdx)
                    next_idx = []
                    kdx = 0
                else: 
                    jdx = 0
            idx+=1
            if jdx > size_n-1:
                return idx-size_n
        return -1

