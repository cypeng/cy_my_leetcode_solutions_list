
# @Title: 搜索旋转排序数组 (Search in Rotated Sorted Array)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 21:18:15
# @Runtime: 52 ms
# @Memory: 15.1 MB

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        idx, jdx = 0, len(nums)-1
        while idx <= jdx:
            if nums[idx] == target:
                return idx
            if nums[jdx] == target:
                return jdx
                
            idx += 1
            jdx -= 1 
        return -1
