
# @Title: 逆波兰表达式求值 (Evaluate Reverse Polish Notation)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-04 18:56:14
# @Runtime: 60 ms
# @Memory: 16 MB

def operation(a,b,exp):
    if exp == '+':
        return int(a + b)
    if exp == '-':
        return int(a - b)
    if exp == '*':
        return int(a * b)
    if exp == '/':
        return int(a/b)

class myStack:
    def __init__(self, size):
        self.stack = [None]*size
        self.idx = -1
    
    def push(self, val):
        self.idx += 1
        self.stack[self.idx] = val

    def pop(self):
        if self.idx < 0:
            return None
        this_element = self.stack[self.idx]
        self.stack[self.idx] = None
        self.idx -= 1
        return this_element
    
    def showStack(self):
        print(self.stack)

class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        size = len(tokens)
        if size <= 1:
            return int(tokens[0])
        evalStack = myStack(size)
        for idx in range(0, size):
            #evalStack.showStack()
            this_token = tokens[idx]
            if this_token == '+' or this_token == '-' or this_token == '*' or this_token == '/':
                b = evalStack.pop()
                a = evalStack.pop()
                c = operation(a, b, this_token)
                if idx == size -1:
                    return c
                evalStack.push(int(c))
                #evalStack.showStack()
            else:
                evalStack.push(int(this_token))

