
# @Title: 对称二叉树 (Symmetric Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 20:36:11
# @Runtime: 40 ms
# @Memory: 14.7 MB

def unit_traversal(root, this_roots, this_roots_val):
    if root:
        if root.left:
            this_roots.append(root.left)
            this_roots_val.append(root.left.val)
        else:
            this_roots_val.append(None)
        if root.right:
            this_roots.append(root.right)
            this_roots_val.append(root.right.val)
        else:
            this_roots_val.append(None)
    return this_roots, this_roots_val

def traversal(this_roots):
    next_roots = []
    next_roots_val = []
    for root in this_roots:
        #print(root)
        next_roots, next_roots_val = unit_traversal(root, next_roots, next_roots_val)

    idx, jdx = 0, len(next_roots_val)-1
    #print(next_roots_val)
    while idx <= jdx:
        if not next_roots_val[idx] == next_roots_val[jdx]:
            #print('False')
            return False
        idx += 1
        jdx -= 1
    if len(next_roots) > 0:
        return traversal(next_roots)
    return True
    

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSymmetric(self, root: TreeNode) -> int:
        return traversal([root])
