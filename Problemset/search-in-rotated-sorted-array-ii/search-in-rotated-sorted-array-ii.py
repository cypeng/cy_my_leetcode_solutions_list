
# @Title: 搜索旋转排序数组 II (Search in Rotated Sorted Array II)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-05 00:50:09
# @Runtime: 28 ms
# @Memory: 15 MB

class Solution:
    def search(self, nums: List[int], target: int) -> bool:
        idx, jdx = 0, len(nums)-1
        while idx <= jdx:
            if (nums[idx] == target) or (nums[jdx] == target):
                return True
            idx+=1
            jdx-=1
        return False
