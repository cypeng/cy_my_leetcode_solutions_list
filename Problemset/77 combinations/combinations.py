
# @Title: Combinations (Combinations)
# @Author: pcysl
# @Date: 2018-11-07 17:59:20
# @Runtime: 556 ms
# @Memory: N/A

def __to_combine__(n, k):
    answer = []
    this_array = [0]*k
    for jdx in range(1, n-k+2):
        __array_combination__(n, answer, this_array, 0, jdx)
        
    return answer

def __array_combination__(n, answer, this_array, idx, number):
    this_array[idx] = number
    if (not idx == len(this_array)-1):
        for jdx in range(1, n-number+1):
            next_number = number+jdx
            #print(next_number,jdx,number,n)
            __array_combination__(n, answer, this_array, idx+1, next_number)
    else:
        answer.append(list(this_array))

class Solution:
    def combine(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: List[List[int]]
        """
        if ((k > 0) and (n > 0)):
            answer = __to_combine__(n, k)
        elif ((k == 0) and (n == 0)):
            answer = [[]]
        else:
            answer = []
    
        return answer
