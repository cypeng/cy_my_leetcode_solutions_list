
# @Title: 第K个语法符号 (K-th Symbol in Grammar)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-15 12:20:54
# @Runtime: 32 ms
# @Memory: 14.6 MB

def generateKthGrammar(n, k, cache):
    cache[n] = k
    pre_kth = int((k+1)/2)
    if n > 1:
        return generateKthGrammar(n-1, pre_kth, cache)
    return pre_kth, cache

def getKthGrammar(n, cache):
    if n == 1:
        return 0
    else:
        pre = getKthGrammar(n-1, cache)
        if pre == 0:
            this = 1-cache[n]%2
        else:
            this = cache[n]%2
    return this

class Solution:
    def kthGrammar(self, n: int, k: int) -> int:
        #cache = {'0': '01', '1':'10'}
        cache = {}
        kthG, cache = generateKthGrammar(n,k, cache) 
        thisKthG = getKthGrammar(n, cache)
        #print(cache)
        return thisKthG #int(kthG[k-1])
