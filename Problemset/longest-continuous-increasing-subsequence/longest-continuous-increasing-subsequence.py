
# @Title: 最长连续递增序列 (Longest Continuous Increasing Subsequence)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-30 01:21:31
# @Runtime: 40 ms
# @Memory: 15.5 MB

class Solution:
    def findLengthOfLCIS(self, nums: List[int]) -> int:
        if len(nums) == 0:
            return 0
        if len(nums) == 1:
            return 1
        count = 1
        comp = nums[0]
        opt = 0
        for idx in range(1, len(nums)):
            if nums[idx]-comp > 0:
                count += 1
            else:
                count = 1
            comp = nums[idx]
            if opt < count:
                opt = count
        return opt
