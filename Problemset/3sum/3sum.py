
# @Title: 三数之和 (3Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-11 00:42:17
# @Runtime: 772 ms
# @Memory: 17.6 MB

class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        answer = []
        nums = sorted(nums)
        if len(nums) < 3 or nums[-1] < 0 or nums[0] > 0:
            return answer
        if nums[0] == 0 and nums[-1] == 0:
            answer.append([0, 0, 0])
            return answer
        
        a = []
        for idx in range(0,len(nums)-2):
            if nums[idx] > 0:
                break
            if ((nums[idx] <= 0) and (nums[idx] != a)):
                a = nums[idx]
                jdx = idx + 1
                kdx = len(nums)-1
                while (jdx < kdx):
                    b = nums[jdx]
                    c = nums[kdx] 
                    if (b + c == -a):
                        answer.append([a,b,c])
                        jdx = jdx+1
                        kdx = kdx-1
                        while ((jdx< kdx) and (nums[jdx] == nums[jdx-1])):
                            jdx = jdx+1
                        while ((jdx< kdx) and (nums[kdx] == nums[kdx+1])):
                            kdx = kdx-1
                    elif (nums[jdx] + nums[kdx] < -a):
                        jdx = jdx+1
                    else:
                        kdx = kdx-1
        return answer
