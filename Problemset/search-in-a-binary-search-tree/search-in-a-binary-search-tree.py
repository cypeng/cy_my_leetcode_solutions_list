
# @Title: 二叉搜索树中的搜索 (Search in a Binary Search Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-06 21:59:27
# @Runtime: 112 ms
# @Memory: 16.9 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def traversal(root, val):
    if root:
        if val == root.val:
            return root
        if val < root.val:
            return traversal(root.left, val)
        if val > root.val:
            return traversal(root.right, val)
    return None

class Solution:
    def searchBST(self, root: TreeNode, val: int) -> TreeNode:
        return traversal(root, val)
