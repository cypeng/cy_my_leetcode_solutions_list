
# @Title: 最长递增子序列 (Longest Increasing Subsequence)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-07 00:05:56
# @Runtime: 3032 ms
# @Memory: 15.2 MB

class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        count = [1]*len(nums)
        idx = 1
        opt = 1
        while idx <= len(nums)-1:
            jdx = 0
            local_opt = 0
            while jdx <= idx-1:
                if local_opt < count[jdx]:
                    if nums[jdx] < nums[idx]:
                        local_opt = count[jdx]
                jdx += 1
            count[idx] += local_opt
            if opt < count[idx]:
                opt = count[idx]
            idx +=1
        #print(count)
        return opt
