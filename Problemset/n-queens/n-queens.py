
# @Title: N 皇后 (N-Queens)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-05 12:32:26
# @Runtime: 80 ms
# @Memory: 15.4 MB

def checkPattern(start_idx, start_jdx, n, case):
    idx, jdx = start_idx, start_jdx
    while (idx-1 >= 0) and (jdx-1 >= 0):
        idx -= 1
        jdx -= 1
        if case[idx][jdx] == 'Q':
            #print('F1',case[idx][jdx])
            return False
    idx, jdx = start_idx, start_jdx    
    while (idx-1 >= 0) and (jdx+1 <= n-1):
        idx -= 1
        jdx += 1
        if case[idx][jdx] == 'Q':
            #print('F2',case[idx][jdx])
            return False
    return True
    
def backtrace(idx,jdx,n,case,hash_j,ans):
    #print(idx,jdx,case,hash_j)
    if idx > n-1:
        ans = ans.append(case[:])
        return True
    if jdx +1 <= n-1:
        this_str = case[idx]
        case[idx] = this_str+"."
        backtrace(idx,jdx+1,n,case,hash_j,ans)
        case[idx] = this_str

    if hash_j[jdx]:
        Flag = True
        if idx -1 >= 0:
            pre_str = case[idx-1]
            Flag = checkPattern(idx,jdx, n, case)
            #print(pre_str, jdx, idx)
        if Flag:
            hash_j[jdx] = False
            this_str = case[idx]
            case[idx] = this_str+"Q"+"."*(n-1-jdx)
            backtrace(idx+1,0,n,case,hash_j,ans)
            hash_j[jdx] = True
            case[idx] = this_str
    return False
    
class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:
        hash_j = [True]*n
        case = ['']*n
        ans = []
        backtrace(0,0,n,case,hash_j,ans)
        return ans
