
# @Title: 二分查找 (Binary Search)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-16 12:20:01
# @Runtime: 52 ms
# @Memory: 15.9 MB

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        idx, jdx = 0, len(nums)-1
        while idx <= jdx:
            if nums[idx] == target:
                return idx
            if nums[jdx] == target:
                return jdx
            idx += 1
            jdx -= 1
        return -1
