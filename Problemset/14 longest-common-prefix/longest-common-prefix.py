
# @Title: Longest Common Prefix (Longest Common Prefix)
# @Author: pcysl
# @Date: 2018-07-16 18:39:14
# @Runtime: 216 ms
# @Memory: N/A

class Solution:
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        if (len(strs) == 1):
            return strs[0]
        if (len(strs) < 1):
            return ""
        return findCommonPrefix(strs)

def longPrefix(stdstr, strs, sidx):
    for idx in range(0, len(strs)):
        if (idx == sidx):
            continue
        compstr = strs[idx]
        flag = 0
        if (compstr[0:len(stdstr)] == stdstr):
            flag = 1
        
        if (not flag):
            return bool(0)
    return bool(1)

def findCommonPrefix(strs):
    iniIdx = []
    for idx in range(0, len(strs)):
        if (idx == 0):
            lenstr = len(strs[idx])
            iniIdx.append(idx)
        else:
            if (len(strs[idx]) <= lenstr):
                iniIdx.append(idx)
           
    for idx in range(0, len(iniIdx)):
        stdstr = strs[iniIdx[idx]]
        for comlen in range(0, len(stdstr)):
            finIdx = len(stdstr) - comlen
            if (finIdx <= len(stdstr)):
                baseStr = stdstr[0:finIdx]
                
                if (longPrefix(baseStr, strs, iniIdx[idx])):
                    return baseStr
    return ""
