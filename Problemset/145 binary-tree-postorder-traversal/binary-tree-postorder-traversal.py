
# @Title: Binary Tree Postorder Traversal (Binary Tree Postorder Traversal)
# @Author: pcysl
# @Date: 2018-03-08 00:15:35
# @Runtime: 40 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def postorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.ans = []
        self.postorder(root)
        return self.ans
    
    def postorder(self, root):
        if (root):
            self.postorder(root.left)
            self.postorder(root.right)
            self.ans.append(root.val)
