
# @Title: 二叉树的层序遍历 (Binary Tree Level Order Traversal)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 14:30:38
# @Runtime: 48 ms
# @Memory: 15.5 MB

def unit_traversal(root, next_root_ans, next_root):
    if root:
        if root.left:
            next_root.append(root.left)
            next_root_ans.append(root.left.val)
        if root.right:
            next_root.append(root.right)
            next_root_ans.append(root.right.val)
        return next_root, next_root_ans

def traversal(this_root, answer):
    next_root, next_root_ans = [], []
    for root in this_root:
        next_root, next_root_ans = unit_traversal(root, next_root_ans, next_root)
        #print(next_root_ans)
    if len(next_root_ans) > 0:
        answer.append(next_root_ans)
    if len(next_root) > 0:
        traversal(next_root, answer)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        if root is None:
            return []
        answer = [[root.val]]
        next_root = [root]
        traversal(next_root, answer)
        return answer
