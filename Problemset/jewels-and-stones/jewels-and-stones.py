
# @Title: 宝石与石头 (Jewels and Stones)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-27 22:19:30
# @Runtime: 36 ms
# @Memory: 15.1 MB

class Solution:
    def numJewelsInStones(self, jewels: str, stones: str) -> int:
        hashset = set()
        idx,jdx = 0, len(jewels)-1
        while idx <= jdx:
            hashset.add(jewels[idx])
            if not idx == jdx:
                hashset.add(jewels[jdx])
            idx+=1
            jdx-=1
        count = 0
        idx,jdx = 0, len(stones)-1
        while idx <= jdx:
            if stones[idx] in hashset:
                count += 1
            if not idx == jdx:
                if stones[jdx] in hashset:
                    count += 1
            idx+=1
            jdx-=1
        return count
