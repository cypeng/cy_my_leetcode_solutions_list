
# @Title: Find Peak Element (Find Peak Element)
# @Author: pcysl
# @Date: 2017-12-28 00:30:08
# @Runtime: 59 ms
# @Memory: N/A

class Solution:
    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        idx = 0
        jdx = len(nums)-1
        answer = nums[0]
        ansIdx = 0
        while (idx <= jdx): 
            while (idx < len(nums)-1) and (nums[idx+1] > nums[idx]):
                idx = idx+1
            while (jdx > 0) and (nums[jdx-1] > nums[jdx]):
                jdx = jdx-1
            if (idx > 0) and (idx < len(nums)-1):
                if (nums[idx] > nums[idx-1] ) and (nums[idx] > nums[idx+1]):
                    return idx
            if (jdx > 0) and (jdx < len(nums)-1):
                if (nums[jdx] > nums[jdx-1] ) and (nums[jdx] > nums[jdx+1]):
                    return jdx
            if (idx == jdx):
                return idx
            idx = idx+1
            jdx = jdx-1

        
        return ansIdx
