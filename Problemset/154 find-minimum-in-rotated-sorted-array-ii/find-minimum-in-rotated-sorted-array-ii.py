
# @Title: Find Minimum in Rotated Sorted Array II (Find Minimum in Rotated Sorted Array II)
# @Author: pcysl
# @Date: 2017-12-27 15:39:48
# @Runtime: 69 ms
# @Memory: N/A

class Solution:
    def findMin(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # Remove the front and the rear same number
        # nums[front] = nums[front+1] =....
        # nums[rear] = nums[rear-1] = ....
        # nums[front] = nums[rear]
    
        answer = nums[0]
        if (len(nums) > 1):
            idx = 0
            jdx = len(nums)-1 
            while (idx <= jdx):
                midIdx = int((idx+jdx)*0.5)
                while (idx < len(nums)-1) and (nums[idx] == nums[idx+1]):
                    idx = idx + 1
                while (jdx > 0) and (nums[jdx] == nums[jdx-1]):
                    jdx = jdx - 1
                if (nums[midIdx] < nums[midIdx-1]):
                    answer = nums[midIdx]
                    break
                if (nums[idx] < answer):
                    answer = nums[idx]
                if (nums[jdx] < answer):
                    answer = nums[jdx]
                idx = idx +1
                jdx = jdx -1
        
        return answer 
