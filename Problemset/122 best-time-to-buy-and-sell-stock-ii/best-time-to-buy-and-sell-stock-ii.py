
# @Title: Best Time to Buy and Sell Stock II (Best Time to Buy and Sell Stock II)
# @Author: pcysl
# @Date: 2018-12-03 14:03:56
# @Runtime: 60 ms
# @Memory: N/A

def find_max_profit(prices):
    idx = 0
    profit = 0
    while (idx <= len(prices)-1):
        if (idx > 0) and (idx <= len(prices)-1):
            if (prices[idx-1] < prices[idx]):
                profit += (prices[idx] - prices[idx-1])
        if (idx == len(prices)-1):
            break
        idx += 1
        
    return profit

class Solution:
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        return find_max_profit(prices)
