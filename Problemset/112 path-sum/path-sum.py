
# @Title: Path Sum (Path Sum)
# @Author: pcysl
# @Date: 2018-03-11 20:48:38
# @Runtime: 68 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def hasPathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: bool
        """
        
        if (root):
            self.rootArray = []
            self.rootArray.append(root)
            self.rootSum   = []
            self.rootSum.append(sum)
            while (self.rootArray):
                rootArray = []
                rootSum   = []
                for idx in range(0,len(self.rootArray)):
                    root = self.rootArray[idx]
                    
                    sum  = self.rootSum[idx] - root.val
                    print(root.val,self.rootSum[idx])
                    if (root.left):
                        rootArray.append(root.left)
                        rootSum.append(sum)
                    if (root.right):
                        rootArray.append(root.right)
                        rootSum.append(sum)
                       
                    if (not root.left) and (not root.right):
                        if (sum == 0):
                            return bool(1)
                self.rootSum = []
                self.rootSum = rootSum
                self.rootArray = []
                self.rootArray = rootArray
            return bool(0)
        else:
            return bool(0)
