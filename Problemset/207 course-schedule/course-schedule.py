
# @Title: Course Schedule (Course Schedule)
# @Author: pcysl
# @Date: 2018-07-10 12:59:07
# @Runtime: 272 ms
# @Memory: N/A

class Solution:
    def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        
        
        mapCourses  = [None]*numCourses
        
        for idx in range(0,len(prerequisites)):
            if (mapCourses[prerequisites[idx][0]] == None):
                eachmap = []
            else:
                eachmap = mapCourses[prerequisites[idx][0]]
            eachmap.append(prerequisites[idx][1])
            mapCourses[prerequisites[idx][0]] = eachmap
        
        viewDict = [0]*numCourses
        for idx in range(0, numCourses):
            if (self.CheckLoop(mapCourses, idx, viewDict, idx)):
                return bool(0)
        return bool(1)
    
    def CheckLoop(self, MapCourses, count, ViewDict, thredidx):
        EachMap = MapCourses[count]
        newViewDict = list(ViewDict)
        if (count < thredidx):
            return bool(0)
        if (EachMap == None):
            return bool(0)
        else:   
            if (newViewDict[count] == 0):
                newViewDict[count] = 1
                for jdx in range(0, len(EachMap)):
                    searchcount = EachMap[jdx]
                    if (self.CheckLoop(MapCourses, searchcount, newViewDict, thredidx)):
                        return bool(1)
            else:
                return bool(1)
            
         
