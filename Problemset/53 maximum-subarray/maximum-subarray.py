
# @Title: Maximum Subarray (Maximum Subarray)
# @Author: pcysl
# @Date: 2019-02-25 12:54:27
# @Runtime: 12516 ms
# @Memory: 13.6 MB

def main(nums):
    answer = None
    if (len(nums) == 0):
        return answer
    answer = nums[0]
    if (len(nums) == 0):
        return answer
    
    for idx in range(0, len(nums)):
        sum_num = 0
        for jdx in range(idx, len(nums)):
            sum_num += nums[jdx]
            if (sum_num > answer):
                answer = sum_num
            #print(sum_num, idx, jdx)

    return answer
class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        return main(nums)
