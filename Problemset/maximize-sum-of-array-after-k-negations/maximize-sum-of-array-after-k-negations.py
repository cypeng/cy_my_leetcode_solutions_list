
# @Title: K 次取反后最大化的数组和 (Maximize Sum Of Array After K Negations)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-07 00:04:58
# @Runtime: 36 ms
# @Memory: 15.2 MB

class Solution:
    def largestSumAfterKNegations(self, nums: List[int], k: int) -> int:
        nums.sort()
        idx = 0
        while k > 0:
            if nums[idx] < 0:
                k-= 1
                nums[idx] = -1*nums[idx]
            else:
                nums.sort()
                break
            idx += 1
        #print(nums)
        k = k%2
        if k == 1:
            nums[0] = -1*nums[0]
        #print(nums)
        return sum(nums)
