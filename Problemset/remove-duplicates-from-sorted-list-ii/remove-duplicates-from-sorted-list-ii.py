
# @Title: 删除排序链表中的重复元素 II (Remove Duplicates from Sorted List II)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 12:46:17
# @Runtime: 48 ms
# @Memory: 14.9 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def checkEachDeplicates(l):
    head = l
    depFlag = False
    while l:
        if l.next:
            if l.next.val == l.val:
                depFlag = True
                l = l.next
            else:
                return depFlag, head, l.next
        else:
            return depFlag, head, l.next

def removeDeplicates(l):
    iniFlag = True
    depFlag = True
    answerHead = None
    while l:
        depFlag, head, l = checkEachDeplicates(l)
        if iniFlag and not depFlag:
            answerHead = head
            answer = answerHead
            answer.next = None
            iniFlag = False
        elif not iniFlag and not depFlag:
            answer.next = head
            answer = answer.next
            answer.next = None
        #print('L', depFlag, l)
    return answerHead
    
class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if head is None:
            return head
        if head.next is None:
            return head  
        return removeDeplicates(head)
