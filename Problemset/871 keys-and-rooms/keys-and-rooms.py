
# @Title: Keys and Rooms (Keys and Rooms)
# @Author: pcysl
# @Date: 2018-08-02 10:13:38
# @Runtime: 40 ms
# @Memory: N/A

class Solution:
    def canVisitAllRooms(self, rooms):
        """
        :type rooms: List[List[int]]
        :rtype: bool
        """
        return checkVisitAllRooms(rooms)

def checkVisitAllRooms(rooms):
    visitRecord = [0]*len(rooms)
    nextRooms(rooms, visitRecord, 0)
    if (0 in visitRecord):
        return bool(0)
    return bool(1)
    
def nextRooms(rooms, visitRecord, visitNo):
    if (0 == visitRecord[visitNo]):
        visitRecord[visitNo] = 1
        thisroom = rooms[visitNo]
        for idx in range(0,len(thisroom)):
            nextRooms(rooms, visitRecord, thisroom[idx])
