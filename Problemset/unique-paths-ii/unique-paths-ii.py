
# @Title: 不同路径 II (Unique Paths II)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-16 12:09:37
# @Runtime: 40 ms
# @Memory: 14.9 MB

class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid: List[List[int]]) -> int:
        if obstacleGrid[0][0] == 1:
            return 0
        n, m = len(obstacleGrid[0]), len(obstacleGrid)
        F = [0]*n
        if obstacleGrid[0][0] == 0:
            F[0] = 1
        elif obstacleGrid[0][0] == 1:
            F[0] = 0
        for idx in range(0, m):
            for jdx in range(0, n):
                if (jdx >= 0 and idx == 0) or (idx >= 0 and jdx == 0):
                    if idx > 0:
                        if obstacleGrid[idx][jdx] == 1 or F[jdx] == 0:
                            F[jdx] = 0
                        else:
                            F[jdx] = 1
                    if jdx > 0:
                        if obstacleGrid[idx][jdx] == 1 or F[jdx-1] == 0:
                            F[jdx] = 0
                        else:
                            F[jdx] = 1
                else:
                    if obstacleGrid[idx][jdx] == 1:
                        F[jdx] = 0
                    else:
                        F[jdx] = F[jdx]+F[jdx-1]
        return F[-1]
