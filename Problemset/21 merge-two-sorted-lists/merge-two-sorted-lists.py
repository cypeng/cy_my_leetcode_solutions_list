
# @Title: Merge Two Sorted Lists (Merge Two Sorted Lists)
# @Author: pcysl
# @Date: 2019-07-11 14:06:22
# @Runtime: 48 ms
# @Memory: 13.3 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

def lists_cond(l1, l2):
    if l1.val < l2.val:
        return 1
    if l2.val < l1.val:
        return 2
    if l1.val == l2.val:
        return 3

def merge_init(l1, l2):
    case = lists_cond(l1, l2)
    if case is 1:
        return l1
    if case is 2:
        return l2
    if case is 3:
        return l1    

def merge_lists(l1, l2):
    if l1 is None and l2:
        return l2
    if l1 and l2 is None:
        return l1
    if l1 is None and l2 is None:
        return None

    case = lists_cond(l1, l2)
    if case is 1:
        l = ListNode(l1.val)
        l1 = l1.next
        answer = l
        answer_head = answer
    if case is 2:
        l = ListNode(l2.val)
        l2 = l2.next
        answer = l
        answer_head = answer
    if case is 3:
        l = ListNode(l1.val)
        answer = l
        answer_head = answer
        answer.next = ListNode(l2.val)
        answer = answer.next
        l1 = l1.next
        l2 = l2.next
    while (1):
        if l1 is None and l2:
            answer.next = l2
            return answer_head
        if l2 is None and l1:
            answer.next = l1
            return answer_head
        if l1 is None and l2 is None:
            return answer_head
        case = lists_cond(l1, l2)
        if case is 1:
            l = ListNode(l1.val)
            answer.next = l
            l1 = l1.next
        if case is 2:
            l = ListNode(l2.val)
            answer.next = l
            l2 = l2.next
        if case is 3:
            l = ListNode(l1.val)
            answer.next = l
            answer = answer.next
            l = ListNode(l2.val)
            answer.next = l
            l1 = l1.next
            l2 = l2.next
        answer = answer.next
    return answer_head

class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        return merge_lists(l1, l2)
