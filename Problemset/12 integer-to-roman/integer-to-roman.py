
# @Title: Integer to Roman (Integer to Roman)
# @Author: pcysl
# @Date: 2018-07-16 11:21:22
# @Runtime: 144 ms
# @Memory: N/A

class Solution:
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        return int2roman(num)
    
def int2roman(num):
    idxMap   = {1:6, 5:5, 10:4, 50:3, 100:2, 500:1, 1000:0}
    intMap   = [1000, 500, 100, 50, 10, 5, 1]
    romanMap = ['M','D','C','L','X','V','I']
    idx = 0
    roman = ''
    while (num > 0):
        if (num >= intMap[idx]):
            scale = int(num/intMap[idx])
            roman = roman + romanMap[idx]*scale
            num -= intMap[idx]*scale
        else:
            if (intMap[idx]-num <= 10**(len(str(num))-1)):
                comIdx = idxMap[10**(len(str(num))-1)]
                roman = roman + romanMap[comIdx] + romanMap[idx]
                num -= (intMap[idx]-intMap[comIdx])
            else:
                idx += 1
    return roman
