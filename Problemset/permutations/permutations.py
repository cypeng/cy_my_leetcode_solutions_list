
# @Title: 全排列 (Permutations)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-04 10:58:00
# @Runtime: 36 ms
# @Memory: 15.3 MB

def backtrack(idx, nums, case, ans):
    if (len(nums) == 0) or (idx > len(nums)-1):
        if (len(nums) == 0):
            ans.append(case)
    else:
        backtrack(idx+1, nums, case, ans)
        
        this_case = case.copy()
        this_case.append(nums[idx])
        this_nums = nums.copy()
        #print(this_case, this_nums, idx)
        this_nums.remove(this_nums[idx])
        #print(this_case, this_nums, idx)
        backtrack(0, this_nums, this_case, ans)

class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        ans = []
        backtrack(0, nums, [], ans)
        return ans
