
# @Title: O(1) 时间插入、删除和获取随机元素 (Insert Delete GetRandom O(1))
# @Author: pcysl@hotmail.com
# @Date: 2021-06-29 00:40:23
# @Runtime: 1068 ms
# @Memory: 49.3 MB

class RandomizedSet:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        import random
        self.set = []


    def insert(self, val: int) -> bool:
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        """
        if not val in self.set:
            self.set.append(val)
            return True
        return False

    def remove(self, val: int) -> bool:
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        """
        for idx, item in enumerate(self.set):
            if item == val:
                self.set[-1], self.set[idx] = self.set[idx], self.set[-1]
                self.set.pop()
                return True
        return False

    def getRandom(self) -> int:
        """
        Get a random element from the set.
        """
        idx = int(random.random()*len(self.set))
        return self.set[idx]

# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()
