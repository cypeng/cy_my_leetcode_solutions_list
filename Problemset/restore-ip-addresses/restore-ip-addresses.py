
# @Title: 复原 IP 地址 (Restore IP Addresses)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-03 21:58:53
# @Runtime: 44 ms
# @Memory: 14.9 MB

def backtrack(idx,s,this_ip,hashset,count,ans):
    #print(idx,this_ip, s)
    if (idx > len(s)-1) or (count >= 4):
        if (count == 4) and (idx > len(s)-1):
            ans.append(this_ip)
        return False
    
    if (int(s[idx]) < 256) and (idx+1 <= len(s)):
        if count == 0:
            backtrack(idx+1,s,s[idx],hashset,count+1,ans)
        else:
            backtrack(idx+1,s,this_ip+'.'+s[idx],hashset,count+1,ans)
    if (not int(s[idx]) == 0) and (int(s[idx:idx+2]) < 256) and (idx+2 <= len(s)):
        if count == 0:
            backtrack(idx+2,s,s[idx:idx+2],hashset,count+1,ans)
        else:
            backtrack(idx+2,s,this_ip+'.'+s[idx:idx+2],hashset,count+1,ans)
    if (not int(s[idx]) == 0) and (int(s[idx:idx+3]) < 256) and (idx+3 <= len(s)):
        if count == 0:
            backtrack(idx+3,s,s[idx:idx+3],hashset,count+1,ans)
        else:
            backtrack(idx+3,s,this_ip+'.'+s[idx:idx+3],hashset,count+1,ans)    
        
class Solution:
    def restoreIpAddresses(self, s: str) -> List[str]:
        ans = []
        hashset = []
        backtrack(0,s,'',hashset,0,ans)
        return ans
