
# @Title: Search in Rotated Sorted Array (Search in Rotated Sorted Array)
# @Author: pcysl
# @Date: 2017-12-25 14:09:40
# @Runtime: 69 ms
# @Memory: N/A

class Solution:
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        answer = -1
        idx = 0
        jdx = len(nums)-1
        while (idx <= jdx):
            if (nums[idx] == target):
                answer = idx
                return answer
            elif (nums[jdx] == target):
                answer = jdx
                return answer
        
            idx = idx+1
            jdx = jdx-1
        
        return answer
