
# @Title: 单调递增的数字 (Monotone Increasing Digits)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-07 01:34:26
# @Runtime: 40 ms
# @Memory: 15 MB

def check_monotone(n):
    str_n = str(n)
    len_n = len(str_n)
    pre_digit = int(str_n[0])
    for idx in range(1, len(str_n)):
        if int(str_n[idx]) < pre_digit:
            return (len_n-idx)-1
        pre_digit = int(str_n[idx])
    return -1

def tune_to_upperbound(n, upperbound):
    str_n = str(n)
    len_n = len(str_n)
    idx, jdx = len_n-1, 0
    opt = n
    while (n < upperbound) and (idx >= 0): 
        #print(n, idx, jdx)
        while (n + 10**jdx <upperbound) and (not int(str(n)[idx]) == 9): 
            n += 10**jdx
        if check_monotone(n) == -1:
            opt = n
        #print(check_monotone(n), n)
        jdx+=1
        idx-=1
    return opt

class Solution:
    def monotoneIncreasingDigits(self, n: int) -> int:
        upperbound = n
        while 1:
            ith_digit = check_monotone(n)
            if ith_digit == -1:
                break
            n -= (10**ith_digit)
        opt = tune_to_upperbound(n, upperbound)
        return opt
