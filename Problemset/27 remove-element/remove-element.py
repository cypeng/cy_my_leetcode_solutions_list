
# @Title: Remove Element (Remove Element)
# @Author: pcysl
# @Date: 2017-12-16 10:37:25
# @Runtime: 35 ms
# @Memory: N/A

class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        kdx = 0
        for idx in range(0,len(nums)):
            if (nums[idx] != val):
                nums[kdx] = nums[idx]
                kdx = kdx+1
                
        return kdx
        
