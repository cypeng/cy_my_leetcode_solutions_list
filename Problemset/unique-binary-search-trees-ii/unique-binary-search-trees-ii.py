
# @Title: 不同的二叉搜索树 II (Unique Binary Search Trees II)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-15 21:36:47
# @Runtime: 60 ms
# @Memory: 16.4 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def createTree(ori_root, left_roots, right_roots, this_ans): 
    #print(left_roots, right_roots)
    if len(left_roots) > 0 and len(right_roots) > 0:
        for idx in range(0, len(left_roots)):
            for jdx in range(0, len(right_roots)):
                this_root = TreeNode(ori_root.val)
                this_root.left = left_roots[idx]
                this_root.right = right_roots[jdx]
                this_ans.append(this_root)
    elif len(left_roots) > 0 and len(right_roots) == 0:
        for idx in range(0, len(left_roots)):
            this_root = TreeNode(ori_root.val)
            this_root.left = left_roots[idx]
            this_ans.append(this_root)
    elif len(left_roots) == 0 and len(right_roots) > 0:
        for jdx in range(0, len(right_roots)):
            this_root = TreeNode(ori_root.val)
            #this_root.left = left_roots[idx]
            this_root.right = right_roots[jdx]
            this_ans.append(this_root)
    else:
        this_ans.append(ori_root)
    return this_ans

def splitNode(this_nums):
    this_ans = []
    for idx in range(0, len(this_nums)):
        node_val = this_nums[idx]
        this_root = TreeNode(node_val)
        left_roots, right_roots = [], []
        left_nums, right_nums = [], []
        if idx > 0:
            left_nums = this_nums[0:idx]
            left_roots = splitNode(left_nums)
        if idx+1 > 0 and idx + 1 <= len(this_nums)-1:
            right_nums = this_nums[idx+1:]
            right_roots = splitNode(right_nums)
        #print(this_nums, idx, left_roots, right_roots, left_nums, right_nums)
        this_ans = createTree(this_root, left_roots, right_roots, this_ans)
    return this_ans

class Solution:
    def generateTrees(self, n: int) -> List[TreeNode]:
        nums = [idx for idx in range(1,n+1)]
        return splitNode(nums) #createTree(None, 1)
