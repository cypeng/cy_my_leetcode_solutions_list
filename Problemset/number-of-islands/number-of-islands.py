
# @Title: 岛屿数量 (Number of Islands)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-03 00:24:14
# @Runtime: 72 ms
# @Memory: 18.6 MB

def groupIsland(grid, idx, jdx):
    while grid[idx][jdx] == '1':
        grid[idx][jdx] = 0
        if idx +1 <= len(grid)-1:
            groupIsland(grid, idx+1, jdx)
        if jdx +1 <= len(grid[0])-1:
            groupIsland(grid, idx, jdx+1)
        if idx -1 >= 0:
            groupIsland(grid, idx-1, jdx)
        if jdx -1 >= 0:
            groupIsland(grid, idx, jdx-1)

class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        ans = 0
        for idx in range(0, len(grid)):
            for jdx in range(0, len(grid[idx])):
                if grid[idx][jdx] == '1':
                    ans += 1
                    groupIsland(grid, idx, jdx)
        return ans

