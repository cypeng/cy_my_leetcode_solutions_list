
# @Title: 两个数组的交集 II (Intersection of Two Arrays II)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-19 18:32:38
# @Runtime: 52 ms
# @Memory: 15.3 MB

def findRange(nums, mid_idx):
    idx,jdx = mid_idx, mid_idx
    while (idx - 1 >= 0) and (nums[idx] == nums[idx-1]):
        idx -= 1
    while (jdx + 1 <= len(nums)-1) and (nums[jdx] == nums[jdx+1]):
        jdx += 1
    return jdx-idx+1

class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:
        nums1.sort()
        nums2.sort()
        ans = []
        for idx, num in enumerate(nums1):
            if idx > 0 and num == nums1[idx-1]:
                continue
            kdx, ldx = 0, len(nums2)-1
            count_idx, count = idx, 1
            while count_idx+1 <= len(nums1)-1 and nums1[count_idx] == nums1[count_idx+1]:
                count_idx+=1
                count+=1
            while kdx <= ldx:
                mid = floor(kdx + (ldx-kdx)/2)
                #print(num, mid,kdx,ldx, nums2)
                if num == nums2[mid]:
                    this_count = findRange(nums2, mid)
                    #print(this_count,count)
                    if this_count >= count:
                        ans.extend([num]*count)
                    else:
                        ans.extend([num]*this_count)
                    break
                if (kdx == ldx): # or (mid == kdx) or (mid == ldx):
                    break
                if nums2[mid] > num:
                    ldx = mid-1
                elif nums2[mid] < num:
                    kdx = mid+1
                else:
                    kdx +=1 
        return ans
