
# @Title: 有序链表转换二叉搜索树 (Convert Sorted List to Binary Search Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-02 14:22:07
# @Runtime: 88 ms
# @Memory: 20.5 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def LinkedList2Array(head):
    num = []
    while head:
        num.append(head.val)
        head = head.next
    return num

def buildTree(num):
    if len(num) == 0:
        return None
    length_num = len(num)
    node_idx = length_num//2
    if (length_num%2 == 1):
        node_idx = (length_num-1)//2
    root = TreeNode(val = num[node_idx])
    root.left = buildTree(num[0:node_idx])
    root.right = buildTree(num[node_idx+1:])
    return root

class Solution:
    def sortedListToBST(self, head: ListNode) -> TreeNode:
        num = LinkedList2Array(head)
        return buildTree(num)
