
# @Title: 验证二叉搜索树 (Validate Binary Search Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-06 18:12:20
# @Runtime: 52 ms
# @Memory: 17.6 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def traversal(root, pre, flag):
    if flag and root:
        if root.left:
            pre, flag = traversal(root.left, pre, flag)
        if not pre == None and pre >= root.val:
            flag = False
        pre = root.val
        if root.right:
            pre, flag = traversal(root.right, pre, flag)
    return pre, flag

class Solution:
    def isValidBST(self, root: TreeNode) -> bool:
        return traversal(root, None, True)[1]
