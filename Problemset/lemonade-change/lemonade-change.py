
# @Title: 柠檬水找零 (Lemonade Change)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-06 11:00:05
# @Runtime: 68 ms
# @Memory: 15.2 MB

def customer_change(change, my_money):
    for bill in [20, 10, 5]:
        while change >= bill:
            if my_money[bill] > 0:
                my_money[bill] -= 1
                change -= bill
            else:
                break
    return change

class Solution:
    def lemonadeChange(self, bills: List[int]) -> bool:
        my_money = {5:0, 10:0, 20:0}
        for pay_money in bills:
            if pay_money == 5:
                my_money[pay_money] += 1
            else:
                change = pay_money - 5
                change = customer_change(change, my_money)
                #print(my_money, change)
                if change > 0:
                    return False
                my_money[pay_money] += 1
        return True
