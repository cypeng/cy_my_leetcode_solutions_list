
# @Title: 子集 (Subsets)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-04 11:09:28
# @Runtime: 32 ms
# @Memory: 15.1 MB

def backtrace(idx,nums,case,ans):
    if (idx > len(nums)-1):
        ans.append(case)
        return 0
    
    backtrace(idx+1,nums,case,ans)
    this_case = case.copy()
    this_case.append(nums[idx])
    backtrace(idx+1,nums,this_case,ans)
        

class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        ans = []
        backtrace(0,nums,[],ans)
        return ans
