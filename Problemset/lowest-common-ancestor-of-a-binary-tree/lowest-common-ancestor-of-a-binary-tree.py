
# @Title: 二叉树的最近公共祖先 (Lowest Common Ancestor of a Binary Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-01 01:29:26
# @Runtime: 1928 ms
# @Memory: 455 MB

def traversal(root, p, q, path, path_p, path_q, flag1, flag2):
    if root:
        path.append(root)
        if p == root:
            path_p = path.copy()
            flag1 = True
        if q == root:
            path_q = path.copy()
            flag2 = True
        if flag1 and flag2:
            return flag1, flag2, path_p, path_q
        if root.left:
            flag1, flag2, path_p, path_q = traversal(root.left, p, q, path.copy(), path_p, path_q, flag1, flag2)
        if root.right:
            flag1, flag2, path_p, path_q = traversal(root.right, p, q, path.copy(), path_p, path_q, flag1, flag2)
    return flag1, flag2, path_p, path_q

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        path, path_p, path_q = [], [], []
        flag1, flag2, path_p, path_q = traversal(root, p, q, path, path_p, path_q, False, False)
        idx, jdx = len(path_p)-1, len(path_q)-1
        while 1:
            jdx = len(path_q)-1
            while jdx >= 0:
                if path_p[idx] == path_q[jdx]:
                    return path_p[idx]
                jdx -= 1
            idx -= 1


