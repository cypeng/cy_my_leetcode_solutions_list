
# @Title: 猜数字大小 (Guess Number Higher or Lower)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-17 00:30:55
# @Runtime: 40 ms
# @Memory: 14.9 MB

# The guess API is already defined for you.
# @param num, your guess
# @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
# def guess(num: int) -> int:

class Solution:
    def guessNumber(self, n: int) -> int:
        idx, jdx = 1, n
        while 1:
            guess_num = int((idx+jdx)*0.5)
            #print(guess_num, idx, jdx)
            if guess(guess_num) == 0:
                return guess_num
            if guess(guess_num) == -1:
                jdx = guess_num - 1
            if guess(guess_num) == 1:
                idx = guess_num + 1
                

