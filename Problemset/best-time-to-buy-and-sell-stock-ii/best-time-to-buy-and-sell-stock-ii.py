
# @Title: 买卖股票的最佳时机 II (Best Time to Buy and Sell Stock II)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-08 00:05:34
# @Runtime: 28 ms
# @Memory: 15.6 MB

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        pre_comp = prices[0]
        output = 0
        for idx in range(1, len(prices)):
            if prices[idx] > pre_comp:
                output += (prices[idx] - pre_comp)
            pre_comp = prices[idx]
        return output
