
# @Title: 多米诺和托米诺平铺 (Domino and Tromino Tiling)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-09 00:30:55
# @Runtime: 24 ms
# @Memory: 15 MB

class Solution:
    def numTilings(self, n: int) -> int:
        # n = 1, output = 1
        # n = 2, output = 2
        # n = 3, output = 5
        # n = 4, output = 11, 
        # n = 5, output = 24
        # n = 6, output = 53
        # n = 7, output = 117
        
        # n = 25, output = 178424817
        # n = 26, output = 393528322
        # n = 27, output = 867954037
        # n = 28, output = 914332884
        # n = 29, output = 222194076
        # n = 30, output = 312342182
        # n, output = 2*f(n-1)+f(n-3)
        dp = [1, 2, 5]
        if n <= 3:
            return dp[n-1]
        else:
            for idx in range(3,n):
                dp[idx%3] = dp[idx%3] + 2*dp[(idx-1)%3]
                #print(dp)
        while dp[idx%3] > 10**9+7:
            dp[idx%3] = dp[idx%3]%(10**9+7)
        return dp[idx%3]
