
# @Title: 最接近的三数之和 (3Sum Closest)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-12 01:07:02
# @Runtime: 84 ms
# @Memory: 14.9 MB

class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        nums = sorted(nums)
        idx, opt = 0, None
        opt_sum = None
        while idx < len(nums)-2:
            this_target = target - nums[idx]
            jdx, kdx = idx+1, len(nums)-1
            while jdx < kdx:
                a, b = nums[jdx], nums[kdx]
                err = (a+b) - this_target
                if opt == None:
                    opt = abs(err)
                    opt_sum = nums[idx]+nums[jdx]+nums[kdx]
                elif opt > abs(err):
                    opt = abs(err)
                    opt_sum = nums[idx]+nums[jdx]+nums[kdx]
                if err > 0:
                    kdx -= 1
                    while jdx < kdx and nums[kdx] == nums[kdx+1]:
                        kdx -= 1
                elif err < 0:
                    jdx+=1
                    while jdx < kdx and nums[jdx] == nums[jdx-1]:
                        jdx += 1
                elif err == 0:
                    return target
            idx += 1
            while idx < len(nums)-2 and nums[idx] == nums[idx-1]:
                idx+=1
        return opt_sum
