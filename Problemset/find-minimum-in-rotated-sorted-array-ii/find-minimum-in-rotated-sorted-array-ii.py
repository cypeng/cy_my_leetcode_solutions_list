
# @Title: 寻找旋转排序数组中的最小值 II (Find Minimum in Rotated Sorted Array II)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-18 21:31:03
# @Runtime: 44 ms
# @Memory: 15.3 MB

class Solution:
    # [3, 1, 1], [10, 1, 10, 10, 10], [10, 10, 10, 10, 1, 10], [0, 1, 5, 5, 5, 5], [3,3,1,3]
    def findMin(self, nums: List[int]) -> int:
        size = len(nums)
        if size == 1:
            return nums[0]
        idx, jdx = 0, size-1
        opt = None
        if nums[idx] < nums[jdx]:
            return nums[idx]
        while idx <= jdx:
            mid = (idx+jdx)//2
            if (opt == None) or (nums[mid] < opt):
                opt = nums[mid]
            if (opt == None) or (nums[idx] < opt):
                opt = nums[idx]
            if (opt == None) or (nums[jdx] < opt):
                opt = nums[jdx]
            if idx + 1 <= size-1: 
                idx += 1
                while (idx + 1 <= size-1) and (nums[idx] == nums[idx-1]):
                    idx += 1
            if jdx - 1 >= 0:
                jdx -= 1
                while (jdx - 1 >= 0) and (nums[jdx] == nums[jdx-1]):
                    jdx -= 1
        return opt
