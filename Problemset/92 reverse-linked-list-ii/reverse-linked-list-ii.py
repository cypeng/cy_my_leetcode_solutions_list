
# @Title: Reverse Linked List II (Reverse Linked List II)
# @Author: pcysl
# @Date: 2019-07-12 16:01:42
# @Runtime: 48 ms
# @Memory: 13.1 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

def find_connect(l, m, n):
    count = 0
    l1_head, l1_tail = l, None
    l2_head, l2_tail = None, None
    l3_head, l3_tail = None, None
    while (1):
        count += 1
        if (l is None):
            break
        if count == m-1:
            l1_tail = l
        if count == m:
            l2_head = l
        if count == n:
            l2_tail = l
        if count == n+1:
            l3_head = l
        if l.next is None:
            l3_tail = l
        l = l.next
    if l1_tail:
        l1_tail.next = None
    if l2_head:
        l2_tail.next = None
    return l1_head, l1_tail, l2_head, l2_tail, l3_head, l3_tail

def reverse_l(l_head, l_tail):
    if l_head == l_tail:
        return l_head, l_tail
    p1, p2, p3 = None, None, None
    head, tail = None, None
    while (1):
        p3 = l_head.next

        if p1 and p2:
            p1 = p2
            p2 = l_head
        elif p1 and p2 is None:
            p2 = l_head
        elif p1 is None and p2 is None:
            p1 = l_head
            tail = p1
            p1.next = None

        if  p1 and p2:
            p2.next = p1

        if p3 is None:
            head = l_head
            break
        else:
            l_head = p3
            
    return head, tail
        
def reverse_list(l, m, n):
    if l is None or l.next is None:
        return l
    l1_head, l1_tail, l2_head, l2_tail, l3_head, l3_tail = find_connect(l, m, n)
    l2_head, l2_tail = reverse_l(l2_head, l2_tail)
    if l1_tail is None:
        ans = l2_head
    else:
        ans = l1_head
        l1_tail.next = l2_head

    if l3_head is None:
        l2_tail.next = None
    else:
        l2_tail.next = l3_head
    return ans

class Solution:
    def reverseBetween(self, head: ListNode, m: int, n: int) -> ListNode:
        return reverse_list(head, m, n)
        
