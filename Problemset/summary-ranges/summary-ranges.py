
# @Title: 汇总区间 (Summary Ranges)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-21 12:04:39
# @Runtime: 28 ms
# @Memory: 15 MB

class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:
        if len(nums) == 0:
            return []
        if len(nums) == 1:
            return [str(nums[0])]
        ans = []
        comp = nums[0]
        start = comp
        for idx in range(1, len(nums)):
            if (nums[idx] - comp > 1):
                if (nums[idx-1] - start > 0):
                    ans.append(str(start)+"->"+str(nums[idx-1]))
                else:
                    ans.append(str(start))
                start = nums[idx]
            comp = nums[idx]
        if nums[idx] - start == 0:
            ans.append(str(start))
        else:
            ans.append(str(start)+"->"+str(nums[idx]))
        return ans
