
# @Title: 寻找比目标字母大的最小字母 (Find Smallest Letter Greater Than Target)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-18 01:23:41
# @Runtime: 56 ms
# @Memory: 16.4 MB

def letter2num(letter):
    return ord(letter)-96

def calLetterDiff(letter_num, target):
    diff = letter_num - target
    if diff < 0:
        return 26+diff
    return diff

class Solution:
    def nextGreatestLetter(self, letters: List[str], target: str) -> str:
        target = letter2num(target)
        idx, jdx = 0, len(letters)-1
        while 1:
            idx_letter, jdx_letter = letter2num(letters[idx]), letter2num(letters[jdx])
            idx_diff, jdx_diff = calLetterDiff(idx_letter, target), calLetterDiff(jdx_letter, target)
            pre_idx_letter, pre_jdx_letter = letter2num(letters[idx-1]), letter2num(letters[jdx-1])
            pre_idx_diff, pre_jdx_diff = calLetterDiff(pre_idx_letter, target), calLetterDiff(pre_jdx_letter, target)
            #print(idx, jdx, idx_diff, jdx_diff)
            if ((pre_idx_diff > idx_diff) and (idx_diff > 0)) or ((pre_idx_diff == 0) and (idx_diff > 0)):
                #print('ok')
                return letters[idx]
            if ((pre_jdx_diff > jdx_diff) and (jdx_diff > 0)) or ((pre_jdx_diff == 0) and (jdx_diff > 0)): 
                #print('ok')
                return letters[jdx]
            idx += 1
            while (idx+1 <= len(letters)-1) and (letters[idx] == letters[idx-1]):
                idx += 1
            jdx -= 1
            while (jdx-1 >= 0) and (letters[jdx] == letters[jdx+1]):
                jdx -= 1
            

