
# @Title: 回文子串 (Palindromic Substrings)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-08 11:25:13
# @Runtime: 164 ms
# @Memory: 22.5 MB

class Solution:
    def countSubstrings(self, s: str) -> int:
        dp = [[0]*len(s) for idx in range(0, len(s))]
        count = 0
        for idx in range(len(s)-1,-1,-1):
            for jdx in range(idx, len(s)):
                if s[idx] == s[jdx]:
                    if (jdx-idx <= 1):
                        dp[idx][jdx] = 1
                        count += 1
                    else:
                        if(dp[idx+1][jdx-1] == 1):
                            count += 1
                            dp[idx][jdx] = 1
        return count
