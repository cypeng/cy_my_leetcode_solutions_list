
# @Title: 数组中的第K个最大元素 (Kth Largest Element in an Array)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-23 18:18:58
# @Runtime: 4040 ms
# @Memory: 19.5 MB

def swap(nums, idx, jdx):
    nums[idx], nums[jdx] = nums[jdx], nums[idx]

def quicksort(data, left, right): # 輸入資料，和從兩邊開始的位置
    if left >= right :            # 如果左邊大於右邊，就跳出function
        return

    i = left                      # 左邊的代理人
    j = right                     # 右邊的代理人
    key = data[left]                 # 基準點

    while i != j:                  
        while data[j] > key and i < j:   # 從右邊開始找，找比基準點小的值
            j -= 1
        while data[i] <= key and i < j:  # 從左邊開始找，找比基準點大的值
            i += 1
        if i < j:                        # 當左右代理人沒有相遇時，互換值
            data[i], data[j] = data[j], data[i] 

    # 將基準點歸換至代理人相遇點
    data[left] = data[i] 
    data[i] = key

    quicksort(data, left, i-1)   # 繼續處理較小部分的子循環
    quicksort(data, i+1, right)  # 繼續處理較大部分的子循環


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        len_nums = len(nums)
        fk = len_nums-k
        if len_nums <= 2:
            if len_nums > 1 and nums[1] < nums[0]:
                swap(nums, 0, 1)
            return nums[fk]
        
        quicksort(nums, 0, len_nums-1)
        return nums[fk]
