
# @Title: Remove Duplicates from Sorted Array II (Remove Duplicates from Sorted Array II)
# @Author: pcysl
# @Date: 2018-11-14 14:25:17
# @Runtime: 52 ms
# @Memory: N/A

def removeDuplicates(nums):
    copy_index, answer = find_copy_index(nums, 0)
    probe_index = copy_index
    while (probe_index < len(nums) -1):
        probe_index = find_next_group_start_index(nums, probe_index)
        #print(probe_index)
        if (probe_index == len(nums)-1):
            if (nums[probe_index] == nums[copy_index]):
                answer = copy_index-1
            break
        else:
            probe_index+=1
        probe_index, copy_index = copy_elements(nums,copy_index,probe_index)
        answer = copy_index
        if (probe_index == len(nums)-1):
            break

    return answer+1

def find_copy_index(nums, this_group_start_index):
    if (this_group_start_index < len(nums) -1):
        count = 0
        while (1):
            if (nums[this_group_start_index+1] == nums[this_group_start_index]):
                count += 1
            else:
                count = 0
            this_group_start_index += 1
            if (this_group_start_index == len(nums) -1) or (count > 1):
                #print(nums, this_group_start_index, count)
                copy_index = this_group_start_index
                if (this_group_start_index == len(nums) -1) and (count > 1):
                    copy_index = this_group_start_index-1
                break
        
    return this_group_start_index, copy_index

def copy_elements(nums,copy_index,probe_index):
    count = 0
    while (1):
        nums[copy_index] = nums[probe_index]
        if (count > 1) or (probe_index == len(nums)-1):
            if (count > 1) and (probe_index == len(nums)-1):
                copy_index -= 1
            break
        if (nums[probe_index+1] == nums[probe_index]):
            count += 1
        else:
            count = 0
        copy_index += 1
        probe_index+= 1
    
    return probe_index, copy_index


def find_next_group_start_index(nums, probe_index):
    while (nums[probe_index+1] == nums[probe_index]):
        probe_index +=1
        if (probe_index == len(nums)-1):
            break
    return probe_index

class Solution:
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if (len(nums) > 2):
            return removeDuplicates(nums)
        elif (len(nums) <= 2):
            return len(nums)
        
