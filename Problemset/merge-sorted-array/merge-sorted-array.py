
# @Title: 合并两个有序数组 (Merge Sorted Array)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-23 19:49:48
# @Runtime: 40 ms
# @Memory: 15 MB

class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        m_idx = m+n-1
        n1_idx, n2_idx = m-1, n-1
        while m_idx >= 0:
            #print('s', m_idx, n1_idx, n2_idx)
            while m_idx >= 0 and (n2_idx < 0 or (n1_idx >= 0 and nums1[n1_idx] > nums2[n2_idx])):
                nums1[m_idx] = nums1[n1_idx]
                m_idx -= 1
                n1_idx -= 1
            #print('m', m_idx, n1_idx, n2_idx)
            while m_idx >= 0 and (n1_idx < 0 or (n2_idx >= 0 and nums1[n1_idx] < nums2[n2_idx])):
                nums1[m_idx] = nums2[n2_idx]
                m_idx -= 1
                n2_idx -= 1
            #print('e', m_idx, n1_idx, n2_idx)
            while m_idx >= 0 and n1_idx >= 0 and n2_idx >= 0 and nums1[n1_idx] == nums2[n2_idx]:
                nums1[m_idx] = nums1[m_idx-1] = nums2[n2_idx]
                m_idx -= 2
                n1_idx -= 1
                n2_idx -= 1
