
# @Title: 字符串中的第一个唯一字符 (First Unique Character in a String)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-26 19:32:01
# @Runtime: 252 ms
# @Memory: 15 MB

class Solution:
    def firstUniqChar(self, s: str) -> int:
        hashmap = {}
        hashset = set()
        opt = 1000000000
        idx, jdx = 0, len(s)-1
        while idx <= jdx:
            if s[idx] in hashset:
                if s[idx] in hashmap:
                    hashmap.pop(s[idx])
            elif (not s[idx] in hashset):
                hashmap[s[idx]] = idx
                hashset.add(s[idx])
            if (not idx == jdx):
                if s[jdx] in hashset:
                    if s[jdx] in hashmap:
                        hashmap.pop(s[jdx])
                elif (not s[jdx] in hashset):
                    hashmap[s[jdx]] = jdx
                    hashset.add(s[jdx])
            idx += 1
            jdx -= 1
                 
        for item in hashmap:
            if (hashmap[item] < opt):
                opt = hashmap[item]
        if opt == 1000000000:
            return -1
        return opt
