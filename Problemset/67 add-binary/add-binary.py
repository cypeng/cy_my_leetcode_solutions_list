
# @Title: Add Binary (Add Binary)
# @Author: pcysl
# @Date: 2018-11-05 13:40:50
# @Runtime: 108 ms
# @Memory: N/A

class Solution:
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        if ((len(a) > 0) and (len(b) > 0)):
            if (len(a) >= len(b)):
                answer = self.BinaryAddition(a,b)
            else:
                answer = self.BinaryAddition(b,a)
        else:
            if (len(a) > 0):
                answer = a
            elif (len(b) > 0):
                answer = b
                
        return answer
    
    def BinaryAddition(self, a,b):
        answer = ''
        c = 0
        if (len(a) > len(b)):
            add_lenth_b = len(a)-len(b)
            b = '0'*add_lenth_b + b
        
        for idx in range(0, len(a)):
            kdx = len(b) -1 -idx        
            c = c + int(a[kdx])+int(b[kdx])
            #print(answer)
            if (c >= 2):
                answer = str(c%2) + answer
                c = 1
            else:
                answer = str(c) + answer 
                c = 0
        
        if ((0 == kdx) and (c)):
            answer = "1" +  answer
        
        return answer
