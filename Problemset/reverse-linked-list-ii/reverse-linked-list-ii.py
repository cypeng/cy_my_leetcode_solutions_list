
# @Title: 反转链表 II (Reverse Linked List II)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-02 00:19:06
# @Runtime: 36 ms
# @Memory: 15 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseBetween(self, head: ListNode, left: int, right: int) -> ListNode:
        count = 0
        left_tail = None
        new_head = head
        while head:
            cur = head
            head = head.next
            count += 1
            if count == left-1:
                left_tail = cur
            elif count == left:
                reverse_left = cur
            elif (count > left) and (count <= right):
                if pre.next == cur:
                    pre.next = None
                cur.next = pre
                if count == right:
                    if left_tail:
                        left_tail.next = cur
                    else:
                        new_head = cur
                    reverse_left.next = head
                    break
            pre = cur
        return new_head
