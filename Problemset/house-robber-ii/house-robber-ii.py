
# @Title: 打家劫舍 II (House Robber II)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-20 00:13:32
# @Runtime: 32 ms
# @Memory: 14.8 MB

def buildDP(dp, nums):
    flag = False
    for idx in range(1,len(nums)):
        if idx > 1:
            if (idx == len(nums)-1) and (flag or (idx-2 == 0)):
                dp[idx] = max(dp[idx-2], dp[idx-1])
            else:
                dp[idx] = max(dp[idx-2]+nums[idx], dp[idx-1])
                if (dp[idx-2]+nums[idx] > dp[idx-1]) and (idx-2 == 0):
                    flag = True
                
        else:
            dp[idx] = max(nums[0], nums[1])
    #print(dp)
    return dp

class Solution:
    def rob(self, nums: List[int]) -> int:
        dp0 = [0 for idx in range(0,len(nums))]
        dp0[0] = nums[0]
        dp1 = [0 for idx in range(0,len(nums))]
        dp0 = buildDP(dp0, nums)
        #nums[::-1]
        #print(nums[::-1])
        dp1[0] = nums[::-1][0]
        dp1 = buildDP(dp1, nums[::-1])
        #print(dp0, dp1)
        return max(dp0[-1], dp1[-1])
