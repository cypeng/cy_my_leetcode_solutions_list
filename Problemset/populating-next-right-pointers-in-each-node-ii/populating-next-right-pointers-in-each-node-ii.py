
# @Title: 填充每个节点的下一个右侧节点指针 II (Populating Next Right Pointers in Each Node II)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 21:16:13
# @Runtime: 68 ms
# @Memory: 16.1 MB

def unit_traversal(root, pre, this_roots):
    if root:
        if root.left:
            if not pre is None:
                pre.next = root.left
            pre = root.left
            this_roots.append(root.left)
   
        if root.right:
            if not pre is None:
                pre.next = root.right
            this_roots.append(root.right)
            pre = root.right
    return this_roots, pre

def traversal(this_roots):
    next_roots = []
    pre = None
    for root in this_roots:
        next_roots, pre = unit_traversal(root, pre, next_roots)
    
    if len(next_roots) > 0:
        traversal(next_roots)

"""
# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next
"""

class Solution:
    def connect(self, root: 'Node') -> 'Node':
        traversal([root])
        return root
