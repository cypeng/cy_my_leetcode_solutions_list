
# @Title: 二叉树的中序遍历 (Binary Tree Inorder Traversal)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 13:16:37
# @Runtime: 48 ms
# @Memory: 15 MB

def traversal(root, answer):
    if root:
        if root.left:
            #answer.append(root.left.val) 
            traversal(root.left, answer)
        answer.append(root.val)
        if root.right:
            #answer.append(root.right.val)
            traversal(root.right, answer)
        

class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        answer = []
        
        traversal(root, answer)
        return answer
