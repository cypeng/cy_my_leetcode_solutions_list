
# @Title: Plus One (Plus One)
# @Author: pcysl
# @Date: 2017-12-20 09:51:13
# @Runtime: 62 ms
# @Memory: N/A

class Solution:
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        jdx = len(digits)-1
        answer = digits
        while (digits[jdx] == 9):
            answer[jdx] = 0
            jdx = jdx-1

        if (jdx == -1):
            answer = [1] + answer
        else:    
            answer[jdx] = answer[jdx] + 1    
    
        return answer
