
# @Title: Permutations (Permutations)
# @Author: pcysl
# @Date: 2018-07-20 00:35:00
# @Runtime: 60 ms
# @Memory: N/A

class Solution:
    def permute(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        ans = []
        ansArray = [0]*len(nums)
        elementPermute(nums, 0, ansArray, ans)
        return ans

def elementPermute(nums, fidx, ansArray, ans):
    idx     = 0
    while (idx <= len(nums)-1): 
        newNums = list(nums)
        if (newNums[idx] != None):
            ansArray[fidx] = newNums[idx]
            newNums[idx] = None
            #print(newNums, ansArray, fidx)
            if (fidx == len(nums)-1):
                ans.append(list(ansArray))
            if (fidx < len(nums)-1):
                elementPermute(newNums, fidx+1, ansArray, ans)
        idx+=1
