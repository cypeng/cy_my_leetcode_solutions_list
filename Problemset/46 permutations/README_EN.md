
| English | 中文|

# [0046. Permutations](https://leetcode.com/problems/permutations/)

## Description

<p>Given a collection of <strong>distinct</strong> integers, return all possible permutations.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [1,2,3]
<strong>Output:</strong>
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]
</pre>


## Related Topics

- [Backtracking](https://leetcode.com/tag/backtracking)

## Similar Questions

- [Next Permutation](../next-permutation/README_EN.md)
- [Permutations II](../permutations-ii/README_EN.md)
- [Permutation Sequence](../permutation-sequence/README_EN.md)
- [Combinations](../combinations/README_EN.md)
