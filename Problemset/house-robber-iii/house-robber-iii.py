
# @Title: 打家劫舍 III (House Robber III)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-20 00:12:27
# @Runtime: 48 ms
# @Memory: 16.8 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def rob(root):
    if root == None:
        return (0, 0)
    left = rob(root.left)
    right = rob(root.right)
    val1 = root.val + left[1] + right[1]
    val2 = max(left[0],left[1])+max(right[0],right[1])
    return (val1, val2)

class Solution:
    def rob(self, root: TreeNode) -> int:
        dp = rob(root)
        return max(dp)
