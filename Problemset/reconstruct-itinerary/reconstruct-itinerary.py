
# @Title: 重新安排行程 (Reconstruct Itinerary)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-06 00:08:23
# @Runtime: 52 ms
# @Memory: 15.3 MB

def backtrace(cur, tickets, tickets_dict, ans):
    arri_list = tickets_dict[cur]
    arri_list.sort()
    if len(ans) == len(tickets)+1:
        return True
    
    for idx, item in enumerate(arri_list):
        item = arri_list[idx]
        arri_list.pop(idx)
        ans.append(item)
        if backtrace(item, tickets, tickets_dict, ans):
            return True
        ans.pop()
        #print(arri_list)
        arri_list.insert(0, item)

class Solution:
    def findItinerary(self, tickets: List[List[str]]) -> List[str]:
        #import collections
        ans = ['JFK']
        tickets_dict = collections.defaultdict(list)
        for dept, arri in tickets:
            tickets_dict[dept].append(arri)        
        backtrace('JFK', tickets, tickets_dict, ans)
        return ans
