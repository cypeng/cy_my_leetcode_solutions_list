
# @Title: 环形链表 II (Linked List Cycle II)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-25 11:21:26
# @Runtime: 64 ms
# @Memory: 17.8 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:
        LLF, LLS = head, head
        while LLF:
            if LLF.next and LLF.next.next and LLS.next:
                LLF, LLS = LLF.next.next, LLS.next
            else:
                return None
            if LLS == LLF:
                break
        LL = head
        while LL:
            if LL == LLF:
                return LL
            if LLF.next and LL.next:
                LLF, LL = LLF.next, LL.next
            
