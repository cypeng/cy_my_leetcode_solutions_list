
# @Title: Group Anagrams (Group Anagrams)
# @Author: pcysl
# @Date: 2018-01-05 00:54:24
# @Runtime: 282 ms
# @Memory: N/A

class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        HashTable = []
        HashFunc  = {}
        jdx = 0
        for idx in range(0, len(strs)):
            substr = strs[idx]
            Key = self.KeyFunc(substr)
            if (not HashFunc.has_key(str(Key))):
                subans =[]
                subans.append(str(substr))
                HashTable.append(subans)
                HashFunc[str(Key)] = jdx
                jdx = jdx+1
            else:
                findidx = HashFunc[str(Key)]
                subans = HashTable[findidx]
                subans.append(str(substr))                

        return HashTable
                
                
    def KeyFunc(self, substrs):
        sum = 0
        for idx in range(0,len(substrs)):
            x = (ord(substrs[idx])-ord("a"))
            c = x**6
            sum = sum + c*len(substrs)
            
        return sum
