
# @Title: 扁平化多级双向链表 (Flatten a Multilevel Doubly Linked List)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-27 23:23:30
# @Runtime: 52 ms
# @Memory: 15.6 MB

def readLinkedList(this_head, head, answer):
    while this_head:
        if head is None:
            head = Node(this_head.val, None, None, None)
            answer = head
            #print(answer)
        else:
            temp = head
            head.next = Node(this_head.val, None, None, None)
            head = head.next
            head.prev = temp
        #answer.append(this_head.val)
        if this_head.child:
            child_head = this_head.child
            answer, head = readLinkedList(child_head, head, answer)
        this_head = this_head.next
    return answer, head

"""
# Definition for a Node.
class Node:
    def __init__(self, val, prev, next, child):
        self.val = val
        self.prev = prev
        self.next = next
        self.child = child
"""

class Solution:
    def flatten(self, head: 'Node') -> 'Node':
        this_head, answer = None, None
        answer, _ = readLinkedList(head, this_head, answer)
        #print(answer)
        #print(answer)
        #return answer
        return answer
