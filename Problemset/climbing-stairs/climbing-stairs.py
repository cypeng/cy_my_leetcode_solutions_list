
# @Title: 爬楼梯 (Climbing Stairs)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-16 14:40:21
# @Runtime: 60 ms
# @Memory: 14.9 MB

class Solution:
    def climbStairs(self, n: int) -> int:
        if n < 1:
            return 0

        F = [1, 2]
        idx = 1
        while idx <= n:
            if idx > 2:
                if ((idx-1)%2) == 0:
                    F[0] = F[0] + F[1] 
                else:
                    F[1] = F[0] + F[1]
            #print(F)
            idx += 1
        
        return F[((n-1)%2)]
        
        

