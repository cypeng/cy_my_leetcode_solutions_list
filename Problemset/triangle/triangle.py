
# @Title: 三角形最小路径和 (Triangle)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-19 11:09:49
# @Runtime: 52 ms
# @Memory: 15.4 MB

class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        if len(triangle) == 1:
            return triangle[0][0]
        for idx in range(1, len(triangle)):
            for jdx in range(0,len(triangle[idx])):
                if jdx == 0:
                    triangle[idx][jdx] = triangle[idx-1][jdx] + triangle[idx][jdx]
                    b = triangle[idx-1][jdx-1] + triangle[idx][jdx]
                elif jdx == len(triangle[idx])-1:
                    triangle[idx][jdx] = triangle[idx-1][jdx-1] + triangle[idx][jdx]
                else:
                    a = triangle[idx-1][jdx] + triangle[idx][jdx]
                    b = triangle[idx-1][jdx-1] + triangle[idx][jdx]
                    if b < a:
                        triangle[idx][jdx] = b
                    else:
                        triangle[idx][jdx] = a
                if idx == len(triangle)-1:
                    if jdx == 0:
                        opt = triangle[idx][jdx]
                    else:
                        if opt > triangle[idx][jdx]:
                            opt = triangle[idx][jdx]
        return opt
        #print(triangle)
                    
