
# @Title: 零钱兑换 (Coin Change)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-11 20:00:14
# @Runtime: 1024 ms
# @Memory: 15.2 MB

class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:
        dp0 = [amount+1 for idx in range(0,amount+1)]
        dp0[0] = 0
        #dp1 = [0 for idx in range(0,amount+1)]
        #coins.sort()                         
        for coin in coins[::-1]:
            for jdx in range(coin,amount+1):
                #coin_num = (jdx-dp0[jdx])//coin
                dp0[jdx] = min(dp0[jdx],dp0[jdx-coin]+1)
                #dp1[jdx] = max
                #print(coin,jdx,dp0,dp1,jdx-coin,(jdx)//coin)
        
        #print(dp0)
        if dp0[-1] > amount:
            return -1
        return dp0[-1]
        
