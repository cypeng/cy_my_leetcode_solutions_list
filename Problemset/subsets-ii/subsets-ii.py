
# @Title: 子集 II (Subsets II)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-04 14:27:41
# @Runtime: 64 ms
# @Memory: 15.2 MB

def backtrace(idx,nums,case,hashset,ans):
    if idx > len(nums)-1:
        #print(case)
        case.sort()
        if not case in ans:
            ans.append(case)
        return True
    backtrace(idx+1,nums,case,hashset,ans)
    this_case = case.copy()
    this_case.append(nums[idx])
    backtrace(idx+1,nums,this_case,hashset,ans)

class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        ans = []
        hashset = []
        backtrace(0,nums,[],hashset,ans)
        return ans
