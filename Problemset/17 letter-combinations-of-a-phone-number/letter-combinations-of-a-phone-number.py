
# @Title: Letter Combinations of a Phone Number (Letter Combinations of a Phone Number)
# @Author: pcysl
# @Date: 2018-07-17 13:02:40
# @Runtime: 36 ms
# @Memory: N/A

class Solution:
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        if (len(digits) > 0):
            return letterPress(digits)
        else:
            return []
        

def letterPress(digits):
    PhoneDict = {'2':['a','b','c'], '3':['d','e','f'], '4':['g','h','i'], '5':['j','k','l'],
                 '6':['m','n','o'], '7':['p','q','r','s'], '8':['t','u', 'v'], '9':['w','x','y','z']}
    DL = list(digits)
    ans= ''
    eachMap = []
    ansStrs = []
    totalNo = 1  
    for idx in range(0, len(DL)):
        thisDigist = DL[idx]
        thisMap = PhoneDict[thisDigist]
        totalNo = totalNo*len(thisMap)
        eachMap.append(thisMap)
    
    findLetter(eachMap, ans, 0, ansStrs)
    return ansStrs
            
def findLetter(eachMap, ans, thisIdx, ansStrs):
    thisMap = eachMap[thisIdx]
    orians = str(ans)
        
    for idx in range(0, len(thisMap)):
        if (thisIdx == 0):
            ans = ''
        else:
            ans = str(orians)
        ans = ans + thisMap[idx]
        #print(thisIdx, ans, eachMap)
        if (not thisIdx == len(eachMap)-1):
            findLetter(eachMap, ans, thisIdx+1, ansStrs)
        else:
            ansStrs.append(ans)
