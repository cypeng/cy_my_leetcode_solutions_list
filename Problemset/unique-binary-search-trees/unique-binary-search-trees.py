
# @Title: 不同的二叉搜索树 (Unique Binary Search Trees)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-10 00:54:19
# @Runtime: 28 ms
# @Memory: 15.1 MB

def build_table(n, bst_nodes):
    if n%2 == 1:
        to_count_n = (n+1)//2
    else:
        to_count_n = n//2
    this_node = 0
    for root_node_idx in range(0,to_count_n):
        left_nodes_n = root_node_idx-0
        right_nodes_n = n-root_node_idx-1
        if (n%2 == 1) and (root_node_idx == to_count_n-1):
            this_node += bst_nodes[left_nodes_n]*bst_nodes[right_nodes_n]
        else:
            this_node += 2*bst_nodes[left_nodes_n]*bst_nodes[right_nodes_n]
    bst_nodes[n] = this_node
    return bst_nodes

class Solution:
    def numTrees(self, n: int) -> int:
        # [1,2,3, 4, 5,  6,  7,   8,   9,   10,   11,    12,    13,     14,     15]
        # [1,2,5,14,42,132,429,1430,4862,16796,58786,208012,742900,2674440,9694845]
        # [1]     1
        # [1,2]   1+1, 2*f(1)
        # [1,2,3] 2+1+2, 2*f(2)+f(1)
        # [1,2,3,4] 5+2+2+5, 2*f(3)+2*f(2)
        # [1,2,3,4,5] 14+5+4+5+14, 2*f(4)+2*f(3)+2*f(2)
        # [1,2,3,4,5,6] 42+14+10+10+14+42, 2*f(5)+2*f(4)+2*2*f(3)
        # [1,2,3,4,5,6,7] 132+42+28+5*5+28+42+132
        bst_nodes = [0]*(n+1)
        bst_nodes[0:3] = [1,1,2,5]
        if n <= 3:
            return bst_nodes[n]
        for idx in range(4,n+1):
            bst_nodes = build_table(idx, bst_nodes)
        #print(bst_nodes)
        return bst_nodes[n]
