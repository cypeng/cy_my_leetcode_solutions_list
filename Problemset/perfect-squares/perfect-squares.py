
# @Title: 完全平方数 (Perfect Squares)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-03 23:41:36
# @Runtime: 636 ms
# @Memory: 30.8 MB

def find_upperbound(square_number):
    this_root = square_number/2
    while (square_number - this_root*this_root) < 0:
        this_root -= 1
    return int(this_root)+1

def traversal(root, targets, step):
    next_roots = []
    max_root = 0
    #print(targets)
    for target in targets:
        ori_root = root
        while ori_root > 0:
            #print(target)
            this_root = target - ori_root*ori_root
            if this_root == 0:
                return step
            if this_root > 0:
                #print('check', ori_root)
                if ori_root > max_root:
                    max_root = ori_root
                next_roots.append(this_root)
            ori_root -= 1
    #print(next_roots, root)
    if len(next_roots) > 0:
        step = traversal(max_root, next_roots, step+1)
    return step

class Solution:
    def numSquares(self, n: int) -> int:
        n_upperbound = find_upperbound(n)
        step = traversal(n_upperbound, [n], 1)
        return step
