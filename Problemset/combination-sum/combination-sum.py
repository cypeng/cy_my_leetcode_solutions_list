
# @Title: 组合总和 (Combination Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-03 11:46:24
# @Runtime: 256 ms
# @Memory: 15 MB

def generateCaseCombination(start_idx,candidates,case,case_sum,target,ans):
    if case_sum == target:
        ans.append(case)
        return True
    
    for idx in range(start_idx,len(candidates)):
        item = candidates[idx]
        this_case = case.copy()
        this_case.append(item)
        if case_sum <= target:
            generateCaseCombination(idx,candidates,this_case,case_sum+item,target,ans)
    
class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        ans = []
        generateCaseCombination(0,candidates,[],0,target,ans)
        return ans
