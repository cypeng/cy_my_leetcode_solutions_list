
# @Title: Rotate List (Rotate List)
# @Author: pcysl
# @Date: 2019-07-11 18:20:17
# @Runtime: 36 ms
# @Memory: 13.2 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

def length_l(l):
    idx = 0
    while (1):
        if l is None:
            return idx
        idx += 1
        l = l.next

def find_head(l, head_idx):
    idx = 0
    flag = 0
    cut_head = None
    while (1):
        idx += 1
        if idx == head_idx:
            answer = l
        if idx == head_idx-1:
            cut_head = l
            flag = 1
        if l.next is None:
            tail = l
            break
        l = l.next
    if flag:
        cut_head.next = None
    return answer, tail
        
def rotate(l, k):
    if l is None or l.next is None or k == 0:
        return l
    len_l = length_l(l)
    if k%len_l == 0:
        return l
    k = k%len_l
    k = len_l-k+1
    l2 = l
    answer, l_tail = find_head(l, k)
    if k > 1:
        l_tail.next = l2
    return answer

class Solution:
    def rotateRight(self, head: ListNode, k: int) -> ListNode:
        return rotate(head, k)
        
