
# @Title: Implement Queue using Stacks (Implement Queue using Stacks)
# @Author: pcysl
# @Date: 2018-02-06 00:11:01
# @Runtime: 63 ms
# @Memory: N/A

class MyQueue:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.data = []

    def push(self, x):
        """
        Push element x to the back of queue.
        :type x: int
        :rtype: void
        """
        self.data.append(x)

    def pop(self):
        """
        Removes the element from in front of queue and returns that element.
        :rtype: int
        """
        rx = self.data[0]
        del self.data[0]
        
        return rx

    def peek(self):
        """
        Get the front element.
        :rtype: int
        """
        return self.data[0]

    def empty(self):
        """
        Returns whether the queue is empty.
        :rtype: bool
        """
        if (len(self.data) > 0):
            return bool(0)
        else:
            return bool(1)


# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()
