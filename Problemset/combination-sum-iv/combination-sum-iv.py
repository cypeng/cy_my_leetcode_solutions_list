
# @Title: 组合总和 Ⅳ (Combination Sum IV)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-13 00:28:54
# @Runtime: 40 ms
# @Memory: 15.1 MB

class Solution:
    def combinationSum4(self, nums: List[int], target: int) -> int:
        #   0 1 2 3 4  5  6  7
        # 1 1 1 1 1 1  1  1  1
        # 2 1 1 2 3 5  8 13 21
        # 3 1 1 2 4 7 13 24 44
        dp = [0 for idx in range(0, target+1)]
        dp[0] = 1
        #print(dp)
        
        for jdx in range(0,target+1):
            for idx in range(0,len(nums)):
                if jdx-nums[idx] >= 0:
                    dp[jdx] = dp[jdx] + dp[jdx-nums[idx]]
            #print(idx,dp)
        return dp[-1]
