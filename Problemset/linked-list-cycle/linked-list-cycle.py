
# @Title: 环形链表 (Linked List Cycle)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 12:09:39
# @Runtime: 60 ms
# @Memory: 18 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def hasCycle(self, head: ListNode) -> bool:
        if head is None:
            return False
        l1, l2 = head, head
        while 1:
            if l1.next:
                l1 = l1.next
            if l2.next:
                if l2.next.next:
                    l2 = l2.next.next
                else:
                    return False
            else:
                return False
            if l1 == l2:
                return True
                
