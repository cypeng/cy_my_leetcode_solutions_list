
# @Title: Validate Binary Search Tree (Validate Binary Search Tree)
# @Author: pcysl
# @Date: 2018-02-13 14:28:12
# @Runtime: 97 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def isValidBST(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        
        self.ans  = []
        self.InorderTree(root)
        print(self.ans)
        
        
        for i in range(0,len(self.ans)-1):
            if (self.ans[i] >= self.ans[i+1]):
                return bool(0)        

        
        return bool(1)
        
    def InorderTree(self, root):
        if (root):
            self.InorderTree(root.left)
            self.ans.append(root.val)
            self.InorderTree(root.right)
            
