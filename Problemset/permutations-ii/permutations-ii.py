
# @Title: 全排列 II (Permutations II)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-04 12:26:12
# @Runtime: 180 ms
# @Memory: 15.3 MB

def backtrace(idx,nums,case,hashset,ans):
    if idx > len(nums)-1:
        if len(nums) == 0:
            ans.append(case)
        return True
    backtrace(idx+1,nums,case,hashset,ans)
    if not nums[idx] == None:
        this_case = case.copy()
        this_case.append(nums[idx])
        if not this_case in hashset:
            hashset.append(this_case)
            this_nums = nums.copy()
            this_nums[idx] = None
            this_nums.remove(None)
            backtrace(0,this_nums,this_case,hashset,ans)

class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        ans = []
        hashset = []
        backtrace(0,nums,[],hashset,ans)
        return ans
