
| [English](README_EN.md) | 中文 |

# [47. 全排列 II](https://leetcode-cn.com/problems/permutations-ii/)

## 題目描述

<p>给定一个可包含重复数字的序列 <code>nums</code> ，<strong>按任意顺序</strong> 返回所有不重复的全排列。</p>

<p> </p>

<p><strong>示例 1：</strong></p>

<pre>
<strong>输入：</strong>nums = [1,1,2]
<strong>输出：</strong>
[[1,1,2],
 [1,2,1],
 [2,1,1]]
</pre>

<p><strong>示例 2：</strong></p>

<pre>
<strong>输入：</strong>nums = [1,2,3]
<strong>输出：</strong>[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
</pre>

<p> </p>

<p><strong>提示：</strong></p>

<ul>
	<li><code>1 <= nums.length <= 8</code></li>
	<li><code>-10 <= nums[i] <= 10</code></li>
</ul>


## 相關題目

- [数组](https://leetcode-cn.com/tag/array)
- [回溯](https://leetcode-cn.com/tag/backtracking)

## 相似題目

- [下一个排列](../next-permutation/README.md)
- [全排列](../permutations/README.md)
- [回文排列 II](../palindrome-permutation-ii/README.md)
- [正方形数组的数目](../number-of-squareful-arrays/README.md)
