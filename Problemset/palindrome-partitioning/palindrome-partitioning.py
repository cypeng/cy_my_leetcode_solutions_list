
# @Title: 分割回文串 (Palindrome Partitioning)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-04 15:10:59
# @Runtime: 292 ms
# @Memory: 29.5 MB

def checkPalindrome(s):
    idx, jdx = 0, len(s)-1
    while idx <= jdx:
        if s[idx] == s[jdx]:
            idx += 1
            jdx -= 1
        else:
            return False
    return True

def backtrack(idx,s,case,ans):
    #print(idx,case, len(case), ans)
    if (idx > len(s)-1):
        if len(case) > 0:
            flag = True
            for s in case:
                if not checkPalindrome(s):
                    flag = False
                    break
            if flag:
                ans.append(case)
            #print(idx,ans)
        return True
    this_case = case.copy()
    this_case.append(s[idx])
    backtrack(idx+1,s,this_case,ans)

    if len(case) > 0:
        this_case = case.copy()
        this_str = this_case[-1]
        this_case[-1] = this_str+s[idx]
        backtrack(idx+1,s,this_case,ans)
    
class Solution:
    def partition(self, s: str) -> List[List[str]]:
        ans = []
        backtrack(0,s,[],ans)
        return ans
