
# @Title: Add Two Numbers (Add Two Numbers)
# @Author: pcysl
# @Date: 2019-07-08 18:26:11
# @Runtime: 80 ms
# @Memory: 13.4 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None
def add_main(a, b):
    flag_a, flag_b, flag_d = bool(0), bool(0), bool(0)
    d = 0
    iniFlag = bool(1)
    
    while (1):
      a_val, b_val = 0, 0
      if a is not None:
          a_val = a.val
          a = a.next
      else:
          flag_a = bool(1)
      if b is not None:
         b_val = b.val
         b = b.next
      else:
          flag_b = bool(1)
      if d is 0:
          flag_d = bool(1)
      else:
          flag_d = bool(0)
      if (flag_a & flag_b & flag_d):
          break
      if (iniFlag):
          sum_val = (a_val+b_val)% 10
          if (a_val+b_val) >= 10:
              d = 1
          else:
              d = 0
          c = ListNode(sum_val)
          answer = c
          iniFlag = bool(0)
      else:
          sum_val = (a_val+b_val + d) % 10
          if (a_val+b_val + d) >= 10:
              d = 1
          else:
              d = 0
          nc = ListNode(sum_val)
          c.next = nc
          c = c.next
    return answer
        
class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        ans = add_main(l1, l2)
        return ans
        
        
