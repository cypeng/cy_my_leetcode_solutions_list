
# @Title: 找不同 (Find the Difference)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-15 00:01:41
# @Runtime: 56 ms
# @Memory: 14.9 MB

class Solution:
    def findTheDifference(self, s: str, t: str) -> str:
        hashset_s = [0]*26
        hashset_t = [0]*26
        for idx, char in enumerate(t):
            if idx <= len(s)-1:
                hashset_s[ord(s[idx])-ord('a')] += 1
                hashset_t[ord(s[idx])-ord('a')] -= 1
            hashset_t[ord(t[idx])-ord('a')] += 1
            hashset_s[ord(t[idx])-ord('a')] -= 1
        #print(hashset_t, hashset_s) 
        for idx,number in enumerate(hashset_t):
            if number == 1:
                return chr(idx+ord('a'))
