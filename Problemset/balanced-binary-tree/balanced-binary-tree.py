
# @Title: 平衡二叉树 (Balanced Binary Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-10 14:33:57
# @Runtime: 80 ms
# @Memory: 18.5 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

def getHeight(root):
    if not root:
        return 0
    return max(getHeight(root.left), getHeight(root.right)) + 1
                
class Solution:
    def isBalanced(self, root: TreeNode) -> bool:
        if root is None:
            return True
        return abs(getHeight(root.left)-getHeight(root.right)) <= 1 and self.isBalanced(root.left) and self.isBalanced(root.right)
