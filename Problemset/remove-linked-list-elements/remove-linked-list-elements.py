
# @Title: 移除链表元素 (Remove Linked List Elements)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-26 22:41:59
# @Runtime: 80 ms
# @Memory: 17.8 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:
        LL1, LL2, LL = None, None, head
        head = None
        iniFlag = True
        while 1:
            if LL is None:
                if LL1:
                    LL1.next = None
                break
            else:    
                if not LL.val == val:
                    if iniFlag:
                        head = LL
                        iniFlag = False
                    if not LL1 is None:
                        LL1.next = LL
                    LL1 = LL
                while LL:
                    LL = LL.next
                    if LL is None or not LL.val == val:
                        break
                         
                
        return head


