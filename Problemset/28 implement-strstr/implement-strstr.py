
# @Title: Implement strStr() (Implement strStr())
# @Author: pcysl
# @Date: 2017-12-16 11:49:17
# @Runtime: 45 ms
# @Memory: N/A

class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        answer = -1
        if (len(needle) <= len(haystack)):
            for idx in range(0,len(haystack)-len(needle)+1):
                jdx = idx
                while ((jdx-idx) <= len(needle)-1 ) and (haystack[jdx] == needle[jdx-idx]):
                    if ((jdx-idx) == len(needle)-1):
                        answer = idx
                        break
                    jdx = jdx+1
                if (answer != -1):
                    break
            if (len(needle) == 0):
                answer = 0
        return answer
                
                
            
