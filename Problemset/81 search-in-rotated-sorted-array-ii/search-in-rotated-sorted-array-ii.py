
# @Title: Search in Rotated Sorted Array II (Search in Rotated Sorted Array II)
# @Author: pcysl
# @Date: 2017-12-26 18:49:18
# @Runtime: 65 ms
# @Memory: N/A

class Solution:
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: bool
        """
        answer = bool(0)
        idx = 0
        jdx = len(nums)-1
        while (idx <= jdx):
            if (nums[idx] == target):
                answer = bool(1)
                return answer
            elif (nums[jdx] == target):
                answer = bool(1)
                return answer
        
            idx = idx+1
            jdx = jdx-1
        
        return answer
