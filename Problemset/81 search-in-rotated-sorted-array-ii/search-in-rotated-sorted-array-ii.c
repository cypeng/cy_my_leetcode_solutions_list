
// @Title: Search in Rotated Sorted Array II (Search in Rotated Sorted Array II)
// @Author: pcysl
// @Date: 2017-12-26 18:54:04
// @Runtime: 3 ms
// @Memory: N/A

bool search(int* nums, int numsSize, int target) {
    int idx = 0;
    int jdx = numsSize-1;
    bool answer = 0;
    
    while (idx <= jdx)
    {
        if (nums[idx] == target){answer = 1;}
        if (nums[jdx] == target){answer = 1;}
        
        idx = idx+1;
        jdx = jdx-1;
    }
    return answer;
}
