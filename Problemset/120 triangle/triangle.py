
# @Title: Triangle (Triangle)
# @Author: pcysl
# @Date: 2018-12-07 11:30:53
# @Runtime: 100 ms
# @Memory: 8.5 MB

def find_min_path_for_triangle(triangle, last_triangle_array, this_idx):
    min_path = [None]
    if (this_idx == len(triangle)-1):
        find_array_path_sum(min_path, triangle, last_triangle_array, this_idx, 1)
    else:
        find_array_path_sum(min_path, triangle, last_triangle_array, this_idx, 0)
    
    return min_path[0]

def find_array_path_sum(min_path, triangle, last_triangle_array, this_idx, end_flag):
    this_array = triangle[this_idx][:]
    this_path_array = [None]*len(this_array)
    idx, jdx = 0, len(last_triangle_array)-1
    while (idx <= jdx):
        this_path_array = one_point_path_sum(this_path_array, this_array, last_triangle_array, idx)
        if (idx < jdx):
            this_path_array = one_point_path_sum(this_path_array, this_array, last_triangle_array, jdx)
        idx+=1
        jdx-=1
    
    if (end_flag):
        idx, jdx = 1, len(this_path_array)-1
        min_path[0] = this_path_array[0]
        while (idx <= jdx):
            if (this_path_array[idx] < min_path[0]):
                min_path[0] = this_path_array[idx]
            if (this_path_array[jdx] < min_path[0]):
                min_path[0] = this_path_array[jdx]
            idx+=1
            jdx-=1
    
    if (this_idx+1 <= len(triangle)-1):                
        if (this_idx+1 == len(triangle)-1):
            find_array_path_sum(min_path, triangle, this_path_array, this_idx+1, 1)
        else:
            find_array_path_sum(min_path, triangle, this_path_array, this_idx+1, 0)

def compare_path_sum(this_path_array, path_sum, array_idx):
    if (this_path_array[array_idx] == None):
        this_path_array[array_idx] = path_sum
    else:
        if (path_sum < this_path_array[array_idx]):
            this_path_array[array_idx] = path_sum
    return this_path_array

def one_point_path_sum(this_path_array, this_array, last_triangle_array, this_idx):
    temp1 = last_triangle_array[this_idx] + this_array[this_idx]
    this_path_array = compare_path_sum(this_path_array, temp1, this_idx)
    
    if (this_idx +1 <= len(this_array)-1):
        temp2 = last_triangle_array[this_idx] + this_array[this_idx+1]
        this_path_array = compare_path_sum(this_path_array, temp2, this_idx+1)
    return this_path_array

class Solution:
    def minimumTotal(self, triangle):
        """
        :type triangle: List[List[int]]
        :rtype: int
        """
        if (len(triangle) > 0):
            if (len(triangle) < 2):
                return triangle[0][0]
            else:
                return find_min_path_for_triangle(triangle, triangle[0], 1)
        else:
            return 0
        
