
# @Title: 打开转盘锁 (Open the Lock)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-03 22:51:53
# @Runtime: 1416 ms
# @Memory: 16 MB


def digit_step(digit, move_step):
    return str((digit + move_step) %10)

def traversal(roots, target, step, deadends, seens, flag):
    next_roots = []
    next_step_list = []
    for idx, root in enumerate(roots):
        if root == target:
            #print(root)
            flag[0] = False
            return step, flag
        if root in deadends:
            continue
        this_digit = []
        this_digit[:0] = root
        for idx in range(0, 4):
            temp1 = this_digit.copy()
            temp2 = this_digit.copy()
            unit_digit = int(root[idx])
            temp1[idx] = digit_step(unit_digit, 1)
            this_case = ''.join(temp1)
            if not this_case in seens:
                seens.add(this_case)
                next_roots.append(this_case)
            temp2[idx] = digit_step(unit_digit, -1)
            this_case = ''.join(temp2)
            if not this_case in seens:
                seens.add(this_case)
                next_roots.append(this_case)
        #if '0000' == root:
    #print(next_roots, temp1, temp2)
    if flag[0] and len(next_roots) > 0:
        step, flag = traversal(next_roots, target, step+1, deadends, seens, flag)
    if flag[0] and len(next_roots) == 0:
        step = -1
    return step, flag

class Solution:
    def openLock(self, deadends: List[str], target: str) -> int:
        step, _ = traversal(['0000'], target, 0, deadends, {'0000'}, [True])
        return step
