
# @Title: 反转链表 (Reverse Linked List)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-26 15:46:07
# @Runtime: 52 ms
# @Memory: 15.6 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        if not head:
            return head
        if not head.next:
            return head
        LL1, LL2, LL3 = head, head.next, None
        if head.next.next:
            LL3 = head.next.next
            LL = LL3
            LL1.next = None
        else:
            LL2.next = LL1
            LL1.next = None
            head = LL2
            return head
        while 1:
            if not LL2:
                head = LL1
                break
            else:
                #print(LL1, LL2, LL3)
                LL2.next = LL1
                if LL:
                    LL = LL.next
                    LL1, LL2, LL3 = LL2, LL3, LL
                else:
                    LL1, LL2, LL3 = LL2, LL3, LL

        return head
