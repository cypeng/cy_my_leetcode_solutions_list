
# @Title: Pascal's Triangle (Pascal's Triangle)
# @Author: pcysl
# @Date: 2018-12-07 11:51:25
# @Runtime: 36 ms
# @Memory: 7.1 MB

def get_Pascal(numRows):
    Pascal = [None]*numRows
    for idx in range(0, numRows):
        if (idx <= 1):
            sub_array = [1]*(idx+1)
        else:
            last_sub_array = Pascal[idx-1]
            sub_array = [1]*(idx+1)
            for jdx in range(1, len(sub_array)-1):
                sub_array[jdx] = last_sub_array[jdx]+last_sub_array[jdx-1]
        Pascal[idx] = sub_array
    return Pascal

class Solution:
    def generate(self, numRows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """
        Pascal = get_Pascal(numRows)
        return Pascal
