
# @Title: Roman to Integer (Roman to Integer)
# @Author: pcysl
# @Date: 2018-07-15 18:51:18
# @Runtime: 132 ms
# @Memory: N/A

class Solution:
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        roman = self.romanDict()
        sumNum= 0
        idx = 0
        while (idx <= len(s)-1):
            jdx = idx +1
            
            if (jdx <= len(s) - 1):
                #print(idx, jdx, s[idx], s[jdx] ,roman[s[jdx]] > roman[s[idx]])
                if (roman[s[jdx]] > roman[s[idx]]):
                    sumNum = sumNum + roman[s[jdx]] - roman[s[idx]]
                    idx += 2           
                else:
                    sumNum = sumNum + roman[s[idx]]
                    idx += 1
            else:
                sumNum = sumNum + roman[s[idx]]
                idx += 1
        
        return sumNum
            
    def romanDict(self):
        roman = {}
        roman['I']  = 1
        roman['V']  = 5
        roman['X']  = 10
        roman['L']  = 50
        roman['C']  = 100
        roman['D']  = 500
        roman['M']  = 1000
        return roman
