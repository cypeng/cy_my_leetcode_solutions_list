
# @Title: 单词拆分 (Word Break)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-09 00:03:17
# @Runtime: 48 ms
# @Memory: 15 MB

class Solution:
    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        dp = [0]*(len(s)+1)
        dp[0] = 1
        for idx in range(0,len(s)):
            for jdx in range(idx+1,len(s)+1):
                if (dp[idx] == 1) and (s[idx:jdx] in wordDict):
                    dp[jdx] = 1
        #print(dp)
        if dp[-1] == 1:
            return True
        else:
            return False
