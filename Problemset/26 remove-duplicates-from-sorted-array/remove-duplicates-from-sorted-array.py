
# @Title: Remove Duplicates from Sorted Array (Remove Duplicates from Sorted Array)
# @Author: pcysl
# @Date: 2017-12-15 13:45:40
# @Runtime: 99 ms
# @Memory: N/A

class Solution:
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        idx = 1
        jdx = idx
        if (len(nums) > 0):
            repnum = nums[0]
            answer = 1
            while (idx < len(nums)):
                if (nums[idx] != repnum):
                    nums[jdx] = nums[idx]
                    repnum = nums[idx]
                    jdx = jdx+1
                    answer = jdx
                idx = idx +1
        else:
            answer = 0
        return answer
