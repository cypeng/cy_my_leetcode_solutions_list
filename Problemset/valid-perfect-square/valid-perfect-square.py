
# @Title: 有效的完全平方数 (Valid Perfect Square)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-17 23:27:36
# @Runtime: 44 ms
# @Memory: 15 MB

class Solution:
    def isPerfectSquare(self, num: int) -> bool:
        idx, jdx = 0, num
        while 1:
            mid = int((idx+jdx)*0.5)
            if (mid*mid) == num:
                return True
            elif ((mid*mid) < num) and ((mid+1)*(mid+1) > num):
                return False
            if mid*mid > num:
                jdx = mid-1
            else:
                idx = mid+1
