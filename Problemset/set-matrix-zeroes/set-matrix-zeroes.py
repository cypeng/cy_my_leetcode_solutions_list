
# @Title: 矩阵置零 (Set Matrix Zeroes)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-05 00:49:26
# @Runtime: 40 ms
# @Memory: 15.2 MB

class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        set_zero = set()
        for idx in range(0, len(matrix)):
            for jdx in range(0, len(matrix[idx])):
                if matrix[idx][jdx] == 0:
                    set_zero.add((idx,jdx))
        #print(set_zero)
        for coord in set_zero:
            for count in range(0, len(matrix)):
                matrix[count][coord[1]] = 0
            for count in range(0, len(matrix[0])):
                matrix[coord[0]][count] = 0
