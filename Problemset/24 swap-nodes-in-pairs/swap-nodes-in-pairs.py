
# @Title: Swap Nodes in Pairs (Swap Nodes in Pairs)
# @Author: pcysl
# @Date: 2019-07-11 16:52:17
# @Runtime: 24 ms
# @Memory: 13 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None
def swap_case(l):
    if l is None:
        return 0
    if l and l.next is None:
        return 1
    if l and l.next:
        return 2

def swap_init(l):
    nl = None
    if l.next.next:
        nl = l.next.next
    a = l.next
    b = l
    answer = a
    answer_head = answer
    answer.next = b
    answer = answer.next
    answer.next =None
    return answer_head, answer, nl

def swap(ans, l):
    nl = None
    if l.next.next:
        nl = l.next.next
    a = l.next
    b = l
    ans.next = a
    ans.next.next = b
    ans = ans.next.next
    ans.next = None
    return ans, nl
    
def pair_swap(l):
    case = swap_case(l)
    if case == 0 or case == 1:
        return l
    answer_head, answer, l = swap_init(l)
    
    while (1):
        case = swap_case(l)
        if case == 0:
            answer.next = None
            return answer_head
        if case == 1:
            answer.next = l
            answer.next.next = None
            return answer_head
        if case == 2:
            answer, l = swap(answer, l)
    return answer_head

class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        return pair_swap(head)
        
