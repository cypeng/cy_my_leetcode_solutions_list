
# @Title: 二叉搜索树的最近公共祖先 (Lowest Common Ancestor of a Binary Search Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-09 00:15:18
# @Runtime: 88 ms
# @Memory: 18.8 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        if not root:
            return None

        if (p.val <= root.val and q.val >= root.val) or (q.val <= root.val and p.val >= root.val):
            return root

        if (p.val < root.val and q.val < root.val):
            return self.lowestCommonAncestor(root.left, p, q)

        if (p.val > root.val and q.val > root.val):
            return self.lowestCommonAncestor(root.right, p, q)
