
# @Title: Remove Duplicates from Sorted List II (Remove Duplicates from Sorted List II)
# @Author: pcysl
# @Date: 2019-07-12 12:44:05
# @Runtime: 48 ms
# @Memory: 13.3 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

def find_top(head):
    count = 0
    while (1):
        count += 1
        if head is None:
            return None, None, count
        if head.next is None:
            return head, None, count
        if head.val != head.next.val:
            tail = head
            next_head = head.next
            return tail, next_head, count
        head = head.next

def remain_only_one(l):
    if l is None or l.next is None:
        return l
    ans = None 
    while (1):
        if l is None:
            return ans_head
        tail, next_head, count = find_top(l)
        if ans is None and count >= 1:
            ans = tail
            ans_head = ans
            ans.next = None
        elif count >= 1:
            ans.next = tail
            ans = ans.next
            ans.next = None
        l = next_head
    
def remove_all_duplicated(l):
    if l is None or l.next is None:
        return l
    ans = None 
    while (1):
        if l is None:
            return ans_head
        tail, next_head, count = find_top(l)
        if ans is None and count == 1:
            ans = tail
            ans_head = ans
            ans.next = None
        elif ans is None and count > 1:
            ans_head = None
        elif count == 1:
            ans.next = l
            ans = ans.next
            ans.next = None
        l = next_head

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        return remove_all_duplicated(head)
