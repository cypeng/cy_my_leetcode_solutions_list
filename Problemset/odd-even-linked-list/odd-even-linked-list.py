
# @Title: 奇偶链表 (Odd Even Linked List)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-27 00:57:09
# @Runtime: 52 ms
# @Memory: 16.7 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def oddEvenList(self, head: ListNode) -> ListNode:
        if head is None:
            return head
        count = 1
        LL1, LL2, LLE, LL = None, None, None, head 
        while LL:
            if count == 1:
                LL1 = LL
                head = LL1
                LL = LL.next
                LL1.next = None
                #print(head)
            elif count == 2:
                LL2 = LL
                LLE = LL2
                LL = LL.next
                LL2.next = None
                #print(LL2)
            else:
                if count % 2 == 1:
                    LL1.next = LL
                    #print(LL1)
                    LL1 = LL1.next
                    #print(LL1)
                    LL = LL.next
                    LL1.next = None
                    #print(LL1)
                else:
                    LL2.next = LL
                    LL2 = LL2.next
                    LL = LL.next
                    LL2.next = None
            count += 1
        
        LL1.next = LLE
        #print(head, LL1, LLE)
        return head
