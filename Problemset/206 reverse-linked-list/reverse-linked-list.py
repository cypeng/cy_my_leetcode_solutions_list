
# @Title: Reverse Linked List (Reverse Linked List)
# @Author: pcysl
# @Date: 2018-01-03 14:05:21
# @Runtime: 46 ms
# @Memory: N/A

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if (head == None):
            return head
        lasttemp = None
        Flag     = 0
        while (Flag == 0):
            temp0 = head
            if (head.next != None):
                head = head.next
            else:
                Flag = 1
            temp0.next = lasttemp
            lasttemp = temp0

                
        return head
            
