
# @Title: 重排链表 (Reorder List)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-02 00:00:43
# @Runtime: 100 ms
# @Memory: 23 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
def ListNodeLength(head):
    count = 0
    while head:
        head = head.next
        count += 1
    return count

def ReverseAfterMiddle(head, midIdx):
    count = 0
    pre = None
    while head:
        cur = head
        head = head.next
        count += 1
        if count > midIdx+1:
            if pre and pre.next == cur:
                pre.next = None
                cur.next = None
            else:
                cur.next = pre
        pre = cur
    return pre

def reorderLL(head, tail):
    
    while 1:
        if head:
            cur_head = head
        else:
            break
        if tail:
            cur_tail = tail
        else:
            break
        head = head.next
        tail = tail.next
        cur_head.next = cur_tail
        cur_tail.next = head
        pre_head = cur_head
        pre_tail = cur_tail

class Solution:
    def reorderList(self, head: ListNode) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        LLn = ListNodeLength(head)
        if (LLn == 1) or (LLn == 2):
            return 0
        tail = ReverseAfterMiddle(head, LLn//2)
        #print(tail, head)
        reorderLL(head, tail)
