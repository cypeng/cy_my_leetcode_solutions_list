
# @Title: 最长回文子串 (Longest Palindromic Substring)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-14 00:20:41
# @Runtime: 9196 ms
# @Memory: 14.7 MB

class Solution:
    def longestPalindrome(self, s: str) -> str:
        if len(s) <= 1:
            return s
        idx = 0
        opt = ''
        stride = len(s)-idx
        while idx <= len(s)-1:
            idx = 0
            jdx = idx+stride
            while jdx <= len(s):
                this_str = s[idx:jdx]
                if this_str == this_str[::-1]:
                    return this_str
                idx +=1
                jdx = idx+stride
            stride -= 1
        return opt
