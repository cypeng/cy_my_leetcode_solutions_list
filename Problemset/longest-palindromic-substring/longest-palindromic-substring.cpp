
// @Title: 最长回文子串 (Longest Palindromic Substring)
// @Author: pcysl@hotmail.com
// @Date: 2021-07-13 23:48:57
// @Runtime: 176 ms
// @Memory: 7 MB

class Solution {
public:
    string longestPalindrome(string s) {
        //using namespace std;
        //using std::string;
        int idx = 0; 
        int s_n = s.length();
        int step = s_n-1-idx;
        int kdx = 0;
        int ldx = 0;
        while (idx <= s_n-1){
             if (s[idx] == s[idx+step]){
                 kdx = idx;
                 ldx = idx+step;
                 //cout << kdx << endl;
                 //cout << ldx << endl;
                 
                 while (kdx <= ldx){
                     if (s[kdx] != s[ldx]){
                         break;
                     }
                     kdx+=1;
                     ldx-=1;
                     if (kdx > ldx){
                         return s.substr(idx,step+1);
                     }
                 }
             }
            idx += 1;
            if ((idx + step) > (s_n-1)){
                step -= 1;
                idx = 0;
            }
        }
        return s.substr(0,1);
    }
};
