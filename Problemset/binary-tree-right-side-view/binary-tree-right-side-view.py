
# @Title: 二叉树的右视图 (Binary Tree Right Side View)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-02 11:00:46
# @Runtime: 48 ms
# @Memory: 15.1 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def traversal(roots, ans):
    this_roots = []
    for root in roots:
        if root.left:
            this_roots.append(root.left)
        if root.right:
            this_roots.append(root.right)
    if len(this_roots) > 0:
        ans.append(this_roots[-1].val)
        traversal(this_roots, ans)
    

class Solution:
    def rightSideView(self, root: TreeNode) -> List[int]:
        ans = []
        if root:
            ans.append(root.val)
            traversal([root], ans)
        return ans
