
# @Title: Implement Stack using Queues (Implement Stack using Queues)
# @Author: pcysl
# @Date: 2018-02-06 00:02:02
# @Runtime: 56 ms
# @Memory: N/A

class MyStack:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.data = []

    def push(self, x):
        """
        Push element x onto stack.
        :type x: int
        :rtype: void
        """
        self.data.append(x)

    def pop(self):
        """
        Removes the element on top of the stack and returns that element.
        :rtype: int
        """
        rx = self.data[len(self.data)-1]
        del self.data[len(self.data)-1]
        return rx

    def top(self):
        """
        Get the top element.
        :rtype: int
        """
        return self.data[len(self.data)-1]

    def empty(self):
        """
        Returns whether the stack is empty.
        :rtype: bool
        """
        if (len(self.data)>0):
            return bool(0)
        else:
            return bool(1)


# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()
