
# @Title: 无重复字符的最长子串 (Longest Substring Without Repeating Characters)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-28 01:02:10
# @Runtime: 112 ms
# @Memory: 15.1 MB

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        if len(s) == 0:
            return 0
        idx, jdx = 0, 1
        opt = 0
        while 1:
            substr = s[idx:jdx]
            #print(substr,idx,jdx)
            if len(s)-idx < opt:
                break
            if len(substr) >= opt:
                opt = len(substr)
            if len(substr) >= 1:
                if ((jdx <= len(s)-1) and (s[jdx] in substr)) or (jdx == len(s)):
                    idx += 1
                    if idx == jdx:
                        jdx += 1
                else:
                    jdx += 1
        return opt
