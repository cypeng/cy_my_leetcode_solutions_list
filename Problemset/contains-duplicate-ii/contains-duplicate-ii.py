
# @Title: 存在重复元素 II (Contains Duplicate II)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-27 00:16:54
# @Runtime: 96 ms
# @Memory: 26 MB

class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        hashmap = {}
        for idx in range(0, len(nums)):
            if (not nums[idx] in hashmap):
                hashmap[nums[idx]] = idx
            else:
                if (idx - hashmap[nums[idx]] <= k):
                    return True
                else:
                    hashmap[nums[idx]] = idx
        return False
