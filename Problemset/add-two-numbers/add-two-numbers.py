
# @Title: 两数相加 (Add Two Numbers)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-07 01:08:11
# @Runtime: 68 ms
# @Memory: 15 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
def ListNodeSumInit(l1, l2, bit):
    if (l1 is None) and l2:
        l3 = ListNode(val=(l2.val+bit)%10, next=None)
        bit = (l2.val+bit)//10
    elif (l2 is None) and l1:
        l3 = ListNode(val=(l1.val+bit)%10, next=None)
        bit = (l1.val+bit)//10
    elif (l1 is None) and (l2 is None):
        l3 = ListNode(val=(bit)%10, next=None)
        bit = (bit)//10
    else:
        l3 = ListNode(val=(l1.val+l2.val+bit)%10, next=None)
        bit = (l1.val+l2.val+bit)//10
    return l3, bit

def ListNodeSum(l1, l2, l3, next_bit):
    ini_flag = True
    l3n = l3
    while 1:
        if l1 is not None:
            l1 = l1.next
        if l2 is not None:
            l2 = l2.next
        if (l1 is None) and (l2 is None):
            if next_bit > 0:
                l3n.next, next_bit = ListNodeSumInit(l1, l2, next_bit)
                l3n = l3n.next
            break
        
        if ini_flag:
            l3.next, next_bit = ListNodeSumInit(l1, l2, next_bit)
            l3n = l3.next
            ini_flag = False
        else:
            l3n.next, next_bit = ListNodeSumInit(l1, l2, next_bit)
            l3n = l3n.next
    return l3            

class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        if l1 is None:
            return l2
        if l2 is None:
            return l1
        l3, next_bit= ListNodeSumInit(l1, l2, 0)
        l3 = ListNodeSum(l1, l2, l3, next_bit)
        return l3



