
# @Title: 最小路径和 (Minimum Path Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-16 15:11:19
# @Runtime: 60 ms
# @Memory: 15.9 MB

class Solution:
    def minPathSum(self, grid: List[List[int]]) -> int:
        n, m = len(grid[0]), len(grid)
        #print(m, n)
        for idx in range(0, m):
            for jdx in range(0, n):
                #print(idx, jdx)
                if (idx == 0) and (jdx > 0):
                    grid[idx][jdx] += grid[idx][jdx-1]
                elif (jdx == 0) and (idx > 0):
                    grid[idx][jdx] += grid[idx-1][jdx]
                elif (idx > 0) and (jdx > 0):
                    if grid[idx][jdx] + grid[idx-1][jdx] < grid[idx][jdx] + grid[idx][jdx-1]:
                         grid[idx][jdx] += grid[idx-1][jdx]
                    else:
                        grid[idx][jdx] += grid[idx][jdx-1]
        #print(grid)
        return grid[-1][-1]
