
# @Title: Search a 2D Matrix (Search a 2D Matrix)
# @Author: pcysl
# @Date: 2017-12-26 17:49:18
# @Runtime: 75 ms
# @Memory: N/A

class Solution:
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        for iMatIdx in range(0,len(matrix)):
            nums = matrix[iMatIdx]
            idx = 0
            jdx = len(nums)-1
            while (idx <= jdx) and (target >= nums[0]) and (target <= nums[-1]):
                midIdx = int((idx+jdx)*0.5)
                if (nums[midIdx] == target):
                    return bool(1)
                elif (nums[midIdx] < target):
                    idx = midIdx+1
                elif (nums[midIdx] > target):
                    jdx = midIdx-1
    
        return bool(0)
