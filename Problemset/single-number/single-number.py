
# @Title: 只出现一次的数字 (Single Number)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-26 11:48:26
# @Runtime: 64 ms
# @Memory: 16.6 MB

class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        hashset = set()
        idx, jdx = 0, len(nums)-1
        while idx <= jdx:
            if idx == jdx:
                if nums[idx] in hashset:
                    hashset.remove(nums[idx])
                else:
                    return nums[idx]
            else:
                if not nums[idx] in hashset:
                    hashset.add(nums[idx])
                elif nums[idx] in hashset:
                    hashset.remove(nums[idx])

                if not nums[jdx] in hashset:
                    hashset.add(nums[jdx])
                elif nums[jdx] in hashset:
                    hashset.remove(nums[jdx])
            idx += 1
            jdx -= 1
        for item in hashset:
            return item

