
# @Title: 外观数列 (Count and Say)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-02 15:32:52
# @Runtime: 68 ms
# @Memory: 15.1 MB

class Solution:
    def countAndSay(self, n: int) -> str:
        if n == 1:
            return "1"
        else:
            preStr = self.countAndSay(n-1)
            idx, jdx = 0, 0
            newStr = ''
            while jdx <= len(preStr)-1:
                count = 0
                while (jdx <= len(preStr)-1) and (preStr[idx] == preStr[jdx]):
                    #print(n, idx,jdx,count)
                    count += 1
                    jdx += 1
                newStr += str(count)+preStr[idx]
                idx = jdx
            return newStr
