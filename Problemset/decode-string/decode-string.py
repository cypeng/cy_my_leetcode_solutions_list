
# @Title: 字符串解码 (Decode String)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-05 18:13:48
# @Runtime: 48 ms
# @Memory: 14.9 MB

class stack:
    def __init__(self, size):
        self.stack = [None]*size
        self.idx = -1
    
    def push(self,val):
        self.idx += 1
        self.stack[self.idx] = val
        #print('push', self.stack)
    
    def pop(self):
        if self.idx < 0:
            return None
        ans = self.stack[self.idx] 
        self.stack[self.idx] = None
        self.idx -= 1
        #print('pop', self.stack)
        return ans
    
    def empty(self):
        if self.idx < 0:
            return True
        return False

def operation(this_str, num):
    sum_str = ''
    for idx in range(0, num):
        sum_str = sum_str + this_str
    return sum_str

class Solution:
    def decodeString(self, s: str) -> str:
        ds = stack(len(s))
        flag = False
        ans_str = ''
        for idx in range(0, len(s)):
            char = s[idx]
            if not flag:
                if char.isalpha():
                    ans_str = ans_str + char
                    flag = False
                else:
                    flag = True
                    num_str = ''
                    this_str = ''
            
            if flag:
                if char.isdigit():
                    num_str = num_str + char
                if char.isalpha():
                    ds.push(char)
                if char == '[':
                    ds.push(int(num_str))
                    ds.push('[')
                    num_str = ''
                if char == ']':
                    this_str = ''
                    pop_str = ''
                    while not pop_str == '[':
                        this_str = pop_str + this_str
                        pop_str = ds.pop()
                    dup_num = ds.pop()
                    if ds.empty():
                        ans_str = ans_str + operation(this_str,dup_num)
                        flag = False
                    else:
                        ds.push(operation(this_str,dup_num))
                    #print(this_str, num_str)
                    
        return ans_str
