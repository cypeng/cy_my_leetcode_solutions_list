
# @Title: 四数相加 II (4Sum II)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-29 00:25:04
# @Runtime: 788 ms
# @Memory: 15.1 MB

class Solution:
    def fourSumCount(self, nums1: List[int], nums2: List[int], nums3: List[int], nums4: List[int]) -> int:
        hashmap12 = {}
        for u in nums1:
            for v in nums2:
                if not u+v in hashmap12:
                    hashmap12[u+v] = 1
                else:
                    hashmap12[u+v] += 1
        
        ans = 0
        for u in nums3:
            for v in nums4:
                if -u-v in hashmap12:
                    ans += hashmap12[-u-v]
        return ans
                
