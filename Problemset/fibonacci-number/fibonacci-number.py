
# @Title: 斐波那契数 (Fibonacci Number)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-13 11:28:16
# @Runtime: 40 ms
# @Memory: 14.7 MB

def calFib(n, catch):
    if n in catch:
        return catch[n]
        
    if n == 0:
        catch[0] = 0
        return 0
    elif n == 1:
        catch[1] = 1
        return 1
    else:
        catch[n] = calFib(n - 1,catch) + calFib(n - 2,catch)
        return catch[n]

class Solution:
    def fib(self, n: int) -> int:
        return calFib(n, {})
        
