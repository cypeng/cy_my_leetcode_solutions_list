
# @Title: Same Tree (Same Tree)
# @Author: pcysl
# @Date: 2018-02-13 13:41:24
# @Runtime: 63 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def isSameTree(self, p, q):
        """
        :type p: TreeNode
        :type q: TreeNode
        :rtype: bool
        """
        self.flag = 0
        self.ans = []
        self.ord = []
        self.InorderTree(p)
        pTree = self.ans
        pOrder= self.ord

        self.flag = 0
        self.ans = []
        self.ord = []
        self.InorderTree(q)
        qOrder= self.ord
        qTree = self.ans
         
        print(pTree,qTree,pOrder,qOrder)
        if (pTree != qTree) or (pOrder != qOrder):
            return bool(0)
        else:
            return bool(1)

    def InorderTree(self,root):
        if (root):
            if (root.left):
                self.flag = 1
                self.InorderTree(root.left)
            self.ord.append(self.flag)
            self.ans.append(root.val)
            if (root.right):
                self.flag = 2
                self.InorderTree(root.right)

            
