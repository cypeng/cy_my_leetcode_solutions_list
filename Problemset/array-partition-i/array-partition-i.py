
# @Title: 数组拆分 I (Array Partition I)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-24 10:48:28
# @Runtime: 72 ms
# @Memory: 16.6 MB

class Solution:
    def arrayPairSum(self, nums: List[int]) -> int:
        nums.sort()
        idx, jdx = 0, len(nums)-2
        sum_nums = 0
        while idx <= jdx:
            #print(nums, idx, jdx)
            if idx == jdx:
                sum_nums += nums[idx]
                return sum_nums
            else:
                sum_nums += nums[idx]
                sum_nums += nums[jdx]
            idx+=2
            jdx-=2
        return sum_nums
