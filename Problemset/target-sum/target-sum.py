
# @Title: 目标和 (Target Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-05 14:08:14
# @Runtime: 1532 ms
# @Memory: 15.5 MB


def traversal(nums, sums, idx, size, counts, target):
    next_sums = []
    num = nums[idx]
    next_counts = {}
    for this_sum in sums:
        count = counts[this_sum]
        a = this_sum +1*num
        b = this_sum -1*num
        if idx <= size-1:
            if not a in next_sums:
                next_sums.append(a)
                next_counts[a] = count
            else:
                next_counts[a] = count+next_counts[a]
            if not b in next_sums:
                next_sums.append(b)
                next_counts[b] = count
            else:
                next_counts[b] = next_counts[b]+count
    if idx == size-1:
        if target in next_counts:
            count = next_counts[target]
        else:
            count = 0    
    if idx+1 <= size-1:
        count = traversal(nums, next_sums, idx+1, size, next_counts, target)   
    return count     

class Solution:
    def findTargetSumWays(self, nums: List[int], target: int) -> int:
        size = len(nums)
        if size < 1:
            return 0
        dup_counts = {}
        dup_counts[0] = 1 
        count = traversal(nums, [0], 0, size, dup_counts, target)
        return count

