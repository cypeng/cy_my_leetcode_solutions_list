
# @Title: Binary Tree Level Order Traversal II (Binary Tree Level Order Traversal II)
# @Author: pcysl
# @Date: 2018-02-15 11:56:45
# @Runtime: 72 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def levelOrderBottom(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        
        self.ans = []
        if (root):
            self.level = []
            self.level.append(root)
            while (self.level):
                thislevel = []
                thislevel = self.level
                self.level = []
                self.leveldata = []
                for idx in range(0,len(thislevel)):
                    root = thislevel[idx]
                    self.leveltree(root)
                self.ans.append(self.leveldata)
        
        return self.ans[::-1]
        
    def leveltree(self, root):
        self.leveldata.append(root.val)
        if (root.left):
            self.level.append(root.left)
        if (root.right):
            self.level.append(root.right)
        
