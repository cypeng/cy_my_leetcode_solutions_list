
# @Title: 两数之和 (Two Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-06 23:44:26
# @Runtime: 484 ms
# @Memory: 14.9 MB

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        output = []
        if len(nums) < 2:
            return output
        if len(nums) == 2:
            if nums[0]+nums[1] == target:
                return [0, 1]
            else:
                return []
        
        kdx = len(nums)-1
        idx, jdx = 0, kdx-1
        while 1:
            this_target = target - nums[kdx] 
            flag = False 
            while idx < jdx:
                if this_target - nums[idx] == 0:
                    output.append(idx)
                    flag = True
                if this_target - nums[jdx] == 0:
                    output.append(jdx)
                    flag = True
                idx += 1
                jdx -= 1
                if idx == jdx:
                    this_target = target - nums[kdx] 
                    if this_target - nums[idx] == 0:
                        output.append(idx)
                        output.append(kdx)
            if flag:
                output.append(kdx)
            kdx = kdx-1
            idx, jdx = 0, kdx-1
            if idx == jdx:
                this_target = target - nums[kdx] 
                if this_target - nums[idx] == 0:
                    output.append(idx)
                    output.append(kdx)
                break
        return output


        
        
            
            

