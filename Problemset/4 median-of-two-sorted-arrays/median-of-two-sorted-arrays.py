
# @Title: Median of Two Sorted Arrays (Median of Two Sorted Arrays)
# @Author: pcysl
# @Date: 2017-12-19 00:29:22
# @Runtime: 268 ms
# @Memory: N/A

class Solution(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        nums = sorted(nums1+nums2)
        if (len(nums)%2 == 1):
            mididx = (len(nums)-1)/2
            answer = nums[mididx]
        else:
            mididx = len(nums)/2
            answer = (nums[mididx]+nums[mididx-1])*0.5
        print(nums,mididx,nums[mididx])
        return answer
