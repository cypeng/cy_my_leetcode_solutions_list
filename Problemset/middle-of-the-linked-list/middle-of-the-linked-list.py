
# @Title: 链表的中间结点 (Middle of the Linked List)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-06 21:40:22
# @Runtime: 32 ms
# @Memory: 14.9 MB

# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def middleNode(self, head: ListNode) -> ListNode:
        temp = head
        count = 0
        while temp:
            count += 1
            temp = temp.next
        if count%2 == 1:
            middle = (count+1)//2
        else:
            middle = (count//2)+1
        temp = head
        #print(middle)
        count = 0
        while temp:
            count += 1
            if count == middle:
                return temp
            temp = temp.next
