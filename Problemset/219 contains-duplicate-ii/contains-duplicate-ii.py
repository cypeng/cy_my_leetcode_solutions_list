
# @Title: Contains Duplicate II (Contains Duplicate II)
# @Author: pcysl
# @Date: 2018-01-11 23:39:38
# @Runtime: 53 ms
# @Memory: N/A

class Solution(object):
    def containsNearbyDuplicate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        Hash = dict()
        for idx in range(0,len(nums)):
            num = nums[idx]             
            if (not num in Hash):
                Hash[num] = idx
            else:
                if (idx -Hash[num] <= k):
                    return bool(1)
                Hash[num] = idx
        
        return bool(0)
