
# @Title: Pow(x, n) (Pow(x, n))
# @Author: pcysl
# @Date: 2017-11-03 11:31:35
# @Runtime: 89 ms
# @Memory: N/A

class Solution:
    def myPow(self, x, n):
        """
        :type x: float
        :type n: int
        :rtype: float
        """
        return x**n
