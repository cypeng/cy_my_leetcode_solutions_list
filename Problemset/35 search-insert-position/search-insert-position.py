
# @Title: Search Insert Position (Search Insert Position)
# @Author: pcysl
# @Date: 2017-12-26 14:02:58
# @Runtime: 65 ms
# @Memory: N/A

class Solution:
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        idx = 0
        jdx = len(nums)-1
        if (len(nums) > 0):
            while (idx <= jdx):
                midIdx = int((idx+jdx)*0.5)
                if (nums[midIdx] == target):
                    return midIdx
                elif (nums[midIdx] < target):
                    idx = midIdx+1
                elif (nums[midIdx] > target):
                    jdx = midIdx-1
        else:
            return 0

        if (midIdx == len(nums)-1):
            if (nums[midIdx] < target):
                return midIdx+1
            elif (nums[midIdx] > target):
                return midIdx
        elif (midIdx == 0):
            if (nums[0] > target):
                return 0
            elif (nums[0] < target):
                return 1
        else:
            if(nums[midIdx] > target) and (nums[midIdx-1] < target):
                return midIdx
            elif (nums[midIdx] < target) and (nums[midIdx+1] > target):
                return midIdx+1
