
# @Title: 买卖股票的最佳时机 III (Best Time to Buy and Sell Stock III)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-26 23:07:24
# @Runtime: 312 ms
# @Memory: 24.6 MB

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        buy1, buy2, sell1, sell2 = -10000, -10000, 0, 0
        for price in prices:
            buy1 = max(buy1, 0-price)
            sell1 = max(sell1, buy1+price)
            buy2=max(buy2, sell1-price)
            sell2 = max(sell2, buy2+price)
        return sell2
