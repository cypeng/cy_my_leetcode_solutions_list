
# @Title: Subsets (Subsets)
# @Author: pcysl
# @Date: 2018-11-07 19:06:03
# @Runtime: 80 ms
# @Memory: N/A

def __find_subsets__(nums):
    answer = []
    answer.append([])
    for jdx in range(0, len(nums)):
        this_array = []
        __subsets__(nums, answer, this_array, jdx)
    return answer

def __subsets__(nums, answer, array, idx):
    #print(nums, answer, this_array, idx)
    this_array = list(array)
    this_array.append(nums[idx])
    answer.append(list(this_array))
    for jdx in range(1, len(nums)-idx): 
        next_idx = idx + jdx
        #print(next_idx,idx,jdx,this_array)
        __subsets__(nums, answer, this_array, next_idx)
        
class Solution:
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        if (len(nums) > 0):
            answer = __find_subsets__(nums)
        else:
            answer = []
        return answer
