
| English | 中文|

# [0078. Subsets](https://leetcode.com/problems/subsets/)

## Description

<p>Given a set of <strong>distinct</strong> integers, <em>nums</em>, return all possible subsets (the power set).</p>

<p><strong>Note:</strong> The solution set must not contain duplicate subsets.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3]
<strong>Output:</strong>
[
  [3],
&nbsp; [1],
&nbsp; [2],
&nbsp; [1,2,3],
&nbsp; [1,3],
&nbsp; [2,3],
&nbsp; [1,2],
&nbsp; []
]</pre>


## Related Topics

- [Array](https://leetcode.com/tag/array)
- [Backtracking](https://leetcode.com/tag/backtracking)
- [Bit Manipulation](https://leetcode.com/tag/bit-manipulation)

## Similar Questions

- [Subsets II](../subsets-ii/README_EN.md)
- [Generalized Abbreviation](../generalized-abbreviation/README_EN.md)
- [Letter Case Permutation](../letter-case-permutation/README_EN.md)
