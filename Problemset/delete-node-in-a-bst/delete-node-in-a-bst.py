
# @Title: 删除二叉搜索树中的节点 (Delete Node in a BST)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-08 23:54:03
# @Runtime: 84 ms
# @Memory: 18.9 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

def getLeftTreeRight(root):
    root = root.left
    while root.right:
        root = root.right
    return root.val

def getRightTreeLeft(root):
    root = root.right
    while root.left:
        root = root.left
    return root.val

class Solution:
    def deleteNode(self, root: TreeNode, key: int) -> TreeNode:
        if root is None:
            return None
        
        if key > root.val:
            root.right = self.deleteNode(root.right, key)
        elif key < root.val:
            root.left = self.deleteNode(root.left, key)
        else:
            if root.left is None and root.right is None: 
                root = None
            elif root.left:
                root.val = getLeftTreeRight(root)
                root.left = self.deleteNode(root.left,root.val)
            else:
                root.val = getRightTreeLeft(root)
                root.right = self.deleteNode(root.right,root.val)
        return root
