
# @Title: 赎金信 (Ransom Note)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-15 00:01:00
# @Runtime: 312 ms
# @Memory: 15.2 MB

class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        hashset = []
        for item in ransomNote:
            Flag = False
            for idx, comp in enumerate(magazine):
                if (comp == item) and (not idx in hashset):
                    hashset.append(idx)
                    Flag = True
                    break
            if not Flag:
                return False
        return True
