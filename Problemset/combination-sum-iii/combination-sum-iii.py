
# @Title: 组合总和 III (Combination Sum III)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-03 14:51:22
# @Runtime: 44 ms
# @Memory: 15.1 MB

def backtrack(item,k,n,case,case_sum,ans):
    if (len(case) == k) and (case_sum == n):
        if not case in ans:
            ans.append(case)
    if (len(case) > k) or (item > 9):
        return False
    if (case_sum + item <= n):
        this_case = case.copy()
        this_case.append(item)
        backtrack(item+1,k,n,this_case,case_sum+item,ans)
    backtrack(item+1,k,n,case,case_sum,ans)
       
class Solution:
    def combinationSum3(self, k: int, n: int) -> List[List[int]]:
        ans = []
        backtrack(1,k,n,[],0,ans)
        return ans
