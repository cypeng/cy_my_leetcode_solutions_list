
# @Title: Linked List Cycle (Linked List Cycle)
# @Author: pcysl
# @Date: 2018-01-04 00:33:24
# @Runtime: 89 ms
# @Memory: N/A

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def hasCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """      
        IniTemp = head
        while (head):
            temp = head
            if (temp.next == IniTemp):
                return bool(1)
            head = head.next
            temp.next = IniTemp
            
        return bool(0)    

                
