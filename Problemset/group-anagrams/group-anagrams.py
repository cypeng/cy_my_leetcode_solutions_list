
# @Title: 字母异位词分组 (Group Anagrams)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-27 10:17:47
# @Runtime: 52 ms
# @Memory: 17.4 MB

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        hashmap = {}
        for item in strs:
            key_list = []
            key_list[:0] = item
            key_list.sort()
            key = ''.join(key_list)
            if (not key in hashmap):
                hashmap[key] = [item]
            else:
                hashmap[key].append(item)
        return list(hashmap.values())

