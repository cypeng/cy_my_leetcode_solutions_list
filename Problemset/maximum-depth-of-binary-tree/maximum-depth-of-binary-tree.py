
# @Title: 二叉树的最大深度 (Maximum Depth of Binary Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 18:05:46
# @Runtime: 52 ms
# @Memory: 16.6 MB

def traversal(root, depth, opt_depth):
    if depth > opt_depth:
        opt_depth = depth
    if root:
        if root.left:
            opt_depth = traversal(root.left, depth+1, opt_depth)
        if root.right:
            opt_depth = traversal(root.right, depth+1, opt_depth)
    return opt_depth


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        if root is None:
            return 0
        depth, opt_depth = 1, 1
        return traversal(root, depth, opt_depth)
