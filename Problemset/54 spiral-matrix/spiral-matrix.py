
# @Title: Spiral Matrix (Spiral Matrix)
# @Author: pcysl
# @Date: 2018-07-31 16:52:32
# @Runtime: 40 ms
# @Memory: N/A

class Solution:
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        if (len(matrix) > 1):
            return spiralMatrix(matrix)
        elif ((len(matrix) <= 1) and (len(matrix) > 0)):
            return matrix[0][:]
        else:
            return []
def spiralMatrix(matrix):
    Done = bool(0)
    flag = 1
    ans  = []
    
    # flag1
    flag1IniIdx   = 0
    flag1IniJdx   = 0
    flag1FinalJdx = len(matrix[flag1IniIdx][:])-1
    countFlag1    = bool(1)
    
    # flag2
    flag2IniIdx   = 1
    flag2IniJdx   = len(matrix[flag2IniIdx][:])-1
    flag2FinalIdx = len(matrix)-1
    countFlag2    = bool(1)
    
    # flag3
    flag3IniIdx   = len(matrix)-1
    flag3IniJdx   = len(matrix[flag3IniIdx][:])-2
    flag3FinalJdx = 0
    countFlag3    = bool(1)
    
    # flag4
    flag4IniIdx   = len(matrix)-2
    flag4IniJdx   = 0
    flag4FinalIdx = 1 
    countFlag4    = bool(1)

    countNo       = 0
    limitNo       = len(matrix)*len(matrix[0][:])
    while (not Done):
        if (countNo == limitNo):
            Done = bool(1)
            break
        if (5 == flag):
            flag = 1
        if (4 == flag):
            if (bool(1) == countFlag4):
                idx = flag4IniIdx
                jdx = flag4IniJdx
                countFlag4 = bool(0)
                countFlag1 = bool(1)
            ans.append(matrix[idx][jdx])
            countNo += 1
            if (flag4FinalIdx == idx):
                flag = 5
                flag4IniIdx   -= 1
                flag4IniJdx   += 1
                flag4FinalIdx += 1
            else:
                idx -= 1
        if (3 == flag):
            if (bool(1) == countFlag3):
                idx = flag3IniIdx
                jdx = flag3IniJdx
                countFlag3 = bool(0)
                countFlag4 = bool(1)
            ans.append(matrix[idx][jdx])
            countNo += 1
            if (flag3FinalJdx == jdx):
                flag = 4
                flag3IniIdx   -= 1
                flag3IniJdx   -= 1
                flag3FinalJdx += 1
            else:
                jdx -= 1
        if (2 == flag):
            if (bool(1) == countFlag2):
                idx = flag2IniIdx
                jdx = flag2IniJdx
                countFlag2 = bool(0)
                countFlag3 = bool(1)
            ans.append(matrix[idx][jdx])
            countNo += 1
            if (flag2FinalIdx == idx):
                flag = 3
                flag2IniIdx   += 1
                flag2IniJdx   -= 1
                flag2FinalIdx -= 1
            else:
                idx += 1

        if (1 == flag):
            if (bool(1) == countFlag1):
                idx = flag1IniIdx
                jdx = flag1IniJdx
                countFlag1 = bool(0)
                countFlag2 = bool(1)            
            ans.append(matrix[idx][jdx])
            countNo += 1
            if (flag1FinalJdx == jdx):
                flag = 2
                flag1IniIdx   += 1
                flag1IniJdx   += 1
                flag1FinalJdx -= 1
            else:
                jdx += 1
    return ans
