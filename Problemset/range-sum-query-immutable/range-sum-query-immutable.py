
# @Title: 区域和检索 - 数组不可变 (Range Sum Query - Immutable)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-21 12:03:44
# @Runtime: 4896 ms
# @Memory: 17.9 MB

class NumArray:

    def __init__(self, nums: List[int]):
        self.nums = nums

    def sumRange(self, left: int, right: int) -> int:
        sum_value = 0
        for idx in range(left, right+1):
            sum_value += self.nums[idx]
        return sum_value


# Your NumArray object will be instantiated and called as such:
# obj = NumArray(nums)
# param_1 = obj.sumRange(left,right)
