
# @Title: 二叉搜索树迭代器 (Binary Search Tree Iterator)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-06 21:11:12
# @Runtime: 100 ms
# @Memory: 20.8 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

def traversal(root, root_list):
    if root:
        if root.left:
            root_list = traversal(root.left, root_list)
        root_list.append(root.val)
        if root.right:
            root_list = traversal(root.right, root_list)
    return root_list

class BSTIterator:
    def __init__(self, root: TreeNode):
        self.root_list = traversal(root, [])
        self.size = len(self.root_list)
        self.idx = -1

    def next(self) -> int:
        self.idx+=1
        return self.root_list[self.idx]
        
    def hasNext(self) -> bool:
        if self.idx+1 > self.size-1:
            return False
        return True

# Your BSTIterator object will be instantiated and called as such:
# obj = BSTIterator(root)
# param_1 = obj.next()
# param_2 = obj.hasNext()
