
# @Title: 最长公共前缀 (Longest Common Prefix)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-22 15:02:23
# @Runtime: 40 ms
# @Memory: 15.1 MB

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if len(strs) <= 1:
            return strs[0]
        ans = ""
        temp = ""
        for char in strs[0]:
            temp = temp + char
            idx, jdx = 1, len(strs)-1
            while idx <= jdx:
                i_str = strs[idx]
                if not temp == i_str[0:len(temp)]:
                    return ans
                j_str = strs[jdx]
                if not temp == j_str[0:len(temp)]:
                    return ans
                idx+=1
                jdx-=1
            ans = temp
        return ans

