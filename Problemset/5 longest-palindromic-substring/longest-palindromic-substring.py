
# @Title: Longest Palindromic Substring (Longest Palindromic Substring)
# @Author: pcysl
# @Date: 2019-02-25 11:58:43
# @Runtime: 4764 ms
# @Memory: 13.1 MB

def main(str):
    if (len(str) == 0):
        return str
    str_num = len(str)
    while (str_num > 0):
        idx = 0
        while (idx+str_num -1 <= len(str)-1):
            comp_str = str[idx:idx+str_num]
            #print(comp_str)
            if (comp_str == comp_str[::-1]):
                return comp_str
            idx += 1
        str_num -= 1
    return comp_str

class Solution:
    def longestPalindrome(self, s: str) -> str:
        return main(s)
