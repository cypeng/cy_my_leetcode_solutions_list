
# @Title: 旋转矩阵 (Rotate Matrix LCCI)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-22 00:59:50
# @Runtime: 40 ms
# @Memory: 14.8 MB

class Solution:
    def rotate(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        size = [len(matrix), len(matrix[0])]
        new_matrix = []
        for idx in range(0, size[0]):
            this_column = []
            for jdx in range(0, size[1]):
                this_column.append(matrix[size[1]-jdx-1][idx])
                #new_matrix[jdx][size[1]-idx-1] = matrix[idx][jdx]
            new_matrix.append(this_column)
        for idx in range(0, size[0]):
            for jdx in range(0, size[1]):
                matrix[idx][jdx] = new_matrix[idx][jdx]
