
# @Title: 寻找数组的中心下标 (Find Pivot Index)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-20 21:49:36
# @Runtime: 116 ms
# @Memory: 16.2 MB


class Solution:
    def pivotIndex(self, nums: List[int]) -> int:
        left_acc_sum = nums.copy()
        right_acc_sum = nums.copy()
        idx, jdx = 1, len(right_acc_sum)-2
        while 1:
            if (idx > len(left_acc_sum)-1) and (jdx < 0):
                break
            if idx <= len(left_acc_sum) - 1:
                left_acc_sum[idx] = left_acc_sum[idx] + left_acc_sum[idx-1]
                idx+=1
            if jdx >= 0:
                right_acc_sum[jdx] = right_acc_sum[jdx] + right_acc_sum[jdx+1]
                jdx-=1
        if 0 == right_acc_sum[1]:
            return 0
        for idx in range(1, len(nums)-1):
            if left_acc_sum[idx-1] == right_acc_sum[idx+1]:
                return idx
        if left_acc_sum[len(nums)-2] == 0:
            return len(nums)-1
        return -1
