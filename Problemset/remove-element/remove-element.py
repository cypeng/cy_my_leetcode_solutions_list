
# @Title: 移除元素 (Remove Element)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 15:23:08
# @Runtime: 28 ms
# @Memory: 14.9 MB

class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        idx, jdx = 0, 0
        for jdx in range(0, len(nums)):
            if nums[jdx] is not val:
                nums[idx] = nums[jdx]
                idx += 1
            jdx += 1
        
        return idx
