
# @Title: Path Sum II (Path Sum II)
# @Author: pcysl
# @Date: 2018-03-22 00:04:48
# @Runtime: 120 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: List[List[int]]
        """
        self.ans = []

        if (root):
            self.rootArray = []
            self.rootArray.append(root)
            self.sumArray  = []
            self.sumArray.append(sum)
            self.routeArray = []
            while (self.rootArray):
                rootArray = []
                routeArray= []
                sumArray  = []
                route     = []
                for idx in range(0,len(self.rootArray)):
                    root = self.rootArray[idx]
                    thissum = self.sumArray[idx]-root.val
                    if (len(self.routeArray)):
                        route = list(self.routeArray[idx])
                    route.append(root.val)
                    if (root.left):
                        rootArray.append(root.left)
                        sumArray.append(thissum)
                        routeArray.append(route)
                    if (root.right):
                        rootArray.append(root.right)
                        sumArray.append(thissum)
                        routeArray.append(route)
                        
                    if (not root.left) and (not root.right):
                        if (thissum == 0):
                            self.ans.append(route)
                            
                self.sumArray = []
                self.sumArray = sumArray    
                self.rootArray = []
                self.rootArray = rootArray
                self.routeArray = []
                self.routeArray = routeArray
            return self.ans
        else:
            return self.ans

            
