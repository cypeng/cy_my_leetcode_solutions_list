
# @Title: Binary Tree Preorder Traversal (Binary Tree Preorder Traversal)
# @Author: pcysl
# @Date: 2018-03-08 00:11:13
# @Runtime: 40 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.ans = []
        self.preorder(root)
        return self.ans
        
    def preorder(self, root):
        if (root):
            self.ans.append(root.val)
            self.preorder(root.left)
            self.preorder(root.right)
        
    
