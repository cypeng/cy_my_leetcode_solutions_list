
| English | 中文|

# [0144. Binary Tree Preorder Traversal](https://leetcode.com/problems/binary-tree-preorder-traversal/)

## Description

<p>Given a binary tree, return the <em>preorder</em> traversal of its nodes&#39; values.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>&nbsp;<code>[1,null,2,3]</code>
   1
    \
     2
    /
   3

<strong>Output:</strong>&nbsp;<code>[1,2,3]</code>
</pre>

<p><strong>Follow up:</strong> Recursive solution is trivial, could you do it iteratively?</p>


## Related Topics

- [Stack](https://leetcode.com/tag/stack)
- [Tree](https://leetcode.com/tag/tree)

## Similar Questions

- [Binary Tree Inorder Traversal](../binary-tree-inorder-traversal/README_EN.md)
- [Verify Preorder Sequence in Binary Search Tree](../verify-preorder-sequence-in-binary-search-tree/README_EN.md)
- [N-ary Tree Preorder Traversal](../n-ary-tree-preorder-traversal/README_EN.md)
