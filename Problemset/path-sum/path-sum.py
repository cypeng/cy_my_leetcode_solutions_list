
# @Title: 路径总和 (Path Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 18:14:10
# @Runtime: 68 ms
# @Memory: 16.5 MB

def traversal(root, this_sum, targetSum, flag):
    if not flag and root:
        this_sum += root.val
        if root.left is None and root.right is None:
            if this_sum == targetSum:
                return True
        if root.left:
            flag = traversal(root.left, this_sum, targetSum, flag)
        if root.right:
            flag = traversal(root.right, this_sum, targetSum, flag)
    return flag

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def hasPathSum(self, root: TreeNode, targetSum: int) -> bool:
        flag = False
        this_sum = 0
        return traversal(root, this_sum, targetSum, flag)
