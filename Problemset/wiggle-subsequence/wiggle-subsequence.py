
# @Title: 摆动序列 (Wiggle Subsequence)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-07 12:26:21
# @Runtime: 44 ms
# @Memory: 15.1 MB

class Solution:
    def wiggleMaxLength(self, nums: List[int]) -> int:
        if len(nums) <= 1:
            return len(nums)
        pre_diff = nums[1] - nums[0]
        count = 1
        if (not pre_diff == 0):
            count = 2
        for idx in range(2, len(nums)):
            diff = nums[idx] - nums[idx-1]
            if (diff > 0 and pre_diff <= 0) or (diff < 0 and pre_diff >= 0):
                count += 1
                pre_diff = diff
        return count
