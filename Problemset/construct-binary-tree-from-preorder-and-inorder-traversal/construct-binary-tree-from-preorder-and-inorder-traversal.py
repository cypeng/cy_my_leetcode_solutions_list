
# @Title: 从前序与中序遍历序列构造二叉树 (Construct Binary Tree from Preorder and Inorder Traversal)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-31 11:18:27
# @Runtime: 172 ms
# @Memory: 52.9 MB

def find_val(inorder, val):
    idx, jdx = 0, len(inorder)-1
    while idx <= jdx:
        if inorder[idx] == val:
            return idx
        if inorder[jdx] == val:
            return jdx 
        idx += 1
        jdx -= 1
    return None

def createNode(head, preorder, inorder, this_idx):
    val = preorder[this_idx]
    this_node = TreeNode(val, None, None)
    if this_idx == 0:
        head = this_node
    if this_idx+1 > len(preorder)-1:
        return this_node
    inorder_idx = find_val(inorder, val)
    left_child, right_child = [], []
    #print(inorder_idx, inorder, left_child, right_child)
    if not inorder_idx is None:
        left_child = inorder[0:inorder_idx]
        right_child = inorder[inorder_idx+1:]
    else:
        return None

    left_child_idx = this_idx    
    while left_child_idx+1 <= len(preorder)-1 and len(left_child) > 0:
        obj  = createNode(head, preorder, left_child, left_child_idx+1)
        if not obj is None:
            this_node.left = obj
            break
        left_child_idx += 1
    right_this_idx = this_idx
    while right_this_idx+1 <= len(preorder)-1 and len(right_child) > 0:
        obj = createNode(head, preorder, right_child, right_this_idx+1)
        if not obj is None:
            this_node.right = obj
            break
        right_this_idx += 1
    
    return this_node


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        head = createNode(None, preorder, inorder, 0)
        return head
