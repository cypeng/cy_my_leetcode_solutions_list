
# @Title: 合并区间 (Merge Intervals)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-21 23:02:58
# @Runtime: 48 ms
# @Memory: 15.7 MB

def checkIntervals(a,b):
    if (a[0] < b[1]) and (a[0] > b[0]):
        return True
    if (a[1] > b[0]) and (a[1] < b[1]):
        return True
    if (a[0] == b[0]) or (a[1] == b[1]) or (a[1] == b[0]):
        return True
    return False 

class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        if len(intervals) == 1:
            return intervals
        intervals.sort(key=lambda x: x[0])
        ans = []
        for idx in range(0, len(intervals)):
            cur = intervals[idx]
            if len(ans) > 0:
                temp = ans[-1] 
                if checkIntervals(cur,temp) or checkIntervals(temp,cur):
                    temp[0] = min(temp[0],cur[0])
                    temp[1] = max(temp[1],cur[1])
                    ans[-1] = temp
                else:
                    ans.append(cur)
            else:
                ans.append(cur)
        return ans
