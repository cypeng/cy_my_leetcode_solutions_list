
# @Title: 最后一块石头的重量 II (Last Stone Weight II)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-13 11:38:48
# @Runtime: 60 ms
# @Memory: 14.9 MB

class Solution:
    def lastStoneWeightII(self, stones: List[int]) -> int:
        #   0 1 2 3 4 5
        # 1 0 0 1 2 3 4
        # 2 0 0 0 2 3 4
        
        target = sum(stones)//2
        dp = [0 for idx in range(0,target+1)]
        #dp[0]=1
        for idx in range(0, len(stones)):
            for jdx in range(target, stones[idx]-1, -1):
                dp[jdx] = max(dp[jdx], dp[jdx-stones[idx]]+stones[idx])
                #print(dp)
        return sum(stones)-2*dp[target]
