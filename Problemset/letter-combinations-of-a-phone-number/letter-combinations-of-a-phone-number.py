
# @Title: 电话号码的字母组合 (Letter Combinations of a Phone Number)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-02 21:18:23
# @Runtime: 28 ms
# @Memory: 14.9 MB

def generateOutput(idx,digits,this_str,ans):
    digit_dict = {'2':['a','b','c'],'3':['d','e','f'],'4':['g','h','i'], '5':['j','k','l'],'6':['m','n','o'],'7':['p','q','r','s'],'8':['t','u','v'],'9':['w','x','y','z']}
    #print(digit_dict)
    this_dict = digit_dict[digits[idx]]
    for char in this_dict: 
        if idx+1 <= len(digits)-1:
            generateOutput(idx+1,digits,this_str+char,ans)
        if idx == len(digits)-1:
            ans.append(this_str+char)    

class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        if len(digits)==0:
            return []
        ans = []
        generateOutput(0,digits,'',ans)
        return ans
