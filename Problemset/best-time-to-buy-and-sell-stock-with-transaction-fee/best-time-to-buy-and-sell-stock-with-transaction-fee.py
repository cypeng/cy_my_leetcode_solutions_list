
# @Title: 买卖股票的最佳时机含手续费 (Best Time to Buy and Sell Stock with Transaction Fee)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-08 00:15:30
# @Runtime: 120 ms
# @Memory: 19.8 MB

class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        buy = prices[0]+fee
        profit = 0
        for idx in range(1, len(prices)):
            if (prices[idx] + fee < buy):
                buy = prices[idx] + fee
            elif (prices[idx] - buy > 0):
                profit += prices[idx] - buy
                buy = prices[idx]
        return profit
