
| English | 中文|

# [0125. Valid Palindrome](https://leetcode.com/problems/valid-palindrome/)

## Description

<p>Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.</p>

<p><strong>Note:</strong>&nbsp;For the purpose of this problem, we define empty string as valid palindrome.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> &quot;A man, a plan, a canal: Panama&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> &quot;race a car&quot;
<strong>Output:</strong> false
</pre>


## Related Topics

- [Two Pointers](https://leetcode.com/tag/two-pointers)
- [String](https://leetcode.com/tag/string)

## Similar Questions

- [Palindrome Linked List](../palindrome-linked-list/README_EN.md)
- [Valid Palindrome II](../valid-palindrome-ii/README_EN.md)
