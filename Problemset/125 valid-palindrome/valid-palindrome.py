
# @Title: Valid Palindrome (Valid Palindrome)
# @Author: pcysl
# @Date: 2018-12-07 13:42:43
# @Runtime: 156 ms
# @Memory: 7.1 MB

def is_palidrome(s):
    idx, jdx = 0, len(s)-1
    flag = bool(1)
    while (idx <= jdx):
        front_character = s[idx]
        back_character = s[jdx]
        while (not is_character(front_character)):
            idx += 1
            if (idx > jdx):
                break
            front_character = s[idx]
            
            
        while (not is_character(back_character)):
            jdx -= 1
            if (idx > jdx):
                break
            back_character = s[jdx]
        #print(idx,jdx)
        if (idx < jdx):
            if (compare_character(front_character) != compare_character(back_character)):
                flag = bool(0)
                break
            
        idx += 1
        jdx -= 1
    return flag

def is_character(character):
    if ord(character) >= ord('0') and ord(character) <= ord('9'):
        return bool(1)
    if ord(character) >= ord('a') and ord(character) <= ord('z'):
        return bool(1)
    if ord(character) >= ord('A') and ord(character) <= ord('Z'):
        return bool(1)
    return bool(0)

def compare_character(character):
    if (ord(character) >= ord('a')):
        return ord(character)- ord('a') + ord('A')
    else:
        return ord(character)

class Solution:
    def isPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        return is_palidrome(s)
