
| English | 中文|

# [0105. Construct Binary Tree from Preorder and Inorder Traversal](https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/)

## Description

<p>Given preorder and inorder traversal of a tree, construct the binary tree.</p>

<p><strong>Note:</strong><br />
You may assume that duplicates do not exist in the tree.</p>

<p>For example, given</p>

<pre>
preorder =&nbsp;[3,9,20,15,7]
inorder = [9,3,15,20,7]</pre>

<p>Return the following binary tree:</p>

<pre>
    3
   / \
  9  20
    /  \
   15   7</pre>


## Related Topics

- [Array](https://leetcode.com/tag/array)
- [Tree](https://leetcode.com/tag/tree)
- [Depth-first Search](https://leetcode.com/tag/depth-first-search)

## Similar Questions

- [Construct Binary Tree from Inorder and Postorder Traversal](../construct-binary-tree-from-inorder-and-postorder-traversal/README_EN.md)
