
# @Title: Construct Binary Tree from Preorder and Inorder Traversal (Construct Binary Tree from Preorder and Inorder Traversal)
# @Author: pcysl
# @Date: 2018-02-15 15:19:59
# @Runtime: 340 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def buildTree(self, preorder, inorder):
        """
        :type preorder: List[int]
        :type inorder: List[int]
        :rtype: TreeNode
        """
        
        self.NoLeftleaf, self.NoRightleaf = 0, 0
        self.root = []
        if (len(preorder) > 0) and (len(inorder) > 0):
            self.root = self.BuildTree(preorder, inorder)
        return self.root
    
    def FindLeaf(self, InorderTree, rootVal):
        idx = InorderTree.index(rootVal)
        NoLeftleaf = idx
        NoRightleaf = len(InorderTree)-(idx+1)
        return NoLeftleaf, NoRightleaf
    
    def BuildTree(self, PreorderTree, InorderTree):
        if (len(PreorderTree) > 0):
            root = TreeNode(PreorderTree[0])
            a, b = self.FindLeaf(InorderTree, PreorderTree[0])
            if (a > 0):
                root.left = self.BuildTree(PreorderTree[1:1+a], InorderTree[0:a])
            if (b > 0):
                root.right= self.BuildTree(PreorderTree[-b:], InorderTree[-b:])
        else:
            root = None
       
        return root

            
              

        
    
