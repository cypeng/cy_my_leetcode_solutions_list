
# @Title: x 的平方根 (Sqrt(x))
# @Author: pcysl@hotmail.com
# @Date: 2021-06-16 23:34:08
# @Runtime: 52 ms
# @Memory: 15 MB

class Solution:
    def mySqrt(self, x: int) -> int:
        if x == 1:
            return 1
        idx, jdx = 0, x
        while 1:
            mid = 0.5*(idx + jdx)
            #print(idx, jdx, mid)
            if abs(mid*mid - x) < 0.001:
                return int(mid)
            if mid*mid > x:
                jdx = mid
            else:
                idx = mid
            
