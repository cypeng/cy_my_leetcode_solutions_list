
# @Title: 判断子序列 (Is Subsequence)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-04 00:08:04
# @Runtime: 36 ms
# @Memory: 15.1 MB

class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        dp = [0 for idx in range(0, len(s)+1)]
        s_jdx = 1
        for idx in range(1, len(s)+1):
            for jdx in range(s_jdx, len(t)+1):
                if s[idx-1] == t[jdx-1]:
                    dp[idx] = max(1, 1+dp[idx-1])
                    s_jdx = jdx+1
                    break
                else:
                    dp[idx] = max(dp[idx], dp[idx-1])
        if dp[-1] == len(s):
            return True
        return False
