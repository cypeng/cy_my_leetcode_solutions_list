
# @Title: Count Primes (Count Primes)
# @Author: pcysl
# @Date: 2018-01-08 10:41:26
# @Runtime: 3396 ms
# @Memory: N/A

class Solution(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        Prime = 2
        Hash  = [0]*(n+1)
        count = 0
        while Prime < n:
            NonPrime = Prime
            if (Hash[NonPrime-1] == 0):
                count = count+1 
            multi = NonPrime
            NonPrime = Prime*multi
            while (NonPrime < n):
                Hash[NonPrime-1] = 1 
                multi = multi +1
                NonPrime = Prime*multi
            Prime = Prime +1
    
        return count
