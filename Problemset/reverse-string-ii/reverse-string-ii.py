
# @Title: 反转字符串 II (Reverse String II)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-11 20:01:08
# @Runtime: 80 ms
# @Memory: 15 MB

class Solution:
    def reverseStr(self, s: str, k: int) -> str:
        new_str = ''
        this_str = ""
        for idx, item in enumerate(s):
            if idx%(2*k) < k:
                this_str = item + this_str
            else:
                this_str += item
            if (idx%(2*k) == 2*k-1) or (idx == len(s)-1):
                new_str += this_str
                this_str = ""
        return new_str
