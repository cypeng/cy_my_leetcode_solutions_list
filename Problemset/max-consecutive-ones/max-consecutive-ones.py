
# @Title: 最大连续 1 的个数 (Max Consecutive Ones)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-22 14:37:04
# @Runtime: 84 ms
# @Memory: 15.2 MB

class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        count, opt = 0, 0
        for idx, item in enumerate(nums):
            if (count == 0) and (len(nums)-idx-1 < opt):
                break
            if item == 1:
                count +=1
            else:
                count = 0
            if opt < count:
                opt = count
        return opt
