
# @Title: Container With Most Water (Container With Most Water)
# @Author: pcysl
# @Date: 2017-12-14 15:44:04
# @Runtime: 119 ms
# @Memory: N/A

class Solution:
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        answer = 0
        l = 0
        r = len(height) - 1;
        while (l < r) :
            answer = max(answer, min(height[l], height[r]) * abs(r - l));
            if (height[l] < height[r]):
                l=l+1;
            else:
                r=r-1;
        
        return answer;
