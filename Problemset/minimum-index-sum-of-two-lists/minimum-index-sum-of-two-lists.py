
# @Title: 两个列表的最小索引总和 (Minimum Index Sum of Two Lists)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-26 18:46:57
# @Runtime: 56 ms
# @Memory: 15.3 MB

class Solution:
    def findRestaurant(self, list1: List[str], list2: List[str]) -> List[str]:
        ans = []
        opt = 5001
        hashmap = {}
        for idx, item in enumerate(list1):
            hashmap[item] = idx

        idx, jdx = 0, len(list2)-1
        while idx <= jdx:
            if list2[idx] in hashmap:
                this_idx = idx + hashmap[list2[idx]]
                if opt == this_idx:
                    ans.append(list2[idx])
                if opt > this_idx:
                    ans = [list2[idx]]
                    opt = this_idx

            if not idx == jdx:
                if list2[jdx] in hashmap:
                    this_idx = jdx + hashmap[list2[jdx]]
                    if opt == this_idx:
                        ans.append(list2[jdx])
                    if opt > this_idx:
                        ans = [list2[jdx]]
                        opt = this_idx
            idx += 1
            jdx -= 1
        return ans
