
# @Title: 旋转链表 (Rotate List)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 14:56:42
# @Runtime: 44 ms
# @Memory: 14.8 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
def ListNodeCount(l):
    count = 0
    lpre = None
    while l:
        lpre = l
        l = l.next
        count += 1
    return lpre, count

def pointSpeciNode(l, N):
    count = 0
    lprev = None
    while l:
        if count == N-1:
            lprev = l
            l = l.next
            break
        l = l.next
        count += 1
    return lprev, l

class Solution:
    def rotateRight(self, head: ListNode, k: int) -> ListNode:
        if head is None:
            return head
        if head.next is None:
            return head
        if k == 0:
            return head
        
        lstart2, N = ListNodeCount(head)
        if N-k == 0 or (k%N) == 0:
            return head
        else:
            lend, lhead = pointSpeciNode(head, N-(k%N))
            lend.next = None
            lstart2.next = head
            answerHead = lhead
            return answerHead
        
