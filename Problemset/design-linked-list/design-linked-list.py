
# @Title: 设计链表 (Design Linked List)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-29 15:37:25
# @Runtime: 352 ms
# @Memory: 15.4 MB

class LinkedList():
    def __init__(self, val):
        self.val  = val
        self.next = None

class MyLinkedList:
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.head = None

    def get(self, index: int) -> int:
        """
        Get the value of the index-th node in the linked list. If the index is invalid, return -1.
        """
        count = 0
        LL = self.head
        while LL:
            if count == index:
                return LL.val
            LL = LL.next
            count += 1
        return -1

    def addAtHead(self, val: int) -> None:
        """
        Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.
        """
        if self.head is None:
            self.head = LinkedList(val)
        else:
            temp = self.head
            self.head = LinkedList(val)
            self.head.next = temp

    def addAtTail(self, val: int) -> None:
        """
        Append a node of value val to the last element of the linked list.
        """
        if self.head is None:
            self.head  = LinkedList(val)
            return None
        LL = self.head
        while LL:
            pre = LL
            LL = LL.next
        pre.next = LinkedList(val)

    def addAtIndex(self, index: int, val: int) -> None:
        """
        Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted.
        """
        LL = self.head
        if index == 0:
            this_obj = LinkedList(val)
            this_obj.next = LL
            self.head = this_obj
            return None
        count = 0
        pre = None
        while LL:
            if count == index:
                this_obj = LinkedList(val)
                pre.next = this_obj   
                temp = LL 
            pre = LL
            LL = LL.next
            if count == index:
                this_obj.next = temp
            count += 1
        if count == index:
            pre.next = LinkedList(val)

    def deleteAtIndex(self, index: int) -> None:
        """
        Delete the index-th node in the linked list, if the index is valid.
        """
        if index == 0:
            self.head = self.head.next
            return None
        count = 0
        pre = None
        LL = self.head
        while LL:
            if count == index-1:
                pre = LL   
            LL = LL.next
            if count == index:
                pre.next = LL
            count += 1



# Your MyLinkedList object will be instantiated and called as such:
# obj = MyLinkedList()
# param_1 = obj.get(index)
# obj.addAtHead(val)
# obj.addAtTail(val)
# obj.addAtIndex(index,val)
# obj.deleteAtIndex(index)
