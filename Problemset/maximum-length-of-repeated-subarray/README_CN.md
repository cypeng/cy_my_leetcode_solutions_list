
| [English](README_EN.md) | 中文 |

# [718. 最长重复子数组](https://leetcode-cn.com/problems/maximum-length-of-repeated-subarray/)

## 題目描述

<p>给两个整数数组&nbsp;<code>A</code>&nbsp;和&nbsp;<code>B</code>&nbsp;，返回两个数组中公共的、长度最长的子数组的长度。</p>

<p>&nbsp;</p>

<p><strong>示例：</strong></p>

<pre><strong>输入：</strong>
A: [1,2,3,2,1]
B: [3,2,1,4,7]
<strong>输出：</strong>3
<strong>解释：</strong>
长度最长的公共子数组是 [3, 2, 1] 。
</pre>

<p>&nbsp;</p>

<p><strong>提示：</strong></p>

<ul>
	<li><code>1 &lt;= len(A), len(B) &lt;= 1000</code></li>
	<li><code>0 &lt;= A[i], B[i] &lt; 100</code></li>
</ul>


## 相關題目

- [数组](https://leetcode-cn.com/tag/array)
- [二分查找](https://leetcode-cn.com/tag/binary-search)
- [动态规划](https://leetcode-cn.com/tag/dynamic-programming)
- [滑动窗口](https://leetcode-cn.com/tag/sliding-window)
- [哈希函数](https://leetcode-cn.com/tag/hash-function)
- [滚动哈希](https://leetcode-cn.com/tag/rolling-hash)

## 相似題目

- [长度最小的子数组](../minimum-size-subarray-sum/README.md)
