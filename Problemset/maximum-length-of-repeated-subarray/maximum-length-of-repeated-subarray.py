
# @Title: 最长重复子数组 (Maximum Length of Repeated Subarray)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-30 01:22:00
# @Runtime: 3336 ms
# @Memory: 40.4 MB

class Solution:
    def findLength(self, nums1: List[int], nums2: List[int]) -> int:
        dp = [[0 for idx in range(0, len(nums1)+1)] for jdx in range(0, len(nums2)+1)]
        opt = 0
        for idx in range(0, len(nums1)):
            for jdx in range(0, len(nums2)):
                if nums1[idx] == nums2[jdx]:
                    dp[idx][jdx] = max(1, dp[idx-1][jdx-1]+1)
                    if dp[idx][jdx] > opt:
                        opt = dp[idx][jdx]
                        
        return opt
