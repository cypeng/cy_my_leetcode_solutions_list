
# @Title: 两两交换链表中的节点 (Swap Nodes in Pairs)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 01:44:36
# @Runtime: 36 ms
# @Memory: 14.9 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def swapListNode(l):
    count = 0
    lm0, lm1, lm, answer = None, None, None, None
    iniFlag = True
    while l: 
        if count % 2 == 0:
            if count > 1:
                if l.next is None:
                    lm.next = l
                    return answer 
            lm0 = l
            l = l.next
            lm0.next = None
        elif count % 2 == 1:
            lm1 = l
            l = l.next
            lm1.next = None
            if count > 2:
                lm.next =lm1
                lm = lm.next
            if iniFlag:
                lm = lm1
                answer = lm
                iniFlag = False
            lm.next = lm0
            lm = lm.next
        count += 1
    return answer

class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        if head is None:
            return head
        if head.next is None:
            return head
        return swapListNode(head)
