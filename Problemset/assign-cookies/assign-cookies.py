
# @Title: 分发饼干 (Assign Cookies)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-06 11:53:22
# @Runtime: 72 ms
# @Memory: 16.1 MB

class Solution:
    def findContentChildren(self, g: List[int], s: List[int]) -> int:
        count = [0]*len(g)
        opt = 0
        s.sort()
        g.sort()
        idx = 0
        for cookies in s:
            while idx <= len(g)-1:
                if cookies < g[idx]:
                    break
                else:
                    if count[idx] > 0:
                        idx += 1
                    else:
                        opt += 1 
                        count[idx] += 1
                        break
        return opt
