
# @Title: Rotate Image (Rotate Image)
# @Author: pcysl
# @Date: 2018-11-13 14:11:46
# @Runtime: 56 ms
# @Memory: N/A

def rotate(matrix):
    matrix_rotate(matrix)
    #coordinate_transfer(matrix)
    #return matrix

def matrix_rotate(matrix):
    m = len(matrix)
    mid_m   = int(m/2)
    kdx, ldx = 0, 0
    array_count = 0
    set_array_count = m-1
    while (set_array_count > 0):
            count = 0
            if (set_array_count == array_count):
                kdx += 1
                ldx = kdx
                set_array_count-= 2
                array_count = 0
            idx, jdx = kdx, ldx
            #print(kdx, ldx, mid_m, mid_m,set_array_count,array_count,end_m)
            while ((set_array_count > 0) and (count < 4)):
                #print(idx,jdx)
                if (m%2):
                    image_x = jdx - mid_m #(jdx-0) - mid_m
                    image_y = idx - mid_m #-1*(idx-mid_m)+mid_m- mid_m
                else:
                    image_x = jdx - mid_m#(jdx-0)- mid_m
                    image_y = idx - mid_m#-1*(idx-mid_m)+mid_m-1- mid_m
                new_image_x = -1*image_y
                new_image_y = 1*image_x
                if (m%2):
                    to_idx= new_image_y + mid_m
                    to_jdx= new_image_x + mid_m
                else:
                    to_idx = new_image_y + mid_m
                    to_jdx = new_image_x + mid_m-1
                if (count == 0):
                    temp = matrix_element_rotate(matrix,idx,jdx,to_idx,to_jdx,last_ele = None)
                    #print(temp)
                else:
                    temp = matrix_element_rotate(matrix,idx,jdx,to_idx,to_jdx,last_ele = temp)
                idx, jdx = to_idx, to_jdx
                #print(count,matrix,temp,idx,jdx,to_idx,to_jdx)
                count += 1
            ldx+= 1
            array_count += 1
            
def matrix_element_rotate(matrix,idx,jdx,to_idx,to_jdx,last_ele = None):
    temp = matrix[to_idx][to_jdx] 
    if (last_ele == None):
        matrix[to_idx][to_jdx] = matrix[idx][jdx]
    else:
        matrix[to_idx][to_jdx] = last_ele
    
    return temp

class Solution:
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        rotate(matrix)
