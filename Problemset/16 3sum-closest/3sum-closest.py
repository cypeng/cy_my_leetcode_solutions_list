
# @Title: 3Sum Closest (3Sum Closest)
# @Author: pcysl
# @Date: 2017-12-13 10:00:17
# @Runtime: 425 ms
# @Memory: N/A

class Solution:
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        sortnums = sorted(nums)
        answer = []
       
        if (len(sortnums)>=3):
            if (target > sortnums[len(sortnums)-1]+sortnums[len(sortnums)-2]+sortnums[len(sortnums)-3]):
                answer = sortnums[len(sortnums)-3]+sortnums[len(sortnums)-2]+sortnums[len(sortnums)-1]
            elif (target < sortnums[0]+sortnums[1]+sortnums[2]):
                answer = sortnums[0]+sortnums[1]+sortnums[2]
            else:
                cost = float('inf')            
            
                for idx in range(0,len(sortnums)-2):
                    if ((idx > 0) and (sortnums[idx] == sortnums[idx-1])):
                        continue
                    jdx  = idx +1
                    kdx  = len(sortnums)-1                
                    while (jdx < kdx):
                        if (abs(target-(sortnums[idx]+sortnums[jdx]+sortnums[kdx])) < cost):
                            cost = abs(target-(sortnums[idx]+sortnums[jdx]+sortnums[kdx]))
                            answer = sortnums[idx]+sortnums[jdx]+sortnums[kdx]
                        if (sortnums[idx]+sortnums[jdx]+sortnums[kdx] == target):
                            answer = sortnums[idx]+sortnums[jdx]+sortnums[kdx]
                            break
                        elif (sortnums[idx]+sortnums[jdx]+sortnums[kdx] < target):
                            jdx = jdx+1
                        else:
                            kdx = kdx-1
                        

        return answer
