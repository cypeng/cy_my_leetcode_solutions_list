
# @Title: Sort Colors (Sort Colors)
# @Author: pcysl
# @Date: 2018-11-06 14:16:38
# @Runtime: 44 ms
# @Memory: N/A

def __sortColors__(nums):
    idx = 0
    kdx = idx
    jdx = len(nums)-1
    Done = 0
    while (not Done):
        while ((0 == nums[idx]) and (idx < jdx)):
            idx += 1
            
        while ((2 == nums[jdx]) and (idx < jdx)):
            jdx -= 1
            
        if (idx > kdx):
            kdx = idx
                  
        if ((2 == nums[kdx]) and (kdx < jdx)):
            __swap__(nums,kdx,jdx)
            
        if ((0 == nums[kdx]) and (kdx > idx)):
            __swap__(nums,kdx,idx)
            
        if ((idx == jdx) or (kdx >= jdx)):
            Done = 1
        kdx += 1
        
    
def __swap__(nums,idx,jdx):
    temp = nums[idx]
    nums[idx] = nums[jdx]
    nums[jdx] = temp

class Solution:
    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        if (len(nums) > 1):
            __sortColors__(nums)
         
