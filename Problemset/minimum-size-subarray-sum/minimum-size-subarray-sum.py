
# @Title: 长度最小的子数组 (Minimum Size Subarray Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-26 00:56:43
# @Runtime: 44 ms
# @Memory: 16.4 MB

class Solution:
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        if len(nums) == 1 and nums[0] < target:
            return 0
        idx, jdx = 0, 0
        this_sum, opt_num = 0, len(nums)
        this_num = len(nums)
        ans_flag = False
        while jdx <= len(nums)-1:
            this_sum += nums[jdx]
            #print(idx, jdx, this_sum)
            while this_sum >= target:
                this_num = jdx - idx + 1
                #print('i', idx, jdx, this_sum, this_num)
                if opt_num >= this_num:
                    opt_num = this_num
                    ans_flag = True
                this_sum -= nums[idx]
                idx += 1
            jdx += 1
        if not ans_flag:
            opt_num = 0
        return opt_num

