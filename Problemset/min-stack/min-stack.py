
# @Title: 最小栈 (Min Stack)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-04 00:33:31
# @Runtime: 4492 ms
# @Memory: 17.9 MB

class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = [None]*5000
        self.idx = -1
        #self.jdx = -1

    def push(self, val: int) -> None:
        self.idx += 1
        self.stack[self.idx] = val
        #print(self.idx, self.stack)

    def pop(self) -> None:
        if self.idx < 0:
            return None
        ans = self.stack[self.idx]
        self.stack[self.idx] = None
        self.idx -= 1

    def top(self) -> int:
        return self.stack[self.idx]

    def getMin(self) -> int:
        min_val = None
        #print(self.stack)
        for kdx in range(0, self.idx+1):
            if min_val == None:
                min_val = self.stack[kdx]
            else:
                if min_val > self.stack[kdx]:
                    min_val = self.stack[kdx]
        return min_val

# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(val)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()
