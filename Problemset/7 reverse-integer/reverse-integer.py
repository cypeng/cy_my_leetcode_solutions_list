
# @Title: Reverse Integer (Reverse Integer)
# @Author: pcysl
# @Date: 2017-11-20 10:24:59
# @Runtime: 92 ms
# @Memory: N/A

class Solution:
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        str_x = str(x)
        if x<0:
            str_x = '-'+str_x[::-1][:-1]
            x = int(str_x)
        else:
            str_x = str_x[::-1]
            x = int(str_x)
        neg_limit= -0x80000000
        pos_limit= 0x7fffffff
        
        if(x<0):
            val=x&neg_limit
            if(val==neg_limit):
                return x
            else:
                return 0
        elif(x==0):
            return x
        else:
            val = x&pos_limit
            if(val==x):
                return x
            else:
                return 0    
        """
                oldstr  = str(x)
        
        if (x >= 0):
            EndIndex = len(str(x))
            newstr = ""
        else:
            EndIndex = len(str(x))-1
            newstr = "-"
        for index in range(0,EndIndex):
            newstr = newstr +oldstr[len(str(x))-index-1]

        if (x > 65535):
            newstr = "0"
            
        return int(newstr)
        """

