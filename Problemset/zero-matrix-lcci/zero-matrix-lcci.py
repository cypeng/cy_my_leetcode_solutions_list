
# @Title: 零矩阵 (Zero Matrix LCCI)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-20 23:52:54
# @Runtime: 40 ms
# @Memory: 15.3 MB

class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        F = matrix.copy()
        size = [len(matrix), len(matrix[0])]
        set_zeros = []
        for idx in range(0, size[0]):
            for jdx in range(0, size[1]):
                if matrix[idx][jdx] == 0:
                    set_zeros.append([idx, jdx])
        for coord in set_zeros:
            idx, jdx = coord[0], coord[1]
            while idx >= 0:
                matrix[idx][jdx] = 0
                idx -= 1
            idx, jdx = coord[0], coord[1]
            while idx <= len(matrix)-1:
                matrix[idx][jdx] = 0
                idx += 1
            idx, jdx = coord[0], coord[1]
            while jdx >= 0:
                matrix[idx][jdx] = 0
                jdx-=1
            idx, jdx = coord[0], coord[1]
            while jdx <= len(matrix[0])-1:
                matrix[idx][jdx] = 0
                jdx += 1
        return matrix
