
# @Title: Valid Sudoku (Valid Sudoku)
# @Author: pcysl
# @Date: 2018-07-24 12:49:55
# @Runtime: 76 ms
# @Memory: N/A

class Solution:
    def isValidSudoku(self, board):
        """
        :type board: List[List[str]]
        :rtype: bool
        """
        return checkSudoku(board)

def checkSudoku(board):
    Sudo9Dict = {}
    SudoRowDict = {}
    SudoColDict = {}
    data = list(board)
    for idx in range(0,3):
        for jdx in range(0,3):
             Sudo9Dict[idx,jdx] = [0]*9
             
    for idx in range(0,9):
        SudoRowDict[idx] = [0]*9
        SudoColDict[idx] = [0]*9
        
    for idx in range(0,9):
        for jdx in range(0,9):
            iidx = int(idx/3)
            jjdx = int(jdx/3)
            temp0 = Sudo9Dict[iidx,jjdx]
            temp1 = SudoRowDict[idx]
            temp2 = SudoColDict[jdx]
            if (data[idx][jdx] != "."):
                temp0[int(data[idx][jdx])-1] += 1
                temp1[int(data[idx][jdx])-1] += 1
                temp2[int(data[idx][jdx])-1] += 1
                if ((temp0[int(data[idx][jdx])-1] > 1) or (temp1[int(data[idx][jdx])-1] > 1) or (temp2[int(data[idx][jdx])-1] > 1)):
                    return bool(0)

    return bool(1)
