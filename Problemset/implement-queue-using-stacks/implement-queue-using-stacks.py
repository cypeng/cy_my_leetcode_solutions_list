
# @Title: 用栈实现队列 (Implement Queue using Stacks)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-03 11:12:46
# @Runtime: 36 ms
# @Memory: 14.8 MB

class MyQueue:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.queue = [None]*9
        self.idx = -1
        self.jdx = 0

    def push(self, x: int) -> None:
        """
        Push element x to the back of queue.
        """
        self.idx += 1
        self.queue[self.idx] = x


    def pop(self) -> int:
        """
        Removes the element from in front of queue and returns that element.
        """
        if not self.empty():
            ans = self.queue[self.jdx]
            self.queue[self.jdx] = None
            #print(ans, self.idx, self.jdx, self.queue)
            if self.idx == self.jdx:
                self.idx = -1
                self.jdx = 0
            else:
                self.jdx+=1
            #print(self.queue)
            return ans
        return None


    def peek(self) -> int:
        """
        Get the front element.
        """
        return self.queue[self.jdx]


    def empty(self) -> bool:
        """
        Returns whether the queue is empty.
        """
        if self.idx >= 0:
            return False
        return True



# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()
