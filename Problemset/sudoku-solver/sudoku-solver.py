
# @Title: 解数独 (Sudoku Solver)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-05 00:41:17
# @Runtime: 132 ms
# @Memory: 15.2 MB

def buildHash(board):
    hash_i, hash_j, hash_88, coord = [[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[]], []
    for idx in range(0,len(board)):
        for jdx in range(0, len(board[idx])):
            #print(idx, jdx, 3*(idx//3)+(jdx//3)%3)
            val = board[idx][jdx]
            if not val == ".":
                hash_i[idx].append(int(val))
                hash_j[jdx].append(int(val))
                hash_88[3*(idx//3)+(jdx//3)%3].append(int(val))
            else:
                coord.append([idx,jdx])
    return hash_i, hash_j, hash_88, coord

def backtrackSolution(idx, board, hash_i, hash_j, hash_88, coords, ans):
    if idx > len(coords)-1:
        ans = board
        #print(ans)
        return True
        
    this_coord = coords[idx]
    i, j = this_coord[0], this_coord[1]
    #print(i,j,coords)
    if board[i][j] == ".":
        for num in range(1,10):
            #print(idx, coords, num)
            if (not num in hash_i[i]) and (not num in hash_j[j]) and (not num in hash_88[3*(i//3)+(j//3)%3]):
                board[i][j] = str(num)
                #new_hash_i = hash_i.copy() #copy.deepcopy(hash_i) #hash_i.copy()
                #new_hash_j = hash_j.copy() #copy.deepcopy(hash_j) #hash_j.copy()
                #new_hash_88 = hash_88.copy() #copy.deepcopy(hash_88) #hash_88.copy()
                hash_i[i].append(num)
                hash_j[j].append(num)
                hash_88[3*(i//3)+(j//3)%3].append(num)
                if backtrackSolution(idx+1, board, hash_i, hash_j, hash_88, coords, ans):
                    return True
                board[i][j] = "."
                hash_i[i].remove(num)
                hash_j[j].remove(num)
                hash_88[3*(i//3)+(j//3)%3].remove(num)
    return False

class Solution:
    def solveSudoku(self, board: List[List[str]]) -> None:
        #import copy

        """
        Do not return anything, modify board in-place instead.
        """
        hash_i, hash_j, hash_88, coords = buildHash(board)
        #print(coords)
        backtrackSolution(0, board, hash_i, hash_j, hash_88, coords, board)
