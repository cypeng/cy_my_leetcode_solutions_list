
# @Title: 图像渲染 (Flood Fill)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-05 18:49:46
# @Runtime: 36 ms
# @Memory: 15 MB

def traversal(image, idx, jdx, val, new_val):
    if image[idx][jdx] == val and not val == new_val:
        image[idx][jdx] = new_val
        #print(image[idx][jdx])
        if idx+1 >=0 and idx+1 <= len(image)-1:
            image = traversal(image, idx+1, jdx, val, new_val)
        if idx-1 >=0 and idx-1 <= len(image)-1:
            image = traversal(image, idx-1, jdx, val, new_val)
        if jdx+1 >=0 and jdx+1 <= len(image[0])-1:
            image = traversal(image, idx, jdx+1, val, new_val)
        if jdx-1 >=0 and jdx-1 <= len(image[0])-1:
            image = traversal(image, idx, jdx-1, val, new_val)
    return image

class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, newColor: int) -> List[List[int]]:
        idx, jdx = sr, sc
        val = image[idx][jdx]
        return traversal(image, idx, jdx, val, newColor)
