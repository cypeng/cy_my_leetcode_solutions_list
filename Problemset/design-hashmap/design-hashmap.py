
# @Title: 设计哈希映射 (Design HashMap)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-25 00:44:43
# @Runtime: 5908 ms
# @Memory: 18 MB

class HashLinkedList:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None

class MyHashMap:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.hash = None

    def put(self, key: int, value: int) -> None:
        """
        value will always be non-negative.
        """
        if self.hash == None:
            self.hash = HashLinkedList(key, value)
            #print(self.hash.key, self.hash.value)
        else:
            head = self.hash
            while head:
                
                if head.key == key:
                    head.value = value
                    return 0
                if not head.next:
                    head.next = HashLinkedList(key, value)
                    return 0
                head = head.next

    def get(self, key: int) -> int:
        """
        Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
        """
        if self.hash == None:
            return -1
        head = self.hash
        
        while head:
            #print(self.hash.key, self.hash.value)
            if head.key == key:
                return head.value
            head = head.next
        return -1

    def remove(self, key: int) -> None:
        """
        Removes the mapping of the specified value key if this map contains a mapping for the key
        """
        head = self.hash
        pre = None
        while head:
            cur = head
            if head.key == key:
                head = head.next
                cur.next = None
                if pre == None:
                    self.hash = head
                    return 0
                pre.next = head
                return 0
            pre = head
            head = head.next

# Your MyHashMap object will be instantiated and called as such:
# obj = MyHashMap()
# obj.put(key,value)
# param_2 = obj.get(key)
# obj.remove(key)
