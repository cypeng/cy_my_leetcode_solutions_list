
# @Title: 寻找重复数 (Find the Duplicate Number)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-19 19:01:40
# @Runtime: 148 ms
# @Memory: 25.6 MB

class Solution:
    def findDuplicate(self, nums: List[int]) -> int:
        nums.sort() 
        for idx in range(0, len(nums)-1):
            if nums[idx] == nums[idx+1]:
                return nums[idx]
