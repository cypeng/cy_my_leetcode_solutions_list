
| English | 中文|

# [0047. Permutations II](https://leetcode.com/problems/permutations-ii/)

## Description

<p>Given a collection of numbers that might contain duplicates, return all possible unique permutations.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [1,1,2]
<strong>Output:</strong>
[
  [1,1,2],
  [1,2,1],
  [2,1,1]
]
</pre>


## Related Topics

- [Backtracking](https://leetcode.com/tag/backtracking)

## Similar Questions

- [Next Permutation](../next-permutation/README_EN.md)
- [Permutations](../permutations/README_EN.md)
- [Palindrome Permutation II](../palindrome-permutation-ii/README_EN.md)
- [Number of Squareful Arrays](../number-of-squareful-arrays/README_EN.md)
