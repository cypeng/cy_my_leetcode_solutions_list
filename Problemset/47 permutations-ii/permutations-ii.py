
# @Title: Permutations II (Permutations II)
# @Author: pcysl
# @Date: 2018-07-20 10:30:26
# @Runtime: 96 ms
# @Memory: N/A

class Solution:
    def permuteUnique(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        ans = []
        ansArray = [0]*len(nums)
        PermutateArray(nums, 0, ansArray, ans)
        return ans

def PermutateArray(nums, fidx, ansArray, ans):
    idx = 0
    dupDict = {}
    while (idx <= len(nums)-1):
        newNums = list(nums)
        if (newNums[idx]  != None):
            if (not newNums[idx] in dupDict):
                dupDict[newNums[idx]] = 1
                ansArray[fidx] = newNums[idx]
                newNums[idx]  = None
                #print(idx, fidx, ansArray, newNums)
                if (fidx < len(nums)-1):
                    PermutateArray(newNums, fidx+1, ansArray, ans)
                if (fidx == len(nums)-1):    
                    ans.append(list(ansArray))
        idx += 1
