
# @Title: Longest Substring Without Repeating Characters (Longest Substring Without Repeating Characters)
# @Author: pcysl
# @Date: 2017-11-03 11:22:19
# @Runtime: 259 ms
# @Memory: N/A

class Solution:
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        if (len(s) > 0):
            testStr  = s
            subStr   = testStr[0]
            optStr   = subStr
            optStrlen= 1
           # iniFlag  = 1
            for index in range(1,len(testStr)):
                testChar  = testStr[index]
                subStrlen = len(subStr)
                ResetFlag = 0
                for jindex in range(0,subStrlen):
                    subChar = subStr[jindex]
                    if (subChar == testChar):
                        ResetFlag = 1
                        break
                if (ResetFlag == 1):
                    if (optStrlen < subStrlen):
                        optStr = subStr
                        optStrlen = subStrlen
                        #iniFlag = 0
                    subStr = subStr[jindex+1:]+testChar
                else:
                    subStr = subStr+testChar
                
                if (index == len(testStr)-1):
                    """
                    if (iniFlag == 1):
                        optStrlen = len(subStr)
                        optStr    = subStr
                        """
                    if (optStrlen < len(subStr)):
                        optStr = subStr
                        optStrlen = len(subStr)
                
            
        else:
            optStrlen = 0
            
        return optStrlen

