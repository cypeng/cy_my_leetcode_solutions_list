
# @Title: 不同路径 (Unique Paths)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-16 01:15:56
# @Runtime: 44 ms
# @Memory: 15 MB

class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        # from (0,0) to (2,6), m = 3, n = 7
        F = [[0]*n]*m
        for idx in range(0, m):
            for jdx in range(0, n):
                if (idx == 0 and jdx > 0) or (idx == 0 and jdx == 0):
                    F[idx][jdx] = 1
                elif idx > 0 and jdx > 0:
                    F[idx][jdx] = F[idx-1][jdx] + F[idx][jdx-1]
        return F[idx][jdx]
        


            
