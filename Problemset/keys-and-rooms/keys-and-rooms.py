
# @Title: 钥匙和房间 (Keys and Rooms)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-06 01:07:31
# @Runtime: 52 ms
# @Memory: 15.4 MB

def visitedRooms(rooms, this_key, seens):
    while not rooms[this_key] is None and not this_key in seens:
        seens.append(this_key)
        for key in rooms[this_key]:
            visitedRooms(rooms, key, seens)
        rooms[this_key] = None
    return rooms

class Solution:
    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:
        visited_rooms = visitedRooms(rooms, 0, [])
        for room in visited_rooms:
            if not room == None:
                return False
        return True

