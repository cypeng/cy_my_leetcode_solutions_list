
# @Title: 打家劫舍 (House Robber)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-20 00:14:07
# @Runtime: 36 ms
# @Memory: 15 MB

class Solution:
    def rob(self, nums: List[int]) -> int:
        dp = [0 for idx in range(0,len(nums))]
        dp[0] = nums[0]
        for idx in range(1,len(nums)):
            if idx > 1:
                dp[idx] = max(dp[idx-2]+nums[idx], dp[idx-1])
            else:
                dp[idx] = max(nums[0], nums[1])
        #print(dp)
        return dp[-1]
