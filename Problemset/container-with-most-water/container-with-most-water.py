
# @Title: 盛最多水的容器 (Container With Most Water)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-07 22:07:32
# @Runtime: 104 ms
# @Memory: 24.6 MB

def area_cal(height1, height2, length):
    if height1 <= height2:
        return height1*length
    else:
        return height2*length

class Solution:
    def maxArea(self, height: List[int]) -> int:
        idx, jdx = 0, len(height)-1
        max_area = area_cal(height[idx], height[jdx], jdx-idx)
        while idx < jdx:
            if idx == jdx:
                break
            area = area_cal(height[idx], height[jdx], jdx-idx)
            if area > max_area:
                max_area = area
                
            if height[idx] <= height[jdx]:
                temp_idx = idx
                while height[idx] <= height[temp_idx]:
                    idx += 1
                    if idx == jdx:
                        break
            else:
                temp_jdx = jdx
                while height[jdx] <= height[temp_jdx]:
                    jdx -= 1
                    if idx == jdx:
                        break 
        return max_area           
             
