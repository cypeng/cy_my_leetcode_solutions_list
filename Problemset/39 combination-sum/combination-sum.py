
# @Title: Combination Sum (Combination Sum)
# @Author: pcysl
# @Date: 2018-07-23 14:06:08
# @Runtime: 96 ms
# @Memory: N/A

class Solution:
    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        ans = []
        ansArray = []
        thisCD = list(sorted(candidates))
        toFindSol(thisCD, target, len(candidates)-1, ansArray, ans)
        return ans

def toFindSol(candidates, target, iniIdx, ansArray, ans):  
    idx    = iniIdx
    while (idx >= 0):
        thisCD = list(candidates)
        #print(idx,thistarget, candidates, thisAnsArray)
        if ((idx > 0) and (target > thisCD[idx])):
            for jdx in range(1, int(target/thisCD[idx])+1):
                thisAnsArray = list(ansArray)
                thistarget = target - thisCD[idx]*jdx
                temp = [thisCD[idx]]*jdx
                thisAnsArray.extend(temp)
                toFindSol(thisCD, thistarget, idx-1, list(thisAnsArray), ans)
        if ((target % thisCD[idx] == 0) and (target >= thisCD[idx])):
            thisAnsArray = list(ansArray)  
            temp = [thisCD[idx]]*int(target/thisCD[idx])
            thisAnsArray.extend(temp)
            ans.append(list(thisAnsArray))    
        idx -= 1
