
# @Title: Remove Nth Node From End of List (Remove Nth Node From End of List)
# @Author: pcysl
# @Date: 2019-07-09 12:36:47
# @Runtime: 40 ms
# @Memory: 13.3 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

def LengthListNode(head):
    i = 0
    while (head is not None):
        head = head.next
        i += 1

    return i

def find_connect_head(head, ith):
    answer = head
    for idx in range(1, ith+2):
        if idx is ith-1:
            head1 = head
        if idx is ith+1:
            head2 = head
            break
        head = head.next
    print(head1, head2)
    head1.next = head2
    return answer

def remove_i_listNode(head, ith, length_ListNode):
    answer = head
    if head is None:
        return head
    if ith == length_ListNode:
        if (length_ListNode-1 is 0):
            answer = None
            return answer
        for idx in range(1, length_ListNode-1):
            head = head.next
        if head is not None:
            head.next = None
        return answer
    if ith == 1:
        if (head.next is not None):
           head = head.next
           answer = head
        else:
            answer = None
        return answer

    answer = find_connect_head(head, ith)
    return answer

def remove_listNode(head, n):
    length_ListNode = LengthListNode(head)
    ith = length_ListNode - n + 1
    head = remove_i_listNode(head, ith, length_ListNode)
    return head

class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        return remove_listNode(head, n)
        
