
# @Title: 两数之和 II - 输入有序数组 (Two Sum II - Input array is sorted)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-23 20:03:30
# @Runtime: 56 ms
# @Memory: 15.4 MB

class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        if len(numbers) == 2:
            return [1, 2]
        idx = 0
        while idx <= len(numbers)-2:
            this_target = target - numbers[idx]
            jdx, kdx = idx+1, len(numbers)-1
            while jdx <= kdx:
                #print(this_target)
                if this_target == numbers[jdx]:
                    return [idx+1, jdx+1]
                if this_target == numbers[kdx]:
                    return [idx+1, kdx+1]
                jdx += 1
                kdx -= 1
            idx += 1
            while idx <= len(numbers)-2 and numbers[idx] == numbers[idx-1]:
                idx+=1
