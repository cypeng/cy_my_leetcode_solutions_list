
# @Title: Set Matrix Zeroes (Set Matrix Zeroes)
# @Author: pcysl
# @Date: 2018-11-13 15:03:10
# @Runtime: 108 ms
# @Memory: N/A

def setZeroes(matrix):
    coord = find_zeros_coord(matrix)
    for idx in range(0, len(coord)):
        set_matrix_zeros(coord[idx][0],coord[idx][1],matrix)

def set_matrix_zeros(i,j,matrix):
    #print(i,j)
    matrix[i][:] = [0]*len(matrix[0])
    for kdx in range(0, len(matrix)):
        matrix[kdx][j] = 0

def find_zeros_coord(matrix):
    i, j = 0, 0
    coord = []
    while ((i < len(matrix)) and (j < len(matrix[0]))):
        #print(i,j)
        if (matrix[i][j] == 0):
            #print(0,matrix,i,j)
            coord.append([i, j])
        j += 1
        if (j == len(matrix[0])):
            i += 1
            j = 0
    return coord

class Solution:
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        setZeroes(matrix)
