
# @Title: 反转字符串 (Reverse String)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-13 00:48:00
# @Runtime: 68 ms
# @Memory: 42.4 MB

def reverseChar(s, idx, jdx):
    s[idx], s[jdx] = s[jdx], s[idx]
    if idx+1 <= jdx-1:
        return reverseChar(s, idx+1, jdx-1)

class Solution:
    def reverseString(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """
        return reverseChar(s, 0, len(s)-1)
