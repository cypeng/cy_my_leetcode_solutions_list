
# @Title: 不相交的线 (Uncrossed Lines)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-04 00:11:22
# @Runtime: 156 ms
# @Memory: 15.2 MB

class Solution:
    def maxUncrossedLines(self, nums1: List[int], nums2: List[int]) -> int:
        #       1   6   7   8   9   10   2   3   4   5   9   11   3   5
        #  7    0   0   1   1   1       
        #  9    0   0   0   1   1
        #  1    1   1   1   1   1
        #  8        1   1   2   2
        #  2        1   1   2   2
        #  9        1   1   2   3
        #  2        1   1   2
        #  3        1   1   2
        # 11        1   1   2
        #  6        2   2   2
        #  5                                 
        #  4                                 
        #  3                                 
        dp1 = [[0 for jdx in range(0, len(nums2)+1)] for idx in range(0, len(nums1)+1)]
        for idx in range(0, len(nums1)):
            for jdx in range(0, len(nums2)):
                #print('check', idx, jdx, dp1)
                if nums1[idx] == nums2[jdx]:
                    dp1[idx+1][jdx+1] = dp1[idx][jdx]+1
                else:
                    #print(idx, jdx, dp1)
                    dp1[idx+1][jdx+1] = max(dp1[idx][jdx+1], dp1[idx+1][jdx])
        return dp1[-1][-1]
