
# @Title: 单词规律 (Word Pattern)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-15 00:02:22
# @Runtime: 36 ms
# @Memory: 15 MB

class Solution:
    def wordPattern(self, pattern: str, s: str) -> bool:
        s_array = s.split()
        hashset = [None]*26
        hashset1 = []
        if len(pattern) > len(s_array):
            return False
        for idx,item in enumerate(s_array):
            #print(item, idx, pattern,hashset1)
            if (idx > len(pattern)-1):
                return False
            if hashset[ord(pattern[idx])-ord('a')] == None:
                if item in hashset1:
                    return False 
                hashset[ord(pattern[idx])-ord('a')] = item
                hashset1.append(item)
            elif (not hashset[ord(pattern[idx])-ord('a')] == item):
                return False
        return True
