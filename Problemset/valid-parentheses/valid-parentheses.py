
# @Title: 有效的括号 (Valid Parentheses)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-08 21:25:39
# @Runtime: 44 ms
# @Memory: 15 MB

class Solution:
    # Stack Solution
    def isValid(self, s: str) -> bool:
        if len(s)%2 > 0:
            return False

        len_s = len(s)
        output_len_s = len(s)//2
        output = [0]*output_len_s
        jdx = 0
        for idx in range(0, len_s):
            char = s[idx]
            if char == '(':
                if jdx >= output_len_s:
                    return False
                output[jdx] = 1
                jdx += 1
            elif char == '{':
                if jdx >= output_len_s:
                    return False
                output[jdx] = 2
                jdx += 1
            elif char == '[':
                if jdx >= output_len_s:
                    return False
                output[jdx] = 4
                jdx += 1
            elif char == ')':
                if output[jdx-1] != 1:
                   return False
                output[jdx-1] = 0
                jdx -= 1
            elif char == '}':
                if output[jdx-1] != 2:
                   return False
                output[jdx-1] = 0
                jdx -= 1
            elif char == ']':
                if output[jdx-1] != 4:
                   return False
                output[jdx-1] = 0
                jdx -= 1
        if jdx == 0:
            return True
        return False
            


