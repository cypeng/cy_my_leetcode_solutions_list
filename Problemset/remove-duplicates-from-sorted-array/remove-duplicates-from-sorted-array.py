
# @Title: 删除有序数组中的重复项 (Remove Duplicates from Sorted Array)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 17:53:27
# @Runtime: 56 ms
# @Memory: 15.8 MB

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        if len(nums) <= 1:
            return 1
        idx, jdx = 0, 0
        while jdx <= len(nums)-1:

            if nums[idx] != nums[jdx]:
                idx += 1
                nums[idx] = nums[jdx]
            jdx += 1
        return idx+1
