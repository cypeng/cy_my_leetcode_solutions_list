
# @Title: 最大子序和 (Maximum Subarray)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-15 23:37:42
# @Runtime: 56 ms
# @Memory: 15.4 MB

class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        if len(nums) == 1:
            return nums[0]
        idx, thissum, maxSum = 0, nums[0], nums[0]
        idx += 1
        while idx < len(nums):
            if (thissum + nums[idx] <= nums[idx]):
                thissum = nums[idx]
            else:
                thissum += nums[idx]
            if (thissum > maxSum):
                maxSum = thissum
            idx += 1
        return maxSum
