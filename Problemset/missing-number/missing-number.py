
# @Title: 丢失的数字 (Missing Number)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-21 12:05:30
# @Runtime: 40 ms
# @Memory: 15.8 MB

class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        nums.sort()
        for idx in range(0, len(nums)):
            if (not nums[idx] == idx):
                return idx
        return len(nums)
