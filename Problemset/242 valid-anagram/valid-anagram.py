
# @Title: Valid Anagram (Valid Anagram)
# @Author: pcysl
# @Date: 2018-01-12 00:01:39
# @Runtime: 112 ms
# @Memory: N/A

class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        Hash = {}
        if (len(s) == len(t)):
            for idx in range(0,len(s)):
                c1 = s[idx]
                if (not c1 in Hash):
                    Hash[c1] = 1
                else:
                    Hash[c1] = Hash[c1]+1
            
            for jdx in range(0,len(t)):
                c2 = t[jdx]
                if (not c2 in Hash):
                    return bool(0)
                else:
                    Hash[c2] = Hash[c2]-1
                    if (Hash[c2] == 0):
                        del Hash[c2]
            
            return bool(1)
            
        else:
            return bool(0)
