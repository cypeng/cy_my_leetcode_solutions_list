
# @Title: 翻转字符串里的单词 (Reverse Words in a String)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-22 14:24:49
# @Runtime: 44 ms
# @Memory: 15.2 MB

class Solution:
    def reverseWords(self, s: str) -> str:
        s_list = s.split()
        this_str = ""
        for idx, item in enumerate(s_list[::-1]):
            if idx > 0:
                this_str = this_str +" "
            this_str = this_str+item
        #print(s_list)
        return this_str
