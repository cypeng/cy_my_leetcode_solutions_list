
# @Title: 颜色分类 (Sort Colors)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-23 15:38:47
# @Runtime: 40 ms
# @Memory: 14.8 MB

def swap(nums, idx, jdx):
    temp = nums[idx]
    nums[idx] = nums[jdx]
    nums[jdx] = temp

class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        idx = 0
        while idx <= len(nums)-1:
            while idx <= len(nums)-1 and (nums[idx] == 0):
                idx += 1
            for jdx in range(idx+1, len(nums)):
                if nums[jdx] < nums[idx]:
                    swap(nums, idx, jdx)
            idx += 1
