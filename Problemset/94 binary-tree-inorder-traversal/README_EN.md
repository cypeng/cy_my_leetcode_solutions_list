
| English | 中文|

# [0094. Binary Tree Inorder Traversal](https://leetcode.com/problems/binary-tree-inorder-traversal/)

## Description

<p>Given a binary tree, return the <em>inorder</em> traversal of its nodes&#39; values.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [1,null,2,3]
   1
    \
     2
    /
   3

<strong>Output:</strong> [1,3,2]</pre>

<p><strong>Follow up:</strong> Recursive solution is trivial, could you do it iteratively?</p>


## Related Topics

- [Hash Table](https://leetcode.com/tag/hash-table)
- [Stack](https://leetcode.com/tag/stack)
- [Tree](https://leetcode.com/tag/tree)

## Similar Questions

- [Validate Binary Search Tree](../validate-binary-search-tree/README_EN.md)
- [Binary Tree Preorder Traversal](../binary-tree-preorder-traversal/README_EN.md)
- [Binary Tree Postorder Traversal](../binary-tree-postorder-traversal/README_EN.md)
- [Binary Search Tree Iterator](../binary-search-tree-iterator/README_EN.md)
- [Kth Smallest Element in a BST](../kth-smallest-element-in-a-bst/README_EN.md)
- [Closest Binary Search Tree Value II](../closest-binary-search-tree-value-ii/README_EN.md)
- [Inorder Successor in BST](../inorder-successor-in-bst/README_EN.md)
- [Convert Binary Search Tree to Sorted Doubly Linked List](../convert-binary-search-tree-to-sorted-doubly-linked-list/README_EN.md)
- [Minimum Distance Between BST Nodes](../minimum-distance-between-bst-nodes/README_EN.md)
