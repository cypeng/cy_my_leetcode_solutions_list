
# @Title: Binary Tree Inorder Traversal (Binary Tree Inorder Traversal)
# @Author: pcysl
# @Date: 2018-02-12 22:02:13
# @Runtime: 78 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.ans = []
        self.InorderTree(root)
        
        return self.ans
    
    def InorderTree(self, root):
        if (root):
            self.InorderTree(root.left)
            self.ans.append(root.val)
            self.InorderTree(root.right)
