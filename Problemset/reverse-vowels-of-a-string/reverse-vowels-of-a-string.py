
# @Title: 反转字符串中的元音字母 (Reverse Vowels of a String)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-24 11:18:28
# @Runtime: 60 ms
# @Memory: 15.6 MB

class Solution:
    def reverseVowels(self, s: str) -> str:
        #s = s.lower()
        if len(s) == 1:
            return s
        idx, jdx = 0, len(s)-1
        s = list(s)
        character_list = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
        while idx <= jdx:
            while idx <= jdx and not s[idx] in character_list:
                idx += 1
            while idx <= jdx and not s[jdx] in character_list:
                jdx -= 1
            if idx <= jdx and s[idx] in character_list and s[jdx] in character_list:
                s[idx], s[jdx] = s[jdx], s[idx]
            idx += 1
            jdx -= 1
        return ''.join(s)
