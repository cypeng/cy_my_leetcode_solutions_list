
# @Title: 旋转图像 (Rotate Image)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-05 00:48:49
# @Runtime: 24 ms
# @Memory: 15.1 MB

class Solution:
    def rotate(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        ans = [[0 for idx in range(0,len(matrix))] for jdx in range(0, len(matrix))]
        for idx in range(0, len(matrix)):
            for jdx in range(0, len(matrix[idx])):
                ans[jdx][len(matrix[idx])-1-idx] = matrix[idx][jdx] 
                #print(idx, jdx, jdx, len(matrix[idx])-1-idx)
        for idx in range(0, len(matrix)):
            for jdx in range(0, len(matrix[idx])):
                matrix[idx][jdx] = ans[idx][jdx]
        return matrix
