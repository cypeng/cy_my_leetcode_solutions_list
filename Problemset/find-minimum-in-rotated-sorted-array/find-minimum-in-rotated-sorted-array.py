
# @Title: 寻找旋转排序数组中的最小值 (Find Minimum in Rotated Sorted Array)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-17 14:07:53
# @Runtime: 32 ms
# @Memory: 15.4 MB

class Solution:
    def findMin(self, nums: List[int]) -> int:
        idx, jdx = 0, len(nums)-1
        if nums[idx] <= nums[jdx]:
            return nums[idx]
        elif (nums[jdx] < nums[idx]) and (nums[jdx] < nums[jdx-1]): 
            return nums[jdx]
        else:
            while 1:
                mid = int((idx+jdx)*0.5)
                #print(idx,jdx,mid)
                if (nums[mid] < nums[mid+1]) and (nums[mid] < nums[mid-1]):
                    return nums[mid]
                if (nums[mid] > nums[idx]) and (nums[mid] > nums[jdx]):
                    idx = mid
                elif (nums[mid] < nums[idx]) and (nums[mid] < nums[jdx]):
                    jdx = mid
            
