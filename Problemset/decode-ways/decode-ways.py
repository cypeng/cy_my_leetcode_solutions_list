
# @Title: 解码方法 (Decode Ways)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-20 00:49:16
# @Runtime: 44 ms
# @Memory: 14.9 MB

class Solution:
    def numDecodings(self, s: str) -> int:
        # F Series
        # 1212 -> ABAB, LL, LAB, ABL, AUB, 1235
        # 12121 -> 12358
        # 121 -> 123
        # Special Case: Not 0
        # 171 -> 122
        # 1717 -> AGAG, QAG, QQ, AGQ, 1224
        # 17171 -> 12244
        # Special Case: 0
        # (20) -> 1
        # (20)6 -> 11
        # (20)21 -> 112
        # 2(10)2 -> 111
        n = len(s)
        f = [1] + [0] * n
        for i in range(1, n + 1):
            if s[i - 1] != '0':
                f[i] += f[i - 1]
            if i > 1 and s[i - 2] != '0' and int(s[i-2:i]) <= 26:
                f[i] += f[i - 2]
        return f[n]

