
# @Title: Min Stack (Min Stack)
# @Author: pcysl
# @Date: 2018-02-05 23:53:48
# @Runtime: 100 ms
# @Memory: N/A

class MinStack(object):

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.data = []
        self.minX = 0
        self.flag = 0

    def push(self, x):
        """
        :type x: int
        :rtype: void
        """
        self.data.append(x)
        if (len(self.data) > 1):
            if (self.minX > x):
                self.minX = x
        elif (len(self.data) == 1):
            self.minX = self.data[0]
            
         
        
    def pop(self):
        """
        :rtype: void
        """ 
        del self.data[len(self.data)-1]
        self.flag = 1
        self.minX = self.getMin()

    def top(self):
        """
        :rtype: int
        """     
        return self.data[len(self.data)-1]

    def getMin(self):
        """
        :rtype: int
        """
        if (self.flag == 1):
            if (len(self.data) > 0):
                self.minX = self.data[0]
                for idx in range(0,len(self.data)):
                    if (self.data[idx] < self.minX ):
                        self.minX = self.data[idx]
            else:
                self.minX = []
        self.flag = 0
        return self.minX


# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(x)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()
