
# @Title: Contains Duplicate (Contains Duplicate)
# @Author: pcysl
# @Date: 2018-01-11 22:47:36
# @Runtime: 91 ms
# @Memory: N/A

class Solution(object):
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        HashTable = {}
        if (nums == []):
            return bool(0)
        
        for idx in range(0,len(nums)):
            num = nums[idx]
            if (not str(num) in HashTable):
                HashTable[str(num)] = 1
            else:
                return bool(1)
            
        return bool(0)
