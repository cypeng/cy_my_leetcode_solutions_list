
# @Title: Single Number (Single Number)
# @Author: pcysl
# @Date: 2018-01-05 12:47:22
# @Runtime: 65 ms
# @Memory: N/A

class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        HashTable = {}
        for idx in range(0,len(nums)):
            num = nums[idx]
            if (not str(num) in HashTable):
                HashTable[str(num)] = 1
            else:
                del HashTable[str(num)]
    
        for key in HashTable:    
            return int(key)
