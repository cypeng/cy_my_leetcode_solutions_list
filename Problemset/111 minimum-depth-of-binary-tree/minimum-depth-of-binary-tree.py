
# @Title: Minimum Depth of Binary Tree (Minimum Depth of Binary Tree)
# @Author: pcysl
# @Date: 2018-03-08 00:03:03
# @Runtime: 56 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def minDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.rootArray = []
        self.rootArray.append(root)
        treeDepth = 1 
        minDepth  = 100000000
        if (root):
            while (self.rootArray):
                rootArray = []
                for i in range(0,len(self.rootArray)):
                    root = self.rootArray[i]
                    if (root.left):
                        rootArray.append(root.left)
                    if (root.right):
                        rootArray.append(root.right)
                    if (not root.left) and (not root.right):
                        if ( treeDepth < minDepth):
                            return treeDepth
                
                treeDepth = treeDepth + 1
                
                self.rootArray = []
                self.rootArray = rootArray
        else:
            return 0
        
