
# @Title: Find Minimum in Rotated Sorted Array (Find Minimum in Rotated Sorted Array)
# @Author: pcysl
# @Date: 2017-12-27 00:25:11
# @Runtime: 105 ms
# @Memory: N/A

class Solution:
    def findMin(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        idx = 0
        jdx = len(nums)-1
        if (nums[idx] <= nums[jdx]):
            return nums[idx]
        else:
            while (idx <= jdx):
                midIdx = int((idx+jdx)*0.5)
                if (nums[midIdx] > nums[-1]):
                    idx = midIdx+1
                elif (nums[midIdx] < nums[0]):
                    jdx = midIdx-1
                
        while (nums[midIdx] > nums[midIdx-1]):
            midIdx = midIdx-1
        if  (nums[midIdx] < nums[midIdx-1]):
            return nums[midIdx]
