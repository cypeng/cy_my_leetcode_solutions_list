
# @Title: 删除链表中的节点 (Delete Node in a Linked List)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-06 21:41:19
# @Runtime: 40 ms
# @Memory: 15.5 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def deleteNode(self, node):
        """
        :type node: ListNode
        :rtype: void Do not return anything, modify node in-place instead.
        """
        if node is None:
            return node
        temp = node
        pre = None
        while temp:
            cur = temp
            temp = temp.next
            if pre:
                cur.val, pre.val = pre.val, cur.val
            if temp and (temp.next is None):
                cur.next = None
            pre = cur
