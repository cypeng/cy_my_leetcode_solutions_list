
# @Title: 杨辉三角 II (Pascal's Triangle II)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-13 01:30:59
# @Runtime: 36 ms
# @Memory: 15 MB

def pascal_ith(ith, Pith_minus, numRows):
    if ith == 0:
        Pith = [1]
    elif ith == 1:
        Pith = [1, 1]
    else:
        Pith = [1]*(ith+1)
        for i in range(1, len(Pith)-1):
            Pith[i] = Pith_minus[i-1]+Pith_minus[i]
    if ith+1 < numRows:
        return pascal_ith(ith+1, Pith, numRows)
    return Pith
   
class Solution:
    def getRow(self, rowIndex: int) -> List[int]:
        return pascal_ith(0, None, rowIndex+1)
