
# @Title: Z 字形变换 (ZigZag Conversion)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-07 21:35:52
# @Runtime: 48 ms
# @Memory: 14.9 MB

class Solution:
    def convert(self, s: str, numRows: int) -> str: 
        group_num = 2*numRows-2
        if group_num == 0:
            return s
        output = ['']*(numRows)
        for idx, character in enumerate(s):
            mod_idx = idx%group_num
            if  mod_idx >= numRows:
                output[group_num-mod_idx] = output[group_num-mod_idx]+character  
            else:
                output[mod_idx] = output[mod_idx] + character
        
        answer = ''
        for character in output: 
            answer = answer + character
        return answer    

