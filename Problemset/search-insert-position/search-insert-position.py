
# @Title: 搜索插入位置 (Search Insert Position)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 18:11:37
# @Runtime: 44 ms
# @Memory: 15.2 MB

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        if target > nums[-1]:
            return len(nums)
        idx, jdx = 0, len(nums)-1
        while 1:
            if nums[idx] < target:
                idx += 1
            else:
                return idx
            if nums[jdx] >= target:
                jdx -= 1
            else:
                return jdx+1

