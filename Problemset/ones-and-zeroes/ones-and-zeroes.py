
# @Title: 一和零 (Ones and Zeroes)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-11 20:01:37
# @Runtime: 2808 ms
# @Memory: 15.3 MB

class Solution:
    def findMaxForm(self, strs: List[str], m: int, n: int) -> int:
        zeros_set = [0 for idx in range(0,len(strs))]
        ones_set = [0 for idx in range(0,len(strs))]
        for idx, item in enumerate(strs):
            zeros_count = 0
            ones_count = 0
            for char in item:
                if char == '0':
                    zeros_count += 1
                else:
                    ones_count += 1
            zeros_set[idx] = zeros_count
            ones_set[idx] = ones_count
        dp = [[0 for idx in range(0,n+1)] for idx in range(0,m+1)]
        #count = 0        
        #print(zeros_set, ones_set, dp0, dp1)
        
        for kdx in range(0, len(strs)):
            for idx in range(m,zeros_set[kdx]-1,-1):
                for jdx in range(n,ones_set[kdx]-1,-1):    
                    #print(idx,jdx)
                    dp[idx][jdx] = max(dp[idx][jdx],dp[idx-zeros_set[kdx]][jdx-ones_set[kdx]]+1)
        #print(dp)        
        return dp[-1][-1]
