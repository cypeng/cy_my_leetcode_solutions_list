
# @Title: Restore IP Addresses (Restore IP Addresses)
# @Author: pcysl
# @Date: 2018-11-09 16:33:49
# @Runtime: 68 ms
# @Memory: N/A

def restoreIpAddresses(s):
    this_ip = ""
    answer = []
    ip_addresses(s,0,0,1,this_ip,answer)
    ip_addresses(s,0,0,2,this_ip,answer)
    ip_addresses(s,0,0,3,this_ip,answer)
    return answer

def ip_addresses(s,count,idx,interval,this_ip,answer):
    add_ip = s[idx:idx+interval]
    #print(s,add_ip,idx,interval,idx+interval)
    if (len(add_ip) == len(str(int(add_ip)))) and ((int(add_ip) >= 0) and (int(add_ip) <= 255)):
        if (count == 0):
            this_ip += add_ip
        else:
            this_ip += '.'+add_ip
        #print(this_ip)
        if ((idx+interval == len(s)) and (count == 3)):
            answer.append(this_ip)
        elif ((idx+interval < len(s)) and (count < 3)):
            ip_addresses(s,count+1,idx+interval,1,this_ip,answer)
            ip_addresses(s,count+1,idx+interval,2,this_ip,answer)
            ip_addresses(s,count+1,idx+interval,3,this_ip,answer)
class Solution:
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        if (len(s) > 0):
            return restoreIpAddresses(s)
        else:
            return []
