
# @Title: Intersection of Two Arrays (Intersection of Two Arrays)
# @Author: pcysl
# @Date: 2018-01-16 00:49:23
# @Runtime: 77 ms
# @Memory: N/A

class Solution(object):
    def intersection(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        if nums1 == None or nums2 == None:
            if nums1 == None:
                return nums1
            else:
                return nums2
        
        Hash = dict()
        print(Hash)
        for idx in range(0,len(nums1)):
            num = nums1[idx]
            if (not num in Hash):
                Hash[num] = 1
        
        ans = []
        for jdx in range(0,len(nums2)):
            num = nums2[jdx]
            if (num in Hash) and (Hash[num] == 1):
                ans.append(num)
                del Hash[num]
                
        return ans
