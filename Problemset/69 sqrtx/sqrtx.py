
# @Title: Sqrt(x) (Sqrt(x))
# @Author: pcysl
# @Date: 2017-12-26 15:24:02
# @Runtime: 112 ms
# @Memory: N/A

class Solution:
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """
        idx = 0
        jdx = x
        while idx <= jdx:
            midIdx = int((idx+jdx)*0.5)
            midnum = midIdx*midIdx
            if (midnum == x):
                return midIdx
            elif (midnum > x):
                jdx = midIdx-1
            elif (midnum < x):
                idx = midIdx+1
    
        if (midnum > x):
            return midIdx-1
        elif (midnum < x):
            return midIdx
