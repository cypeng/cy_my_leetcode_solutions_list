
# @Title: 多数元素 (Majority Element)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-18 00:48:40
# @Runtime: 272 ms
# @Memory: 16.4 MB

def partition(nums, start, end):
    pivot_idx = random.randint(start, end)
    nums[start], nums[pivot_idx] = nums[pivot_idx], nums[start]
    idx, jdx = start+1, end
    while idx < jdx:
        while (idx < jdx) and (nums[idx] < nums[start]):
            idx += 1
        while (idx < jdx) and (nums[jdx] > nums[start]):
            jdx -= 1
        if idx < jdx:
            nums[idx], nums[jdx] = nums[jdx], nums[idx]
            idx += 1
            jdx -= 1
    if (idx == jdx) and (nums[jdx] > nums[start]):
        jdx -= 1
    nums[jdx], nums[start] = nums[start], nums[jdx]  
    return jdx

def quickSort(nums, start, end):
    if start >= end:
        return None
    middle = partition(nums, start, end)
    quickSort(nums, start, middle-1)
    quickSort(nums, middle+1, end)

class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        if len(nums) == 1:
            return nums[0]
        quickSort(nums, 0, len(nums)-1)
        comp = None
        count = 1
        opt = 0
        opt_num = None
        for item in nums:
            #print(nums, comp, item, count)
            if (not comp is None) and (comp == item):
                count += 1
            else:
                count = 1
            #print(count)
            if count > opt:
                opt = count
                opt_num = item
            comp = item
        return opt_num
