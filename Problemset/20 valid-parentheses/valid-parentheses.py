
# @Title: Valid Parentheses (Valid Parentheses)
# @Author: pcysl
# @Date: 2018-02-02 15:02:30
# @Runtime: 40 ms
# @Memory: N/A

class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        jdx = -1
        idx = 0
        nextStack = ['None']*len(s)
        if (len(s)%2 == 0):
            for idx in range(0,len(s)):
                if (')' == nextStack[jdx]) and (s[idx] == nextStack[jdx]):
                    nextStack[jdx] = 'None'
                    jdx = jdx -1
                elif ('}' == nextStack[jdx]) and (s[idx] == nextStack[jdx]):
                    nextStack[jdx] = 'None'
                    jdx = jdx -1                
                elif (']' == nextStack[jdx]) and (s[idx] == nextStack[jdx]):
                    nextStack[jdx] = 'None'
                    jdx = jdx -1                
                
                if (s[idx] == '('):
                    jdx = jdx + 1
                    nextStack[jdx] = ')' 
                elif (s[idx] == '{'):
                    jdx = jdx + 1
                    nextStack[jdx] = '}'                      
                elif (s[idx] == '['):
                    jdx = jdx + 1
                    nextStack[jdx] = ']'
                
            if (jdx < 0):
                return bool(1)
            else:
                return bool(0)
        else:
            return bool(0)
