
# @Title: 第一个错误的版本 (First Bad Version)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-17 00:48:00
# @Runtime: 44 ms
# @Memory: 14.9 MB

# The isBadVersion API is already defined for you.
# @param version, an integer
# @return an integer
# def isBadVersion(version):
def checkBad(nBad):
    
    return False

class Solution:
    def firstBadVersion(self, n):
        """
        :type n: int
        :rtype: int
        """
        idx, jdx = 1, n
        while 1:
            mid = int((idx + jdx)*0.5)
            if ((mid > 1) and isBadVersion(mid) and not isBadVersion(mid-1)) or ((mid == 1) and isBadVersion(mid)):
                return mid
            if not isBadVersion(mid):
                idx = mid + 1
            else:
                jdx = mid - 1

