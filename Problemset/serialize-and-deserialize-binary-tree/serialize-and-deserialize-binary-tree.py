
# @Title: 二叉树的序列化与反序列化 (Serialize and Deserialize Binary Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-01 23:33:52
# @Runtime: 116 ms
# @Memory: 20.2 MB

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

def traversal(this_roots, ans):
    next_roots = []
    flag = False
    for root in this_roots:
        if root:
            ans.append(str(root.val))
            if root.left:
                next_roots.append(root.left)
                flag = True
            else:
                next_roots.append(None)
            if root.right:
                next_roots.append(root.right)
                flag = True
            else:
                next_roots.append(None)
        else:
            ans.append('None')
    if flag:
        ans = traversal(next_roots, ans)
    return ans

def buildTree(data, pre_roots, idx, head):
    next_roots = []
    count = 0
    if idx == 0:
        this_root = TreeNode(data[idx], None, None)
        head = this_root
        next_roots.append(this_root)
        idx += 1
    while count <= len(pre_roots)-1:
        #print(data, idx)
        this_root = pre_roots[count]
        if not this_root is None:
            if idx <= len(data)-1 and not data[idx] is 'None':
                root = TreeNode(data[idx], None, None)
                this_root.left = root
                next_roots.append(root)
            idx += 1
            if idx <= len(data)-1 and not data[idx] is 'None':
                root = TreeNode(data[idx], None, None)
                this_root.right = root
                next_roots.append(root)
            idx += 1
        count += 1
    if len(next_roots) > 0:
        #print(next_roots, head)
        head = buildTree(data, next_roots, idx, head)
    return head

class Codec:
    def serialize(self, root):
        """Encodes a tree to a single string.
        
        :type root: TreeNode
        :rtype: str
        """
        ans = []
        if root is None:
            return []
        ans = traversal([root], ans)
        return ans

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        if len(data) == 0:
            return []
        head = buildTree(data, [], 0, None)
        return head

# Your Codec object will be instantiated and called as such:
# ser = Codec()
# deser = Codec()
# ans = deser.deserialize(ser.serialize(root))
