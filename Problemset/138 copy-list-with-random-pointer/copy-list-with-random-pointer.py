
# @Title: Copy List with Random Pointer (Copy List with Random Pointer)
# @Author: pcysl
# @Date: 2019-02-23 14:58:48
# @Runtime: 128 ms
# @Memory: 15.5 MB

"""
# Definition for a Node.
class Node:
    def __init__(self, val, next, random):
        self.val = val
        self.next = next
        self.random = random
"""

def getNodeLength(head):
    count = 0
    while (head != None):
        count += 1
        head = head.next
    return count

def toListType(head, length):
    valListArray = [None]*length
    idListArray = [None]*length
    nextListArray = [None]*length
    randomArray = [None]*length
    idx = 0
    while (head != None):
        valListArray[idx] = head.val
        idListArray[idx] = head
        nextListArray[idx] = head.next
        randomArray[idx] = head.random
        idx += 1
        head = head.next
    print(randomArray)
    return valListArray, idListArray, nextListArray, randomArray

def createNode(head, valListArray, length):
    idx = 0
    idNodeArray = [None]*length
    while (head != None):
        idNodeArray[idx] = Node(valListArray[idx], None, None)
        idx += 1
        head = head.next
    return idNodeArray

def connectNode(idNodeArray, idListArray, nextListArray, randomArray):
    for idx in range(0, len(idNodeArray)):
        if (idx == 0):
            answer = idNodeArray[idx]

        flag1, flag2 = 0, 0
        for jdx in range(0, len(idNodeArray)):
            if (randomArray[idx] == idListArray[jdx]):
                idNodeArray[idx].random = idNodeArray[jdx]
                flag1 = 1

            if (nextListArray[idx] == idListArray[jdx]):
                idNodeArray[idx].next = idNodeArray[jdx]
                flag2 = 1

            if ((flag1) and (flag2)):
                break
            jdx += 1
    return answer
    
def main(head):
    if (head == None):
        return head
    length = getNodeLength(head)
    valListArray, idListArray, nextListArray, randomArray = toListType(head, length)
    idNodeArray = createNode(head, valListArray, length)
    answer = connectNode(idNodeArray, idListArray, nextListArray, randomArray)
    return answer
    

class Solution:
    def copyRandomList(self, head: 'Node') -> 'Node':
        return main(head)
