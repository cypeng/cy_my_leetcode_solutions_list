
# @Title: Unique Paths (Unique Paths)
# @Author: pcysl
# @Date: 2019-02-25 17:23:23
# @Runtime: 40 ms
# @Memory: 13.2 MB

def main(m, n):
    if (m == 0) or (n == 0):
        return 0
    if (m == 1) or (n == 1):
        return 1

    path_cost_map = []

    for idx in range(0, m):
        this_row = [0]*n
        path_cost_map.append(this_row)

    for idx in range(0, m):
        for jdx in range(0, n):
            if ((idx == 0) and (jdx == 0)):
                path_cost_map[idx][jdx] = 1
            else:
                if ((idx > 0) and (jdx > 0)):
                    path_cost_map[idx][jdx] = path_cost_map[idx-1][jdx] + path_cost_map[idx][jdx-1] + path_cost_map[idx][jdx]
                elif ((idx > 0) and (jdx == 0)):
                    path_cost_map[idx][jdx] = path_cost_map[idx-1][jdx] + path_cost_map[idx][jdx]
                elif ((idx == 0) and (jdx > 0)):
                    path_cost_map[idx][jdx] = path_cost_map[idx][jdx-1] + path_cost_map[idx][jdx]
    #idx, jdx = 0, 0
    #next_route(path_cost_map, m, n, idx, jdx, idx+1, jdx)
    #next_route(path_cost_map, m, n, idx, jdx, idx, jdx+1)
           
    #print(path_cost_map)
    return path_cost_map[m-1][n-1]

class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        return main(m, n)
        
