
# @Title: 整数拆分 (Integer Break)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-10 00:54:22
# @Runtime: 32 ms
# @Memory: 14.8 MB

class Solution:
    def integerBreak(self, n: int) -> int:
        opt, max_product = 0, 0
        for num in range(2,n+1):
            if n%num == 0:
                max_product = (n//num)**num
            else:
                perfect_pow = num-n%num
                max_product = (n//num)**(perfect_pow) * ((n//num)+1)**(n%num)
                #print(n, num, n%num, n//num, max_product)
                
            if opt < max_product:
                opt = max_product
                
        return opt
