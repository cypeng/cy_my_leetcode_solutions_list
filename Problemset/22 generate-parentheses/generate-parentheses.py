
# @Title: Generate Parentheses (Generate Parentheses)
# @Author: pcysl
# @Date: 2018-07-18 14:05:57
# @Runtime: 40 ms
# @Memory: N/A

class Solution:
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        cList = [n,0,0]
        ans   = ''
        ansList = []
        if (n == 0):
            return [""]
        else:
            IniParenthesis(cList, n, ans, ansList)
            return ansList    

def IniParenthesis(cList, n, ans, ansList):   
    PList = ['(',')']
    ans = PList[0]
    FinalParenthesis([cList[0]-1,cList[1],cList[2]+1], n, ans, ansList)

def FinalParenthesis(cList, n, ans, ansList):
    PList = ['(',')']
    #print(cList)
    if ((cList[0] > 0) and (cList[2] > 0)):
        FinalParenthesis([cList[0]-1,cList[1],cList[2]+1], n, ans + PList[0], ansList)
        FinalParenthesis([cList[0],cList[1]+1,cList[2]-1], n, ans + PList[1], ansList)
    if ((cList[0] > 0) and (cList[2] == 0)):
        FinalParenthesis([cList[0]-1,cList[1],cList[2]+1], n, ans + PList[0], ansList)
    if ((cList[0] == 0) and (cList[2] > 0)):
        ansList.append(ans + PList[1]*cList[2])
