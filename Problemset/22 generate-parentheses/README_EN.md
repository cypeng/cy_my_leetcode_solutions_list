
| English | 中文|

# [0022. Generate Parentheses](https://leetcode.com/problems/generate-parentheses/)

## Description

<p>
Given <i>n</i> pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
</p>

<p>
For example, given <i>n</i> = 3, a solution set is:
</p>
<pre>
[
  "((()))",
  "(()())",
  "(())()",
  "()(())",
  "()()()"
]
</pre>

## Related Topics

- [String](https://leetcode.com/tag/string)
- [Backtracking](https://leetcode.com/tag/backtracking)

## Similar Questions

- [Letter Combinations of a Phone Number](../letter-combinations-of-a-phone-number/README_EN.md)
- [Valid Parentheses](../valid-parentheses/README_EN.md)
