
# @Title: 二叉树的后序遍历 (Binary Tree Postorder Traversal)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 13:24:10
# @Runtime: 32 ms
# @Memory: 14.5 MB

def traversal(root, answer):
    if root:
        if root.left:
            #answer.append(root.left.val) 
            traversal(root.left, answer)
            #answer.append(root.val)
        if root.right:
            #answer.append(root.right.val)
            traversal(root.right, answer)
        answer.append(root.val)

class Solution:
    def postorderTraversal(self, root: TreeNode) -> List[int]:
        answer = []
        
        traversal(root, answer)
        return answer
