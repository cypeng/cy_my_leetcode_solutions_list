
# @Title: Binary Tree Zigzag Level Order Traversal (Binary Tree Zigzag Level Order Traversal)
# @Author: pcysl
# @Date: 2018-02-14 13:50:38
# @Runtime: 64 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def zigzagLevelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        self.data = []
        self.level = []
        thislevel = []
        
        if (root):
            self.level.append(root) 
            count = 0
            while (len(self.level)):
                thislevel = []
                thislevel = self.level
                self.level = []
                self.leveldata = []
                for idx in range(0,len(thislevel)):
                    root = thislevel[idx]  
                    self.levelarray(root)
                if (count%2 == 0):
                    self.data.append(self.leveldata)
                else:
                    self.data.append(self.leveldata[::-1])
                count = count +1
                
        return self.data
        
    def levelarray(self, root): 
        self.leveldata.append(root.val)
        if (root.left):
            self.level = self.level + [root.left]
        if (root.right):
            self.level = self.level + [root.right]

            
