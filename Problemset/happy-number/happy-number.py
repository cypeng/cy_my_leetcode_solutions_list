
# @Title: 快乐数 (Happy Number)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-26 14:22:03
# @Runtime: 40 ms
# @Memory: 15 MB

class Solution:
    def isHappy(self, n: int) -> bool:
        hashset = set()
        num = str(n)
        while not num == "1":
            sum_num = 0
            #print(num)
            for item in num:
                item = int(item)         
                sum_num += item*item
            if (sum_num in hashset):
                return False
            hashset.add(sum_num)
            num = str(sum_num)
        return True
