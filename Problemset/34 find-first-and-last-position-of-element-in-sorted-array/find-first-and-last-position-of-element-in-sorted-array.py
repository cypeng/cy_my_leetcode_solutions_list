
# @Title: Find First and Last Position of Element in Sorted Array (Find First and Last Position of Element in Sorted Array)
# @Author: pcysl
# @Date: 2017-12-26 10:01:55
# @Runtime: 68 ms
# @Memory: N/A

class Solution:
    def searchRange(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        answer = [-1,-1]
        idx = 0
        jdx = len(nums)-1
        Flag = 0
        iniIdx = -1
        if (len(nums) > 0):
            while (Flag == 0):
                mididx = int((idx+jdx)*0.5)
                if (nums[mididx] == target):
                    iniIdx = mididx
                    Flag = 1
                elif (nums[mididx] < target):
                    idx = mididx+1
                elif (nums[mididx] > target):
                    jdx = mididx-1
                if (jdx < idx):
                    iniIdx = -1
                    break
    
    
        idx = iniIdx
        jdx = iniIdx
        answer = [idx, jdx]
        while (idx > 0) and (nums[idx-1] == target):
            idx = idx-1
            answer[0] = idx
        while (jdx < len(nums)-1) and (nums[jdx+1] == target):
            jdx = jdx+1
            answer[1] = jdx
         
        return answer
