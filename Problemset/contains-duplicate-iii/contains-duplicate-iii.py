
# @Title: 存在重复元素 III (Contains Duplicate III)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-10 00:00:03
# @Runtime: 96 ms
# @Memory: 17.2 MB


class Solution:
    def containsNearbyAlmostDuplicate(self, nums: List[int], k: int, t: int) -> bool:
        bucket = {}
        for idx in range(0, len(nums)):
            nth = nums[idx]//(t+1)
            #print(nth, bucket)
            if nth in bucket:
                return True
            bucket[nth] = nums[idx]
            if (nth - 1) in bucket and abs(bucket[nth-1]-bucket[nth]) <= t:
                return True
            if (nth + 1) in bucket and abs(bucket[nth+1]-bucket[nth]) <= t:
                return True
            #print('end', nums[idx-k], nums[idx-k]//(t-1))
            if idx >= k:
                if nums[idx-k]//(t+1) in bucket:
                    bucket.pop(nums[idx-k]//(t+1))
            #print(nth, bucket, nth - 1)
        return False
