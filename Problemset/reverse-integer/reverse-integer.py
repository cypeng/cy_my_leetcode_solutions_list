
# @Title: 整数反转 (Reverse Integer)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-08 14:32:09
# @Runtime: 36 ms
# @Memory: 14.7 MB

class Solution:
    def reverse(self, x: int) -> int:
        output =''
        for character in str(abs(x)):
            output = character+output
        output = int(output) 
        if x < 0:
            output = -1*output
        
        if (output > (2**31)-1) or (output < -2**31):
            return 0

        return output

