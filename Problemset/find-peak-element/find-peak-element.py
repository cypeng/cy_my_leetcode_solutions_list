
# @Title: 寻找峰值 (Find Peak Element)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-17 12:20:44
# @Runtime: 48 ms
# @Memory: 15 MB

def checkPeak(index, nums):
    if index == 0:
        if nums[index] > nums[index+1]:
            return True
    elif index == len(nums)-1:
        if nums[index] > nums[index-1]:
            return True
    else: 
        if (nums[index] > nums[index+1]) and (nums[index] > nums[index-1]):
            return True
    return False

def findPeakLoop(nums, idx, jdx):
    mid = int((idx + jdx)*0.5)
    #print(idx,jdx,mid)
    if checkPeak(mid, nums):
        return mid
    elif checkPeak(idx, nums):
        return idx
    elif checkPeak(jdx, nums):
        return jdx
    if idx < mid and (mid - idx) > 1:
        index = findPeakLoop(nums, idx, mid)
        if not index == None:
            return index 
    if jdx > mid and (jdx - mid) > 1:
        index = findPeakLoop(nums, mid, jdx)
        if not index == None:
            return index 

class Solution:
    def findPeakElement(self, nums: List[int]) -> int:
        if len(nums) == 1:
            return 0
        idx, jdx = 0, len(nums)-1
        return findPeakLoop(nums, idx, jdx)
