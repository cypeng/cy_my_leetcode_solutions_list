
# @Title: 3Sum (3Sum)
# @Author: pcysl
# @Date: 2017-12-12 00:43:46
# @Runtime: 1445 ms
# @Memory: N/A

class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        newnums = nums
        

        """
        for idx in range(0,len(nums)):
            for jdx in range(idx+1,len(nums)):
                if (nums[idx] > nums[jdx]):
                    temp = newnums[jdx]
                    newnums[jdx] = newnums[idx]
                    newnums[idx] = temp
        """
        newnums = sorted(nums)
        answer = []
            
        if (len(nums) >= 3):
            a = []
            for idx in range(0,len(nums)-2):
                if ((newnums[idx] <= 0) and (newnums[idx] != a)):
                    a = newnums[idx]
                    b = []
                    c = []
                    
                    jdx = idx + 1
                    kdx = len(nums)-1
                    while (jdx < kdx):
                        b = newnums[jdx]
                        c = newnums[kdx] 
                        if (b + c == -a):
                            answer.append([a,b,c])
                            jdx = jdx+1
                            kdx = kdx-1
                            while ((jdx< kdx) and (newnums[jdx] == newnums[jdx-1])):
                                jdx = jdx+1
                            while ((jdx< kdx) and (newnums[kdx] == newnums[kdx+1])):
                                kdx = kdx-1
                        elif (newnums[jdx] + newnums[kdx] < -a):
                            jdx = jdx+1
                        else:
                            kdx = kdx-1
                    """
                    for jdx in range(idx+1,len(nums)-1):
                        if (newnums[jdx] != b):
                            b = newnums[jdx]
                            tar = -(a+b)
                            c = []
                            for kdx in range(jdx+1, len(nums)):
                                if ((newnums[kdx] >= 0) and (newnums[kdx] != c)):
                                    c = newnums[kdx]
                                    if (c==tar):
                                        answer.append([a,b,c])                    
                    """

                
                  
        return answer

def quickSort(alist):
    quickSortHelper(alist,0,len(alist)-1)

def quickSortHelper(alist,first,last):
    if first<last:
        splitpoint = partition(alist,first,last)
        
        quickSortHelper(alist,first,splitpoint-1)
        quickSortHelper(alist,splitpoint+1,last)


def partition(alist,first,last):
    pivotvalue = alist[first]

    leftmark = first+1
    rightmark = last

    done = False
    while not done:

        while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
            leftmark = leftmark + 1

        while alist[rightmark] >= pivotvalue and rightmark >= leftmark:
            rightmark = rightmark -1

        if rightmark < leftmark:
            done = True
        else:
            temp = alist[leftmark]
            alist[leftmark] = alist[rightmark]
            alist[rightmark] = temp

    temp = alist[first]
    alist[first] = alist[rightmark]
    alist[rightmark] = temp


    return rightmark    
