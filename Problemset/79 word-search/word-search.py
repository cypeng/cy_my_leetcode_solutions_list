
# @Title: Word Search (Word Search)
# @Author: pcysl
# @Date: 2018-11-09 11:55:23
# @Runtime: 380 ms
# @Memory: N/A

def exist(board, word):
    if (len(word) > 0):
        answer = __find_word__(board, word)
    else:
        answer = bool(1)

    return answer

def __find_word__(board, word):
    answer = []
    answer.append(bool(0))
    first_word_list = __find_first_word_list__(board, word[0])
    for kdx in range(0, len(first_word_list)):
        #print(first_word_list[kdx])
        __find_word_to_forward__(answer,[],board,first_word_list[kdx][0],first_word_list[kdx][1],0,word)
        if (answer[0]):
            break
    #__find_word_to_forward__(answer,[],board,0,0,-1,len(word)-1,word)
    return answer[0]

def __find_first_word_list__(board, first_word):
    first_word_list = []
    for kdx in range(0, len(board)):
        i, j = 0, len(board[kdx])-1
        while (i <= j):
            if (first_word == board[kdx][i]):
                #yield list([kdx, i])
                first_word_list.append([kdx, i])
            if (first_word == board[kdx][j]):
                #yield list([kdx, j])
                first_word_list.append([kdx, j])
            i += 1
            j -= 1
    return first_word_list
    #first_word_list = (kdx, i)
    #print(11,first_word_list)

def __find_word_to_forward__(answer,road_list,board,idx,jdx,word_idx,word):
    if (not answer[0]) and (word_idx >= 0) and (word_idx <= len(word)-1):
        #if (word_idx == 0):
        #    road_list = []
        #    __fundemental_search_word__(answer,road_list,board,idx,jdx,word_idx,word)
        
        if ((not [idx,jdx] in road_list) and (board[idx][jdx] == word[word_idx])):
            if (word_idx == len(word)-1):
                answer[0] = bool(1)
            else:
                new_road_list = list(road_list)
                new_road_list.append(list([idx,jdx]))
                __fundemental_search_word__(answer,new_road_list,board,idx,jdx,word_idx+1,word)


def __fundemental_search_word__(answer,road_list,board,idx,jdx,word_idx,word):
    if (word_idx > 0):
        if (idx > 0):
            __find_word_to_forward__(answer,road_list,board,idx-1,jdx,word_idx,word)
        if (jdx > 0):
            __find_word_to_forward__(answer,road_list,board,idx,jdx-1,word_idx,word)
    
    if (idx < len(board)-1):
        __find_word_to_forward__(answer,road_list,board,idx+1,jdx,word_idx,word)    
    if (jdx < len(board[idx])-1):
        __find_word_to_forward__(answer,road_list,board,idx,jdx+1,word_idx,word)
        
class Solution:
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        return exist(board, word)
