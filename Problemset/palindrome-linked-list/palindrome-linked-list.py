
# @Title: 回文链表 (Palindrome Linked List)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-27 11:53:47
# @Runtime: 964 ms
# @Memory: 32.7 MB

def checkPalindrome(L1, L2):
    while 1:
        if L1 and L2:
            if L1.val == L2.val:
                L1 = L1.next
                L2 = L2.next
                if L1 is None and L2 is None:
                    return True
            else:
                return False
        else:
            return False

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        if head and head.next is None:
            return True
        LL1, LL2, LL3, LL = None, None, None, head
        count = 1
        while 1:
            if LL:
                if count == 1:
                    LL1 = LL
                    LL = LL.next
                    LL1.next = None
                    L1, L2 = LL1, LL
                    if checkPalindrome(L1, L2):
                        return True
                    else:
                        if L2:
                            if checkPalindrome(L1, L2.next):
                                return True
                elif count == 2:
                    LL2 = LL
                    LL = LL.next
                    LL3 = LL
                    LL2.next = LL1
                    L1, L2 = LL2, LL
                    #print(LL, LL2)
                    if checkPalindrome(L1, L2):
                        return True
                    else:
                        if L2:
                            if checkPalindrome(L1, L2.next):
                                return True
                else:
                    LL = LL.next
                    LL1, LL2, LL3 = LL2, LL3, LL
                    LL2.next = LL1
                    L1, L2 = LL2, LL
                    #print(LL, LL2)
                    if checkPalindrome(L1, L2):
                        return True
                    else:
                        if L2:
                            if checkPalindrome(L1, L2.next):
                                return True
            else:
                return False
            count += 1

