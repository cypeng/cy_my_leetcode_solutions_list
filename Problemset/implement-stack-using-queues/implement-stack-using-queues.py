
# @Title: 用队列实现栈 (Implement Stack using Queues)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-03 11:01:06
# @Runtime: 40 ms
# @Memory: 14.9 MB

class MyStack:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.stack = [None]*9
        self.idx = -1

    def push(self, x: int) -> None:
        """
        Push element x onto stack.
        """
        self.idx += 1
        self.stack[self.idx] = x

    def pop(self) -> int:
        """
        Removes the element on top of the stack and returns that element.
        """
        if self.empty():
            return None
        temp = self.stack[self.idx]
        self.stack[self.idx] = None
        self.idx -= 1
        return temp

    def top(self) -> int:
        """
        Get the top element.
        """
        return self.stack[self.idx]


    def empty(self) -> bool:
        """
        Returns whether the stack is empty.
        """
        if self.idx < 0:
            return True
        return False

# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()
