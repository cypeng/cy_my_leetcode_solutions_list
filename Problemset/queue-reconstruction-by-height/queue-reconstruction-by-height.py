
# @Title: 根据身高重建队列 (Queue Reconstruction by Height)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-08 01:18:56
# @Runtime: 36 ms
# @Memory: 15.1 MB

class Solution:
    def reconstructQueue(self, people: List[List[int]]) -> List[List[int]]:
        people.sort(key=lambda x: (-x[0],x[1]))
        case = []
        for person in people:
            case.insert(person[1],person)
        return case

