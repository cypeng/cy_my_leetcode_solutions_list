
# @Title: 4Sum (4Sum)
# @Author: pcysl
# @Date: 2017-12-13 13:50:16
# @Runtime: 1829 ms
# @Memory: N/A

class Solution:
    def fourSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        newnums = sorted(nums)   
        answer = []
        if (len(nums) >=4):
            for idx in range(0,len(nums)-3):
                if (idx > 0) and (newnums[idx] == newnums[idx-1]):
                    continue
                for t in range(0,len(nums)-2):
                    kdx = len(nums)-1-t
                    ldx = kdx-1
                    jdx = idx+1
                    if ((kdx < len(nums)-1) and (newnums[kdx] == newnums[kdx+1])):
                        continue
                    gonum = target - (newnums[idx] +newnums[kdx])
                    while (jdx < ldx):  
                        if (newnums[jdx]+newnums[ldx]==gonum):
                            answer.append([newnums[idx],newnums[jdx],newnums[ldx],newnums[kdx]])
                            jdx = jdx+1
                            ldx = ldx-1
                            while ((jdx<ldx) and (newnums[jdx]==newnums[jdx-1])):
                                 jdx = jdx+1
                            while ((jdx<ldx) and (newnums[ldx]==newnums[ldx+1])):
                                 ldx = ldx-1
                        elif (newnums[jdx]+newnums[ldx]> gonum):
                            ldx = ldx-1
                        else:
                            jdx = jdx+1
                       
        return answer
