
# @Title: 递归乘法 (Recursive Mulitply LCCI)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-01 01:10:18
# @Runtime: 20 ms
# @Memory: 14.7 MB

class Solution:
    def multiply(self, A: int, B: int) -> int:
        if B == 1:
            return A
        if B%2 == 1:
            return self.multiply(A, B//2) + self.multiply(A, B//2) + A
        else:
            return self.multiply(A, B//2) + self.multiply(A, B//2)
