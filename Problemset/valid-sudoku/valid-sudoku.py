
# @Title: 有效的数独 (Valid Sudoku)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-27 11:16:25
# @Runtime: 56 ms
# @Memory: 15 MB

class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        hashmap88 = {0:[],1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[]}
        hashmap_idx = {0:[],1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[]}
        hashmap_jdx = {0:[],1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[]} 
        for idx in range(0, len(board)):
            for jdx in range(0, len(board[0])):
                #print(idx,jdx,(idx//3)*3+(jdx//3))
                value = board[idx][jdx]
                if not value in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
                    continue
                key = (idx//3)*3+(jdx//3)
                if value in hashmap88[key]:
                    #print(hashmap88)
                    return False
                hashmap88[key].append(value)
                if value in hashmap_idx[idx]:
                    #print(hashmap_idx)
                    return False
                hashmap_idx[idx].append(value)
                if value in hashmap_jdx[jdx]:
                    return False
                hashmap_jdx[jdx].append(value)
        return True
