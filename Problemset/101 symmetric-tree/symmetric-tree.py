
# @Title: Symmetric Tree (Symmetric Tree)
# @Author: pcysl
# @Date: 2018-02-13 21:42:25
# @Runtime: 74 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        self.flag = 1
        if not (root):
            return bool(1)
        else:
            if (root.left) and (root.right):
                self.isTwoSymmetric(root.left,root.right)
            if not (root.left) and (root.right):
                return bool(0)
            if not (root.right) and (root.left):
                return bool(0)
            if (self.flag == 1):
                return bool(1)
            else:
                return bool(0)
    
    def isTwoSymmetric(self, root1, root2):
        if (root1) and (root2):
            if (root1.val == root2.val):
                self.isTwoSymmetric(root1.left,root2.right)
                self.isTwoSymmetric(root1.right,root2.left)
            else:
                self.flag = 0
        elif not (root1) and (root2):
            self.flag = 0
        elif not (root2) and (root1):
            self.flag = 0
        
            
