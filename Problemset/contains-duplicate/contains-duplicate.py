
# @Title: 存在重复元素 (Contains Duplicate)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-26 11:29:39
# @Runtime: 52 ms
# @Memory: 19.7 MB

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        hashset = set()
        idx, jdx = 0, len(nums)-1
        while idx <= jdx:
            if (nums[idx] in hashset) or (nums[jdx] in hashset):
                return True
            if idx == jdx:
                return False
            else:
                if nums[idx] == nums[jdx]:
                    return True
                hashset.add(nums[idx])
                hashset.add(nums[jdx])
            idx+=1
            jdx-=1
        return False
