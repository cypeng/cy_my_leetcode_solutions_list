
# @Title: 最长回文子序列 (Longest Palindromic Subsequence)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-08 11:29:47
# @Runtime: 1156 ms
# @Memory: 31.1 MB

class Solution:
    def longestPalindromeSubseq(self, s: str) -> int:
        dp = [[0]*len(s) for idx in range(0, len(s))]
        for idx in range(len(s)-1, -1, -1):
            for jdx in range(idx, len(s)):
                if idx == jdx:
                    dp[idx][jdx] = 1
                else:
                    if s[idx] == s[jdx]:
                        dp[idx][jdx] = dp[idx+1][jdx-1]+2
                    else:
                        dp[idx][jdx] = max(dp[idx+1][jdx],dp[idx][jdx-1])
        return dp[0][-1]
