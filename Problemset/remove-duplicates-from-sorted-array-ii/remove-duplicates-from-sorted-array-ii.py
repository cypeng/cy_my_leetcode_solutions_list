
# @Title: 删除有序数组中的重复项 II (Remove Duplicates from Sorted Array II)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-23 14:44:12
# @Runtime: 88 ms
# @Memory: 15 MB

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        idx, jdx = 0, 0
        count, temp = None, None
        while jdx <= len(nums)-1:
            print(jdx, temp, count, nums[jdx])
            if temp is None or (not temp == nums[jdx]):
                temp = nums[jdx]
                count = 0
            elif temp == nums[jdx]:
                count += 1
            
            if count <= 1:
                nums[idx] = temp
                idx += 1
            jdx += 1

        return idx
