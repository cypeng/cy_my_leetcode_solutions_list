
# @Title: Binary Tree Level Order Traversal (Binary Tree Level Order Traversal)
# @Author: pcysl
# @Date: 2018-02-14 11:22:58
# @Runtime: 80 ms
# @Memory: N/A

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        
        self.level = []
        self.data  = []
        self.leveldata = []
        if (root):
            self.level.append(root)
            while (len(self.level)):
                thislevel = []
                thislevel = self.level
                self.leveldata = []
                self.level = []
                for idx in range(0,len(thislevel)):
                    root = thislevel[idx]          
                    self.levelarray(root)   
                self.data.append(self.leveldata)
        
        return self.data
        
    def levelarray(self, root):
        self.leveldata.append(root.val)
        if (root.left):
            self.level.append(root.left)
        if (root.right):
            self.level.append(root.right)
    
