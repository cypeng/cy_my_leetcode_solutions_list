
# @Title: 相交链表 (Intersection of Two Linked Lists)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-26 11:06:16
# @Runtime: 188 ms
# @Memory: 29.5 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        #StoreSpace = [None]*50000
        LLA, LLB = headA, headB
        
        while LLA != LLB:
            LLA = LLA.next
            LLB = LLB.next
            if LLA is None and LLB is None:
                return None
            if LLA is None:
                LLA = headB
            if LLB is None:
                LLB = headA

        return LLA
