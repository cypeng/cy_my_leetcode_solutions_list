
# @Title: Longest Valid Parentheses (Longest Valid Parentheses)
# @Author: pcysl
# @Date: 2018-07-19 00:05:36
# @Runtime: 68 ms
# @Memory: N/A

class Solution:
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        return CreateParenthesesPos(s)
        
def CreateParenthesesPos(s):
    temp = [-2]*len(s)
    AnsTemp = [0]*len(s)
    PDict= ['(',')']
    jdx  = 0
    Pos  = 0
    Count= 0
    Ans  = 0
    for idx in range(0, len(s)):
        #print(jdx, temp, AnsTemp)
        if ((temp[jdx-1] >= 0) and (s[idx] == PDict[1])):
            AnsTemp[temp[jdx-1]] = 1
            temp[jdx-1] = -2
            jdx-=1
        elif ((temp[jdx-1] == -2) and (s[idx] == PDict[1])):
            AnsTemp[Pos] = -1
            Pos+= 1
        
        if (s[idx] == PDict[0]):
            temp[jdx] = Pos
            Pos+= 1
            jdx+= 1
    
    for idx in range(0, len(AnsTemp)):
        if (AnsTemp[idx] == 1):
            Count += 1
        else:
            Count = 0
        if (AnsTemp[idx] == -1):
            Count = 0
        if (Ans < Count):
            Ans = Count
    
    return Ans*2
    
        
        
