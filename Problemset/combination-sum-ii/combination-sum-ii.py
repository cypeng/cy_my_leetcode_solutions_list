
# @Title: 组合总和 II (Combination Sum II)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-03 21:31:52
# @Runtime: 136 ms
# @Memory: 14.8 MB

def backtrack(idx,candidates,case,case_sum,target,path_set,ans):
    #print(idx, candidates, case, case_sum)
    if case_sum == target:
        if not case in ans:
            ans.append(case)
    if (idx > len(candidates)-1):
        return True
    
    item = candidates[idx]
    if case_sum + item <= target:
        this_case = case.copy()
        this_case.append(item)
        if not this_case in path_set:
            path_set.append(this_case)
            backtrack(idx+1,candidates,this_case,case_sum+item,target,path_set,ans)
    backtrack(idx+1,candidates,case,case_sum,target,path_set,ans)
            
class Solution:
    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        Flag = [True]
        ans = []
        candidates.sort()
        path_set = []
        backtrack(0,candidates,[],0,target,path_set,ans)
        return ans
