
# @Title: 找到 K 个最接近的元素 (Find K Closest Elements)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-17 14:51:45
# @Runtime: 72 ms
# @Memory: 16 MB

def find_x(arr, idx, jdx, x):
    while 1:
        mid = int((idx + jdx)*0.5)
        if (mid == idx) or (mid == jdx):
            return mid
        if arr[mid] == x:
            return mid
        if x < arr[mid]:
            jdx = mid
        else:
            idx = mid

class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        idx, jdx = 0, len(arr)-1
        if len(arr) <= k:
            return arr
        elif x >= arr[jdx]:
            return arr[jdx-k+1:jdx+1]
        elif x <= arr[idx]:
            return arr[0:k]
        else:
            x_idx = find_x(arr, idx, jdx, x)
            #print(x_idx)
            idx, jdx = x_idx-k, x_idx+k
            if idx < 0:
                idx = 0
            if jdx > len(arr)-1:
                jdx = len(arr)-1
            #print(idx,jdx)
            while not (jdx - idx +1) == k:
                if abs(arr[jdx] - x) >= abs(arr[idx] - x):
                    jdx -= 1
                else:
                    idx += 1
            #print(idx,jdx)
            return arr[idx:jdx+1]
                
