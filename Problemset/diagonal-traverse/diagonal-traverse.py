
# @Title: 对角线遍历 (Diagonal Traverse)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-22 23:35:59
# @Runtime: 88 ms
# @Memory: 16.6 MB

class Solution:
    def findDiagonalOrder(self, mat: List[List[int]]) -> List[int]:
        size = [len(mat), len(mat[0])]
        idx, jdx = 0, 0
        ans = []
        count = 0
        while (idx >= 0) and (jdx >= 0) and (idx <= size[0]-1) and (jdx <= size[1]-1):
            kdx, ldx = idx, jdx
            diag = []
            while (kdx >= 0) and (ldx >= 0) and (kdx <= size[0]-1) and (ldx <= size[1]-1):
                diag.append(mat[kdx][ldx])
                ldx-=1
                kdx+=1
            if count%2 == 0:
                diag = diag[::-1]
            ans.extend(diag)
            if (jdx < size[1]-1):
                jdx+=1
            else:
                idx+=1
            count += 1
        print(ans)
        return ans
