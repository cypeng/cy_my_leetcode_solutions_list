
# @Title: 寻找重复的子树 (Find Duplicate Subtrees)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-27 22:06:46
# @Runtime: 84 ms
# @Memory: 23.8 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

class Solution:
    def findDuplicateSubtrees(self, root: TreeNode) -> List[TreeNode]:
        hashmap = {}
        ans = []
        def collect(root, hashmap, ans):
            if not root:
                return "#"
            serial = "{},{},{}".format(root.val, collect(root.left,hashmap,ans), collect(root.right,hashmap,ans))
            if not serial in hashmap:
                hashmap[serial] = 1
            else:
                hashmap[serial] += 1
                if hashmap[serial] == 2:
                    ans.append(root)
            return serial
        collect(root, hashmap, ans)
        return ans
