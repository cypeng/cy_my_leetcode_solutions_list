
# @Title: 删除链表的倒数第 N 个结点 (Remove Nth Node From End of List)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-08 21:43:53
# @Runtime: 44 ms
# @Memory: 14.9 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
def ListNodeCount(l):
    count = 1
    while l.next:
        l = l.next
        count +=1
    return count

def ListNodeRemoveN(l, n):
    if n == 0:
        answer = l.next
        return answer
    count = 0
    answer = l
    tail, head = None, None
    while l:
        if count == n-1:
            tail = l
        if count == n+1:
            head = l
        l = l.next
        count +=1
    tail.next = head
    return answer

class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        count = ListNodeCount(head)
        n = count - n
        return ListNodeRemoveN(head, n)
