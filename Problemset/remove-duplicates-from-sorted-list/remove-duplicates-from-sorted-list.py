
# @Title: 删除排序链表中的重复元素 (Remove Duplicates from Sorted List)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 14:18:52
# @Runtime: 52 ms
# @Memory: 15 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def getUniqueHead(l):
    head, lpre = l, l
    while l:
        if lpre.val != l.val:
            break
        lpre = l
        l = l.next
    if l is None:
        return head, None
    return head, l

def removeDup(l):
    iniFlag = True
    answerHead = None
    while l:
        head, l = getUniqueHead(l)
        if iniFlag:
            answerHead = head
            head.next = None
            answer = answerHead
            iniFlag = False
        else:
            head.next = None
            answer.next = head
            answer = answer.next
    return answerHead

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if head is None:
            return head
        if head.next is None:
            return head
        return removeDup(head)
