
# @Title: 杨辉三角 (Pascal's Triangle)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-13 01:34:04
# @Runtime: 40 ms
# @Memory: 15 MB

def pascal_ith(ith, Pith_minus, numRows, ans):
    if ith == 0:
        Pith = [1]
    elif ith == 1:
        Pith = [1, 1]
    else:
        Pith = [1]*(ith+1)
        for i in range(1, len(Pith)-1):
            Pith[i] = Pith_minus[i-1]+Pith_minus[i]
    ans.append(Pith)
    if ith+1 < numRows:
        return pascal_ith(ith+1, Pith, numRows, ans)
    return Pith
   
class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        ans = []
        pascal_ith(0, None, numRows, ans)
        return ans
