
# @Title: 01 矩阵 (01 Matrix)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-06 11:06:37
# @Runtime: 196 ms
# @Memory: 16.1 MB

def traversal(mat,idx,jdx,count,opt_count):
    
    if mat[idx][jdx] == 0 and (opt_count[0] > count or count == 1):
        opt_count[0] = count
    else:
        if count < opt_count[0] and not mat[idx][jdx] == 0:
            #seens.append((idx, jdx))
            #print(idx,jdx,count,opt_count,seens,seens_count)
            #seens_count[(idx,jdx)] = count
            if idx +1 >= 0 and idx +1 <= len(mat) -1:
                traversal(mat,idx+1,jdx,count+1,opt_count)
            if idx -1 >= 0 and idx -1 <= len(mat)-1:
                traversal(mat,idx-1,jdx,count+1,opt_count)
            if jdx -1 >= 0 and jdx -1 <= len(mat[0])-1:
                traversal(mat,idx,jdx-1,count+1,opt_count)
            if jdx +1 >= 0 and jdx +1 <= len(mat[0])-1:
                traversal(mat,idx,jdx+1,count+1,opt_count)
            

class Solution:
    def updateMatrix(self, mat: List[List[int]]) -> List[List[int]]:
        size1, size2 = len(mat), len(mat[0])
        for idx in range(0, size1):
            for jdx in range(0, size2):
                if not mat[idx][jdx] == 0:
                    if idx == 0 and jdx == 0:
                        mat[idx][jdx] = 10000
                    elif idx-1 >= 0 and jdx-1>= 0 and idx-1 <= size1-1 and jdx-1 <= size2-1:
                        mat[idx][jdx] = min(mat[idx-1][jdx], mat[idx][jdx-1])+1
                    elif idx-1 >= 0 and jdx== 0 and idx-1 <= size1-1:
                        mat[idx][jdx] = mat[idx-1][jdx]+1
                    elif idx==0 and jdx-1>=0 and jdx-1 <= size2-1:
                        mat[idx][jdx] = mat[idx][jdx-1]+1
                    #opt_count = [10000]
                    #traversal(mat,idx,jdx,0,opt_count)
        for idx in range(0, size1):
            for jdx in range(0, size2):
                kdx, ldx = size1-1-idx, size2-1-jdx
                #print(kdx, ldx, mat[kdx][ldx])
                if mat[kdx][ldx] > 0:
                    ori_val = mat[kdx][ldx]
                    if kdx == 0 and ldx == 0 and kdx+1 <= size1-1 and ldx+1 <= size2-1:
                        mat[kdx][ldx] = min(mat[kdx+1][ldx], mat[kdx][ldx+1])+1
                    elif kdx+1 <= size1-1 and ldx+1 <= size2-1 and kdx+1 >= 0 and ldx+1>=0:
                        mat[kdx][ldx] = min(min(mat[kdx+1][ldx], mat[kdx][ldx+1])+1, ori_val)
                    elif kdx+1 <= size1-1 and ldx == size2-1 and kdx+1 >=0:
                        mat[kdx][ldx] = min(mat[kdx+1][ldx]+1, ori_val)
                    elif kdx == size1-1 and ldx+1 <= size2-1 and ldx+1 >= 0:
                        mat[kdx][ldx] = min(mat[kdx][ldx+1]+1, ori_val)
                #print(kdx, ldx, mat[kdx][ldx])
        #print(mat)       
        return mat
