
# @Title: 在排序数组中查找元素的第一个和最后一个位置 (Find First and Last Position of Element in Sorted Array)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 18:29:38
# @Runtime: 48 ms
# @Memory: 15.5 MB

class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        idx, jdx = 0, len(nums)-1
        flag1, flag2 = False, False
        while not flag1 or not flag2:
            if idx <= len(nums)-1:
                if not flag1 and nums[idx] == target:
                    flag1 = True
                elif not flag1:
                    idx+= 1
            else:
                idx = -1
                flag1 = True

            if jdx >= 0:
                if not flag2 and nums[jdx] == target:
                    flag2 = True
                elif not flag2:
                    jdx -= 1
            else:
                jdx = -1
                flag2 = True
            
            if flag1 and flag2:
                return [idx, jdx]
        return [-1, -1]
