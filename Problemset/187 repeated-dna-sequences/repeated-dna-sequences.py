
# @Title: Repeated DNA Sequences (Repeated DNA Sequences)
# @Author: pcysl
# @Date: 2018-01-05 13:56:38
# @Runtime: 102 ms
# @Memory: N/A

class Solution(object):
    def findRepeatedDnaSequences(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        idx = 0
        substr = s[idx:idx+10]
        Hash = {}
        ans  = []
        while (substr):        
            Key = substr
            if (not Key in Hash):
                Hash[Key] = 1
            else:
                Hash[Key] = Hash[Key] +1
                if (Hash[Key] == 2):
                    ans.append(substr)
            idx = idx+1
            substr = s[idx:idx+10]
    
        return ans 

    def KeyFunc(self,s):
        sl = ''
        for idx in range(0,len(s)):
            sl = sl + str(ord(s[idx]))
        return sl
