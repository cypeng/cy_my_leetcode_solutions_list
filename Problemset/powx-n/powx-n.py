
# @Title: Pow(x, n) (Pow(x, n))
# @Author: pcysl@hotmail.com
# @Date: 2021-06-14 14:41:41
# @Runtime: 44 ms
# @Memory: 15 MB

def calPow(x,nth,cache):
    #print(x,nth, cache)
    if nth in cache:
        return cache[nth]
    if len([*cache]) > 0:
        jdx = len([*cache])-1
        while jdx >= 0:
            this_max_n = [*cache][jdx]
            this_pow = this_max_n+this_max_n
            if nth - this_pow >= 0:
                break
            jdx -= 1
    #print('cache', this_pow, this_max_n)
    cache[this_pow] = cache[this_max_n]*cache[this_max_n]
    return calPow(x,nth-this_pow,cache)*cache[this_pow]

class Solution:
    def myPow(self, x: float, n: int) -> float:
        cache = {}
        cache[0], cache[1] = 1, x
        if n >= 0:
            return calPow(x,n,cache)
        else:
            ans = calPow(x,abs(n),cache)
            return 1/ans
