
# @Title: 排序数组 (Sort an Array)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-18 00:24:58
# @Runtime: 400 ms
# @Memory: 20.3 MB


def merge(nums, start, mid, end):
    temp = []
    idx, jdx = start, mid+1
    while (idx <= mid) or (jdx <= end):
        if (idx > mid) or ((jdx <= end) and (nums[jdx] < nums[idx])):
            temp.append(nums[jdx])
            jdx += 1
        else:
            temp.append(nums[idx])
            idx += 1
    nums[start:end+1] = temp

def mergeSort(nums, start, end):
    if start >= end:
        return None
    middle = (start+end)//2
    mergeSort(nums, start, middle)
    mergeSort(nums, middle+1, end)
    merge(nums, start, middle, end)

class Solution:
    def sortArray(self, nums: List[int]) -> List[int]:
        mergeSort(nums, 0, len(nums)-1)
        return nums
