
# @Title: Length of Last Word (Length of Last Word)
# @Author: pcysl
# @Date: 2018-07-30 13:06:59
# @Runtime: 56 ms
# @Memory: N/A

class Solution:
    def lengthOfLastWord(self, s):
        """
        :type s: str
        :rtype: int
        """
        jdx = len(s)-1
        sumAns = 0
        iniFlag = 1
        for idx in range(0,len(s)):
            if  ((s[jdx] == ' ') and (iniFlag == 0)):
                return sumAns
            if (s[jdx] != ' '):
                sumAns += 1
                iniFlag = 0

            jdx -= 1
        return sumAns
            
        
        
