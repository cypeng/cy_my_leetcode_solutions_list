
# @Title: 使用最小花费爬楼梯 (Min Cost Climbing Stairs)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-09 00:02:16
# @Runtime: 36 ms
# @Memory: 15 MB

class Solution:
    def minCostClimbingStairs(self, cost: List[int]) -> int:
        best_cost = [0]*2
        for idx in range(0, len(cost)+1):
            if idx == 0:
                best_cost[0] = cost[0]
            elif idx == 1:
                best_cost[1] = cost[1]
            else:
                if idx == len(cost):
                    best_cost[idx%2] = min(best_cost[0], best_cost[1])
                else:
                    best_cost[idx%2] = min(best_cost[0], best_cost[1])+cost[idx]
        if len(cost) <= 2:
            return min(best_cost)
        return best_cost[idx%2]
