
# @Title: 分割等和子集 (Partition Equal Subset Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-10 23:31:15
# @Runtime: 692 ms
# @Memory: 15 MB

class Solution:
    def canPartition(self, nums: List[int]) -> bool:
        allnum = sum(nums)
        if allnum%2 == 1:
            return False
        tar_num = allnum//2
        dp = [False]*(tar_num+1)
        dp[0] = True
        for idx in range(0,len(nums)):
            for jdx in range(tar_num,nums[idx]-1,-1):
                dp[jdx] = dp[jdx] or dp[jdx-nums[idx]]
        #print(dp)
        return dp[-1]
