
# @Title: Two Sum (Two Sum)
# @Author: pcysl
# @Date: 2017-11-01 11:40:28
# @Runtime: 5116 ms
# @Memory: N/A

class Solution:
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        for num1index in range(0, len(nums)):
            num2 = target - nums[num1index]
            for num2index in range(num1index+1, len(nums)):
                if (num2 == nums[num2index]):
                    return [num1index, num2index]
                    break
        
