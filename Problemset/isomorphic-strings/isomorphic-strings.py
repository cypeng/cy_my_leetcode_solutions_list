
# @Title: 同构字符串 (Isomorphic Strings)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-26 15:19:08
# @Runtime: 80 ms
# @Memory: 15.4 MB

def str2count(s):
    hashmap = {}
    count = 0
    s_num = ''
    for item in s:
        #item = ord(item)
        if item in hashmap:
            s_num += str(hashmap[item])
        else:
            hashmap[item] = count
            s_num += str(count)
            count += 1
    return s_num

class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        s_num = str2count(s)
        t_num = str2count(t)
        #print(s_num, t_num)
        if s_num == t_num:
            return True
        return False

