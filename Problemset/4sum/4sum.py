
# @Title: 四数之和 (4Sum)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-13 01:18:28
# @Runtime: 1520 ms
# @Memory: 14.9 MB

class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        answer = []
        if len(nums) < 4:
            return answer
        nums = sorted(nums)
        idx, jdx = 0, len(nums)-1
        while idx <= len(nums)-4:
            a = nums[idx]
            jdx = len(nums)-1
            while idx < jdx:
                b = nums[jdx]
                kdx, ldx = idx+1, jdx-1
                while kdx < ldx:
                    c, d = nums[kdx], nums[ldx]
                    if a+b+c+d == target:
                        if [a,c,d,b] not in answer:
                            answer.append([a,c,d,b])
                        kdx += 1
                        while kdx < ldx and nums[kdx] == nums[kdx-1]:
                            kdx += 1
                        ldx -= 1
                        while kdx < ldx and nums[ldx] == nums[ldx+1]:
                            ldx -= 1
                    elif a+b+c+d > target:
                        ldx -= 1
                        while kdx < ldx and nums[ldx] == nums[ldx+1]:
                            ldx -= 1
                    else:
                        kdx += 1
                        while kdx < ldx and nums[kdx] == nums[kdx-1]:
                            kdx += 1
            
                jdx -= 1
                while idx < jdx and nums[jdx] == nums[jdx+1]:
                    jdx -= 1
            idx += 1
            while idx < jdx and nums[idx] == nums[idx-1]:
                idx += 1
        return answer
