
# @Title: Two Sum II - Input array is sorted (Two Sum II - Input array is sorted)
# @Author: pcysl
# @Date: 2017-12-27 11:45:13
# @Runtime: 149 ms
# @Memory: N/A

class Solution:
    def twoSum(self, numbers, target):
        """
        :type numbers: List[int]
        :type target: int
        :rtype: List[int]
        """
        for idx in range(0, len(numbers)):
            tar = target - numbers[idx]
            kdx = idx+1
            jdx = len(numbers)-1
            while (kdx <= jdx):
                midIdx = int((kdx+jdx)*0.5)
                if (tar == numbers[midIdx]):
                    return [idx+1,midIdx+1]
                elif (tar > numbers[midIdx]):
                    kdx = midIdx+1
                elif (tar < numbers[midIdx]):
                    jdx = midIdx-1
