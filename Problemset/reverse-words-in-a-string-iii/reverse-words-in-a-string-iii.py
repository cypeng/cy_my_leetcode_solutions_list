
# @Title: 反转字符串中的单词 III (Reverse Words in a String III)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-22 14:46:49
# @Runtime: 96 ms
# @Memory: 15.3 MB

class Solution:
    def reverseWords(self, s: str) -> str:
        str_list = s.split()
        #print(str_list)
        ans = ''
        for idx, item in enumerate(str_list): 
            if idx > 0:
                ans = ans + ' '
            for jdx in range(0,len(item)):
                ans = ans +item[len(item)-1-jdx]
        return ans
