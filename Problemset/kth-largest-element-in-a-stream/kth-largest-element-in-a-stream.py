
# @Title: 数据流中的第 K 大元素 (Kth Largest Element in a Stream)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-12 19:01:30
# @Runtime: 212 ms
# @Memory: 18.6 MB

class Heap:
    # min heap
    def __init__(self):
        self.nums = []

    @property
    def size(self):
        return len(self.nums)
    
    def top(self):
        #print(self.nums)
        return self.nums[0]

    def push(self, val):
        self.nums.append(val)
        self.shift_up(self.size-1)
        #print('push', self.nums)

    def shift_up(self, idx):
        while idx > 0: # not idx >= 0
            #print(idx, self.nums, self.nums[idx])
            parent_idx = (idx-1)//2
            if self.nums[idx] > self.nums[parent_idx]:
                break
            self.nums[idx], self.nums[parent_idx] = self.nums[parent_idx], self.nums[idx]
            idx = parent_idx
    
    def shift_down(self, idx):
        while 2*idx+1 < self.size:
            parent_idx = idx 
            left_idx = 2*idx+1
            right_idx = 2*idx+2
            
            if self.nums[parent_idx] > self.nums[left_idx]:
                parent_idx = left_idx
                
            if right_idx < self.size and self.nums[parent_idx] > self.nums[right_idx]:
                parent_idx = right_idx
            
            if parent_idx == idx:
                break
            self.nums[parent_idx], self.nums[idx] = self.nums[idx], self.nums[parent_idx]
            idx = parent_idx

    def pop(self):
        #print('pop',self.nums)
        temp = self.nums[0]
        self.nums[0], self.nums[-1] = self.nums[-1], self.nums[0]
        self.nums.pop()
        self.shift_down(0)
        #print('pope',self.nums)
        return temp

class KthLargest:
    def __init__(self, k: int, nums: List[int]):
        self.heap = Heap()
        self.k = k
        for num in nums:
            self.heap.push(num)
            if self.heap.size > k:
                self.heap.pop()

        #self.heap = myHeap(nums, k)
        
    def add(self, val: int) -> int:
        self.heap.push(val)
        if self.heap.size > self.k:
            self.heap.pop()
        return self.heap.top()

# Your KthLargest object will be instantiated and called as such:
# obj = KthLargest(k, nums)
# param_1 = obj.add(val)
