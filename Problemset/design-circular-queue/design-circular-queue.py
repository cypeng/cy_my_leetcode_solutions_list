
# @Title: 设计循环队列 (Design Circular Queue)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-02 14:17:07
# @Runtime: 256 ms
# @Memory: 15.6 MB

class MyCircularQueue:
    def __init__(self, k: int):
        self.queue = [None]*k
        self.k = len(self.queue)
        self.start_idx = self.k-1
        self.end_idx = self.k-1
        self.empty_idx = self.k-1  

    def enQueue(self, value: int) -> bool:
        if self.isFull():
            return False
        self.queue[self.empty_idx] = value
        self.empty_idx -= 1
        self.end_idx = self.empty_idx +1
        #print('en', self.start_idx, self.end_idx, self.queue)
        return True

    def deQueue(self) -> bool:
        if self.isEmpty():
            return False
        idx = self.k-1
        while idx >= 0:
            self.queue[idx] = self.queue[idx-1]
            idx -= 1
        self.queue[0] = None
        self.empty_idx += 1
        if self.empty_idx < self.k - 1:
            self.end_idx = self.empty_idx +1
        else:
            self.end_idx = self.empty_idx
        #print('de', self.start_idx, self.end_idx, self.queue)
        return True

    def Front(self) -> int:
        if self.queue[self.start_idx] is None:
            return -1
        return self.queue[self.start_idx]

    def Rear(self) -> int:
        if self.queue[self.end_idx] is None:
            return -1
        return self.queue[self.end_idx]

    def isEmpty(self) -> bool:
        if self.empty_idx == self.start_idx:
            return True
        return False

    def isFull(self) -> bool:
        if self.empty_idx < 0:
            return True        
        return False



# Your MyCircularQueue object will be instantiated and called as such:
# obj = MyCircularQueue(k)
# param_1 = obj.enQueue(value)
# param_2 = obj.deQueue()
# param_3 = obj.Front()
# param_4 = obj.Rear()
# param_5 = obj.isEmpty()
# param_6 = obj.isFull()
