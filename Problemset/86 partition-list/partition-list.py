
# @Title: Partition List (Partition List)
# @Author: pcysl
# @Date: 2019-02-22 10:27:40
# @Runtime: 40 ms
# @Memory: 12.5 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

def main(head, x):
    if (head == None):
        return head
    iniFlag1 = 1
    iniFlag2 = 1
    count = 0
    ll_1, tail_head = None, None
    while (head != None):
        count += 1

        if ((iniFlag1 == 0) and (head.val < x)):
            l_1 = ListNode(head.val)
            ll_1.next = l_1
            ll_1 = ll_1.next
        
        if ((iniFlag1) and (head.val < x)):
            ll_1 = ListNode(head.val)
            answer = ll_1
            iniFlag1 = 0

        if ((iniFlag2 == 0) and (head.val >= x)):
            l_2 = ListNode(head.val)
            ll_2.next = l_2
            ll_2 = ll_2.next

        if ((iniFlag2) and (head.val >= x)):
            ll_2 = ListNode(head.val)
            tail_head = ll_2
            iniFlag2 = 0
        
        head = head.next
    if (ll_1 != None) and (tail_head != None):
        ll_1.next = tail_head
    if (ll_1 == None) and (tail_head != None):
        answer = tail_head
    if (ll_1 != None) and (tail_head == None):
        ll_1.next = tail_head
    return answer

class Solution:
    def partition(self, head: 'ListNode', x: 'int') -> 'ListNode':
        return main(head, x)
        
