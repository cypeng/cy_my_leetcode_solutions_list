
# @Title: 最长公共子序列 (Longest Common Subsequence)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-30 01:21:02
# @Runtime: 304 ms
# @Memory: 23.3 MB

class Solution:
    def longestCommonSubsequence(self, text1: str, text2: str) -> int:
        dp = [[0 for idx in range(0, len(text2)+1)] for jdx in range(0, len(text1)+1)]
        for idx in range(1, len(text1)+1):
            for jdx in range(1, len(text2)+1):
                if text1[idx-1] == text2[jdx-1]:
                    dp[idx][jdx] = dp[idx-1][jdx-1]+1
                else:
                    dp[idx][jdx] = max(dp[idx][jdx-1], dp[idx-1][jdx])
        #print(dp)
        return dp[-1][-1]
