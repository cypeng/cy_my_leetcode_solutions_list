
# @Title: 转换成小写字母 (To Lower Case)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-08 10:41:08
# @Runtime: 24 ms
# @Memory: 15.1 MB

class Solution:
    def toLowerCase(self, s: str) -> str:
        uppercase2lowercase = {'A': 'a', 'B': 'b', 'C': 'c', 'D': 'd', 'E': 'e',
                               'F': 'f', 'G': 'g', 'H': 'h', 'I': 'i', 'J': 'j',
                               'K': 'k', 'L': 'l', 'M': 'm', 'N': 'n', 'O': 'o',
                               'P': 'p', 'Q': 'q', 'R': 'r', 'S': 's', 'T': 't',
                               'U': 'u', 'V': 'v', 'W': 'w', 'X': 'x', 'Y': 'y',
                               'Z': 'z'}
        new_str = ''
        for idx, char in enumerate(s):
            if s[idx] in uppercase2lowercase:
                new_str += uppercase2lowercase[s[idx]]
            else:
                new_str += s[idx]
        return new_str
