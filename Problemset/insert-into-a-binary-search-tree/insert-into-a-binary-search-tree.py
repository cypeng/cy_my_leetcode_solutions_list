
# @Title: 二叉搜索树中的插入操作 (Insert into a Binary Search Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-07 10:44:05
# @Runtime: 136 ms
# @Memory: 17.3 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

def traversal(root, val, flag):
    if root:
        if flag:
            if root.left and val < root.val: 
                flag = traversal(root.left, val, flag)
            else:
                if val < root.val:
                    insert_root = TreeNode(val)
                    root.left = insert_root
                    flag = False
        
        if flag:
            if root.right and val > root.val:
                flag = traversal(root.right, val, flag)
            else:
                if val > root.val:
                    insert_root = TreeNode(val)
                    root.right = insert_root
                    flag = False

class Solution:
    def insertIntoBST(self, root: TreeNode, val: int) -> TreeNode:
        if root is None:
            return TreeNode(val)
        traversal(root, val, True)
        return root

