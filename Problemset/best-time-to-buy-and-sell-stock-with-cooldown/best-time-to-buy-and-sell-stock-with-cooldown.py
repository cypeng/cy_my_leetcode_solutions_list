
# @Title: 最佳买卖股票时机含冷冻期 (Best Time to Buy and Sell Stock with Cooldown)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-26 23:09:37
# @Runtime: 36 ms
# @Memory: 15.1 MB

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        buy, sell_pre, sell = -10000, 0, 0
        for price in prices:
            buy = max(buy, sell_pre - price)
            sell_pre, sell = sell, max(sell, buy+price)
        return sell
