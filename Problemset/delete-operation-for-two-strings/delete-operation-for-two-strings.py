
# @Title: 两个字符串的删除操作 (Delete Operation for Two Strings)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-04 00:09:42
# @Runtime: 208 ms
# @Memory: 16.7 MB

class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        #    s  e  a
        # e  1  1   
        # a        2
        # t        3
        #
        #    l e e t c o d e
        # e  1 1 
        # t      2 2 
        # c          2 
        # o            2 3 4
        #
        #    l m n l l p o t e a b c d f l
        # c  0 0 0 0 0 0 0 0 0 0 0 1 1 1 1  
        # d  0 0 0 0 0 0 0 0 0 0 0 1 2 2 2
        # f  0 0 0 0 0 0 0 0 0 0 0 1 2 3 3
        # m  0 1 1 1 1 1 1 1 1 1 1 1 2 3 3
        # n  0 1 2 2 2 2 2 2 2 2 2 2 2 3 3
        # l  1 1 2 3 3 3 3 3 3 3 3 3 3 3 3
        # t  1 1 2 3 3 3 3 4 4 4 4 4 4 4 4
        # e  1 1 2 3 3 3 3 4 5 5 5 5 5 5 5
        # b  1 1 2 3 3 3 3 4 5 5 6 6 6 6 6
        # d  1 1 2 3 3 3 3 4 5 5 6 6 7 7 7
        # l  1 1 2 3 3 3 3 4 5 5 6 6 7 7 8
        # l  1 1 2 3 3 3 3 4 5 5 6 6 7 7 8
        dp = [[0 for jdx in range(0, len(word2)+1)] for idx in range(0, len(word1)+1)]
        for idx in range(1, len(word1)+1):
            for jdx in range(1, len(word2)+1):
                if word1[idx-1] == word2[jdx-1]:
                    dp[idx][jdx] = dp[idx-1][jdx-1] + 1
                else:
                    dp[idx][jdx] = max(dp[idx][jdx-1], dp[idx-1][jdx])
        #print(dp)
        return (len(word1) - dp[-1][-1]) + (len(word2) - dp[-1][-1]) 
