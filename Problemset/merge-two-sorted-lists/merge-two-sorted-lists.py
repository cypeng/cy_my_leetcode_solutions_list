
# @Title: 合并两个有序链表 (Merge Two Sorted Lists)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-09 11:50:00
# @Runtime: 44 ms
# @Memory: 14.8 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def findMainListNode(mainList, subList, answerList):
    count = 0

    while mainList is not None:
        if subList is None:
            break
        if mainList.val > subList.val:
            answerList.next = None
            answerList.next = subList
            answerList = answerList.next
            break
        else:
            if count > 0:
                answerList = answerList.next
        mainList = mainList.next
        count += 1
    if mainList is None:
        answerList.next = subList
    return mainList, subList, answerList

def mergeListsMain(l1, l2):
    if l1.val > l2.val:
        answerHead = l2
        answer = l2
        mainlist = l2
        sublist = l1
    else:
        answerHead = l1
        answer = l1
        mainlist = l1
        sublist = l2
    
    sublist, mainlist, answer = findMainListNode(mainlist, sublist, answer)
    while mainlist:
        sublist, mainlist, answer = findMainListNode(mainlist, sublist, answer)
        if sublist is None:
            break
    return answerHead
    
class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        if l1 is None and l2 is None:
            return l1
        elif l1 is None:
            return l2
        elif l2 is None:
            return l1
        return mergeListsMain(l1, l2)
        

            
