
# @Title: Spiral Matrix II (Spiral Matrix II)
# @Author: pcysl
# @Date: 2018-11-14 15:52:51
# @Runtime: 36 ms
# @Memory: N/A

def generateMatrix(n):
    matrix = []
    
    if (n > 0):
        if (n == 1):
            matrix.append([1])
        else:
            for idx in range(0, n):
                matrix.append([0]*n)
            generate_spiral_matrix(matrix)
    return matrix

def generate_spiral_matrix(matrix):
    idx, jdx = 0, 0
    done = 0
    kdx = 1
    case = 1
    right_jdx = len(matrix[0])-1
    left_jdx  = 0
    top_idx = 1
    down_idx = len(matrix)-1
    count = 0
    while (not done):
        matrix[idx][jdx] = kdx
        kdx += 1
            
        if (case == 1):
            jdx += 1
        elif (case == 2):
            idx += 1
        elif (case == 3):
            jdx -= 1
        elif (case == 4):
            idx -= 1
            
        if (0 != matrix[idx][jdx]):
            break
        
        if (case == 1) and (jdx == right_jdx):
            case = 2
            right_jdx -= 1
            count += 1
        elif (case == 2) and (idx == down_idx):
            case = 3
            down_idx -= 1
            count += 1
        elif (case == 3) and (jdx == left_jdx):
            case = 4
            left_jdx += 1
            count += 1
        elif (case == 4) and (idx == top_idx):
            case = 1
            top_idx += 1
            count += 1
            
class Solution:
    def generateMatrix(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        return generateMatrix(n)
