
# @Title: 复制带随机指针的链表 (Copy List with Random Pointer)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 00:00:36
# @Runtime: 36 ms
# @Memory: 15.5 MB

"""
# Definition for a Node.
class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = int(x)
        self.next = next
        self.random = random
"""

class Solution:
    def copyRandomList(self, head: 'Node') -> 'Node':
        if not head:
            return None
        if not head.next:
            if head.random:
                obj = Node(head.val,None,None)
                obj.random = obj
                return obj
            return Node(head.val,None,None)
        LL = head
        while LL:
            pre = LL
            temp = Node(LL.val,LL.next,None)
            LL = LL.next
            pre.next = temp

        LL = head
        while LL:
            if LL:
                if LL.random:
                    LL.next.random = LL.random.next
                    LL = LL.next.next
                else:
                    LL = LL.next

        head = head.next
        LL = head
        while LL:
            temp = LL
            try:
                LL = LL.next.next
                temp.next = LL
            except:
                break

        #print(head.val, head.next.val, head.next.next.val,head.next.next.next.val,head.next.next.next.next.val,head.next.next.next.next.next.val)
        #print(head, head.next, head.next.next,head.next.next.next,head.next.next.next.next,head.next.next.next.next.next)
        return head
