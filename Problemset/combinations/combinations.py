
# @Title: 组合 (Combinations)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-02 21:39:39
# @Runtime: 516 ms
# @Memory: 16.5 MB

def generateCombinations(start_idx,idx,n,k,this_list,ans):
    if idx == k:
        ans.append(this_list)
    else:
        for ith in range(start_idx,n+1):
            #print(ith)
            next_list = this_list.copy()
            if not ith in this_list:
                next_list.append(ith)
                #print(next_list,start_idx)
                generateCombinations(ith+1,idx+1,n,k,next_list,ans) 

class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:
        ans = []
        generateCombinations(1,0,n,k,[],ans)
        return ans
