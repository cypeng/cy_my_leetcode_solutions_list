
# @Title: 递增子序列 (Increasing Subsequences)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-05 14:29:28
# @Runtime: 5604 ms
# @Memory: 20.9 MB

def backtrace(idx,nums,case,hashset,ans):
    if (idx > len(nums)-1):
        if (not case in ans) and (len(case) >= 2):
            ans.append(case[:])
        return True
    
    backtrace(idx+1,nums,case,hashset,ans)
    
    if (len(case) == 0) or ((len(case) > 0) and (nums[idx] >= case[-1])):
        case.append(nums[idx])
        #print(case, hashset)
        if (not case in hashset):
            hashset.append(case[:])
            backtrace(idx+1,nums,case,hashset,ans)
            hashset.pop()
        case.pop()

class Solution:
    def findSubsequences(self, nums: List[int]) -> List[List[int]]:
        ans = []
        hashset = []
        backtrace(0,nums,[],hashset,ans)
        return ans
