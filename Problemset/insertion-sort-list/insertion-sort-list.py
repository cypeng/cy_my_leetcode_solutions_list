
# @Title: 对链表进行插入排序 (Insertion Sort List)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-18 00:25:35
# @Runtime: 1940 ms
# @Memory: 16.6 MB

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
def insertElement(head):
    #print('head', head)
    pivot = head
    cur = None
    pre = None
    start = head
    while head:
        head = head.next
        pre = cur
        cur = head
        if cur and pre is None:
            if cur.val >= pivot.val:
                break
            if (cur.val < pivot.val) and (cur.next is None):
                start = cur
                pivot.next = cur.next
                cur.next = pivot
                break
        if (cur is None) and pre:
            if pre.val < pivot.val:
                start = start.next
                pre.next= pivot
                pivot.next = None
        if cur and pre:
            if (pre.val <= pivot.val) and (pivot.val < cur.val):
                start = start.next
                #print('start', start)
                pre.next = pivot
                pivot.next = cur
                #print('start', start)
                break
    #print('end', start)
    return start

def reverseElement(head):
    cur = None
    while head:
        if cur:
            cur.next = pre
            cur = insertElement(cur)
        pre = cur
        cur = head
        head = head.next
    if cur:
        cur.next = pre
        cur = insertElement(cur)
    return cur

class Solution:
    def insertionSortList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        tail = reverseElement(head)
        return tail
