
# @Title: 每日温度 (Daily Temperatures)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-04 15:00:29
# @Runtime: 1156 ms
# @Memory: 19.5 MB

class temperaturesStack:
    def __init__(self):
        self.stack = [None]*30000
        self.idx = -1
    
    def push(self, val):
        self.idx += 1
        self.stack[self.idx] = val
        #print(self.stack)

    def pop(self):
        if self.idx < 0:
            return None
        self.stack[self.idx] = None
        self.idx -= 1
    
    def getBack(self):
        if self.idx < 0:
            return None
        return self.stack[self.idx]

class Solution:
    def dailyTemperatures(self, temperatures: List[int]) -> List[int]:
        size = len(temperatures)
        idx = 0
        recordStack = temperaturesStack()
        for idx in range(0, size):
            while not recordStack.getBack() is None:
                if temperatures[idx] > recordStack.getBack()[0]:
                    temperatures[recordStack.getBack()[1]] = idx-recordStack.getBack()[1]
                    recordStack.pop()
                else:
                    break
            recordStack.push((temperatures[idx], idx))
            
        while not recordStack.getBack() is None:
            temperatures[recordStack.getBack()[1]] = 0
            recordStack.pop()
        return temperatures
