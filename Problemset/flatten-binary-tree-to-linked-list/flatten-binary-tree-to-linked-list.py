
# @Title: 二叉树展开为链表 (Flatten Binary Tree to Linked List)
# @Author: pcysl@hotmail.com
# @Date: 2021-08-06 21:41:56
# @Runtime: 32 ms
# @Memory: 14.9 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def traversal(ans, root):
    if root:
        ans.append(root)
        if root.left:
            traversal(ans, root.left)
        if root.right:
            traversal(ans, root.right)
class Solution:
    def flatten(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        if root is None:
            return root
        ans = []
        traversal(ans, root)
        this_ans, pre = None, None
        idx = 0
        while 1:
            if idx <= len(ans)-1:
                head = ans[idx]
                if this_ans is None:
                    this_ans = head
                head.left = None
                head.right = None
                if pre:
                    pre.right = head
                pre = head
            else:
                head = None
                break
            idx+= 1
        return this_ans
