
# @Title: Multiply Strings (Multiply Strings)
# @Author: pcysl
# @Date: 2018-11-30 12:58:38
# @Runtime: 56 ms
# @Memory: N/A

class Solution:
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        return str(int(num1)*int(num2))
        
