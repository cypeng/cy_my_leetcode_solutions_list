
# @Title: 克隆图 (Clone Graph)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-05 11:48:02
# @Runtime: 44 ms
# @Memory: 15.3 MB

"""
# Definition for a Node.
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []
"""

def createNode(LL, seens):
    if LL and LL.val in seens:
        return seens[LL.val], seens, True
    else:
        seens[LL.val] = Node(LL.val)
        return seens[LL.val], seens, False

def traversal(head, pre_root, ori_root, seens):
    if head is None:
        copy_root, seens, Flag = createNode(ori_root, seens)
        head = copy_root
        pre_root = head
    roots = ori_root.neighbors
    copy_roots = []
    for root in roots:
        copy_root, seens, Flag = createNode(root, seens)
        #print(copy_root)
        if not Flag:
            head, seens = traversal(head, copy_root, root, seens)
        copy_roots.append(copy_root)
        pre_root.neighbors = copy_roots
    return head, seens

class Solution:
    def cloneGraph(self, node: 'Node') -> 'Node':
        if node is None:
            return None
        head, seens = traversal(None, None, node, {})
        return head
            
