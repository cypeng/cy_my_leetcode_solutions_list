
# @Title: 将有序数组转换为二叉搜索树 (Convert Sorted Array to Binary Search Tree)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-10 21:33:15
# @Runtime: 60 ms
# @Memory: 16.2 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

def splitNums(nums, pre, head):
    size = len(nums)
    node_idx = int(size/2)
    left_tree_nums = nums[0:node_idx]
    node = nums[node_idx]
    right_tree_nums = nums[node_idx+1:]
    this_node = TreeNode(node)
    #print(left_tree_nums, node, right_tree_nums, head)
    if head is None:
        head = this_node
    else:
        if pre[1] == 0:
            pre[0].left = this_node
        else:
            pre[0].right = this_node
    if len(left_tree_nums) > 0:
        splitNums(left_tree_nums, (this_node, 0), head)
    if len(right_tree_nums) > 0:
        splitNums(right_tree_nums, (this_node, 1), head)
    
    return head

class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:
        head = None
        return splitNums(nums, head, head)
