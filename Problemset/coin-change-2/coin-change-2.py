
# @Title: 零钱兑换 II (Coin Change 2)
# @Author: pcysl@hotmail.com
# @Date: 2021-07-13 00:30:04
# @Runtime: 132 ms
# @Memory: 15.1 MB

class Solution:
    def change(self, amount: int, coins: List[int]) -> int:
        #
        #    0 1 2   3   4  5 
        #  1 1 1 1   1   1  1
        #  2 1 1 2   2   3  3
        #  5 1 1 2   2   3  4
        #
        #  4 = 1+1+1+1
        #  4 = 2+2
        #  4 = 2+1+1
        #  3 = 1+1+1
        #  3 = 2+1
        #
        # if j >= coin, dp[j] = max(dp[j],dp[j-coin]+1)
        dp = [0 for idx in range(0,amount+1)]
        dp[0] = 1
        for idx in range(0,len(coins)):
            for jdx in range(coins[idx],amount+1):
                dp[jdx] = dp[jdx]+dp[jdx-coins[idx]]
                #print(idx,jdx,dp)
        #print(dp)
        return dp[-1]
