
# @Title: Pascal's Triangle II (Pascal's Triangle II)
# @Author: pcysl
# @Date: 2018-12-07 12:34:34
# @Runtime: 36 ms
# @Memory: 7.3 MB

def get_Pascal(rowIndex):
    for idx in range(0, rowIndex+1):
        if (idx <= 1):
            sub_array = [1]*(idx+1)
        else:
            sub_array = [1]*(idx+1)
            for jdx in range(1, len(sub_array)-1):
                sub_array[jdx] = last_sub_array[jdx]+last_sub_array[jdx-1]
        last_sub_array = sub_array
    return sub_array

class Solution:
    def getRow(self, rowIndex):
        """
        :type rowIndex: int
        :rtype: List[int]
        """
        Pascal = get_Pascal(rowIndex)
        return Pascal
        
