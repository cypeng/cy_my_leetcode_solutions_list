
# @Title: 二叉树的前序遍历 (Binary Tree Preorder Traversal)
# @Author: pcysl@hotmail.com
# @Date: 2021-05-30 11:09:37
# @Runtime: 40 ms
# @Memory: 14.9 MB

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
def traversal(root, answer):
    if root:
        answer.append(root.val)
        if root.left:
            #answer.append(root.left.val) 
            traversal(root.left, answer)
        if root.right:
            #answer.append(root.right.val)
            traversal(root.right, answer)

class Solution:
    def preorderTraversal(self, root: TreeNode) -> List[int]:
        answer = []
        
        traversal(root, answer)
        return answer
