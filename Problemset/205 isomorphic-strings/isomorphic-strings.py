
# @Title: Isomorphic Strings (Isomorphic Strings)
# @Author: pcysl
# @Date: 2018-01-08 11:56:01
# @Runtime: 879 ms
# @Memory: N/A

class Solution(object):
    def isIsomorphic(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        if (len(s) == len(t)):
            return self.KeyNum(s,t)
        else:
            return bool(0)
    
    def KeyNum(self,s,t):
        Hash1 = {}
        Hash2 = {}
        KeyNum1 = 0
        KeyNum2 = 0
        count = 0
        multi = 1
        for idx in range(0,len(s)):
            cha1 = s[idx]
            cha2 = t[idx]
            if (not cha1 in Hash1) and (not cha2 in Hash2):
                Hash1[cha1] = count
                Hash2[cha2] = count
                KeyNum1 = KeyNum1 + count*multi
                KeyNum2 = KeyNum2 + count*multi
                count = count +1  
            elif (cha1 in Hash1) and (cha2 in Hash2):
                KeyNum1 = KeyNum1 + Hash1[cha1]*multi
                KeyNum2 = KeyNum2 + Hash2[cha2]*multi
            else:
                return bool(0)
            multi = multi*10
        
        if (KeyNum1 != KeyNum2):
            return bool(0)
    
        return bool(1)
        
