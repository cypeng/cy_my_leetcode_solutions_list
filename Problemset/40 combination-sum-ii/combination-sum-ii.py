
# @Title: Combination Sum II (Combination Sum II)
# @Author: pcysl
# @Date: 2018-07-23 10:23:02
# @Runtime: 84 ms
# @Memory: N/A

class Solution:
    def combinationSum2(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        ans = []
        ansArray = []
        thisCD = list(sorted(candidates))
        toFindSol(thisCD, target, 0, ansArray, ans)
        return ans

def toFindSol(candidates, target, iniIdx, ansArray, ans):  
    idx    = iniIdx
    dupDict= {}
    while (idx < len(candidates)):
        thisCD = list(candidates)
        thisAnsArray = list(ansArray)
        while ((thisCD[idx] == None) and (idx < len(thisCD)-1)):
            idx += 1
        if (not candidates[idx] == None):
            while ((thisCD[idx] in dupDict) and (idx < len(candidates)-1)):
                idx += 1
            if (not thisCD[idx] in dupDict):
                dupDict[thisCD[idx]] = 1
                thistarget = target - thisCD[idx]
                thisAnsArray.append(thisCD[idx])
                thisCD[idx] = None
                #print(target, candidates, thisAnsArray)
                if (thistarget == 0):
                    ans.append(list(thisAnsArray))
                else:
                    if ((idx < len(candidates)-1) and (thistarget >= candidates[idx])):
                        toFindSol(thisCD, thistarget, idx+1, list(thisAnsArray), ans)
        idx += 1
