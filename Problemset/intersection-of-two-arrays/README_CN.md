
| [English](README_EN.md) | 中文 |

# [349. 两个数组的交集](https://leetcode-cn.com/problems/intersection-of-two-arrays/)

## 題目描述

<p>给定两个数组，编写一个函数来计算它们的交集。</p>

<p>&nbsp;</p>

<p><strong>示例 1：</strong></p>

<pre><strong>输入：</strong>nums1 = [1,2,2,1], nums2 = [2,2]
<strong>输出：</strong>[2]
</pre>

<p><strong>示例 2：</strong></p>

<pre><strong>输入：</strong>nums1 = [4,9,5], nums2 = [9,4,9,8,4]
<strong>输出：</strong>[9,4]</pre>

<p>&nbsp;</p>

<p><strong>说明：</strong></p>

<ul>
	<li>输出结果中的每个元素一定是唯一的。</li>
	<li>我们可以不考虑输出结果的顺序。</li>
</ul>


## 相關題目

- [数组](https://leetcode-cn.com/tag/array)
- [哈希表](https://leetcode-cn.com/tag/hash-table)
- [双指针](https://leetcode-cn.com/tag/two-pointers)
- [二分查找](https://leetcode-cn.com/tag/binary-search)
- [排序](https://leetcode-cn.com/tag/sorting)

## 相似題目

- [两个数组的交集 II](../intersection-of-two-arrays-ii/README.md)
- [三个有序数组的交集](../intersection-of-three-sorted-arrays/README.md)
