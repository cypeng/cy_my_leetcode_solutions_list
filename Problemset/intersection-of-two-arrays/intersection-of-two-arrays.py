
# @Title: 两个数组的交集 (Intersection of Two Arrays)
# @Author: pcysl@hotmail.com
# @Date: 2021-06-19 11:52:49
# @Runtime: 412 ms
# @Memory: 14.9 MB

class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        ans = []
        size1, size2 = len(nums1), len(nums2)
        if size2 < size1:
            size1, size2 = size2, size1
            nums1, nums2 = nums2, nums1
        
        for idx, num in enumerate(nums1):
            if (idx > 0) and (nums1[idx] == nums1[idx-1]):
                continue
            idx2, jdx2 = 0, size2-1
            while idx2 <= jdx2:
                if ((nums1[idx] == nums2[idx2]) or (nums1[idx] == nums2[jdx2])):
                    if not nums1[idx] in ans:
                        ans.append(nums1[idx])
                    break
                idx2+=1
                while (idx2 +1 <= size2-1) and (nums2[idx2] == nums2[idx2-1]):
                    idx2+=1
                jdx2-=1
                while (jdx2 -1 >= 0) and (nums2[jdx2] == nums2[jdx2+1]):
                    jdx2 -= 1
        return ans

