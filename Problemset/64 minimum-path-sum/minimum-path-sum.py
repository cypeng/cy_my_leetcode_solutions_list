
# @Title: Minimum Path Sum (Minimum Path Sum)
# @Author: pcysl
# @Date: 2019-02-26 17:26:18
# @Runtime: 64 ms
# @Memory: 14.8 MB

def main(map):
    m = len(map)
    n = len(map[0])
    
    if (m == 0) or (n == 0):
        return 0
    if (m == 1) or (n == 1):
        sum = 0
        for idx in range(0, m):
            for jdx in range(0, n):
                sum += map[idx][jdx]
        return sum

    path_cost_map = []

    for idx in range(0, m):
        this_row = [0]*n
        path_cost_map.append(this_row)

    for idx in range(0, m):
        for jdx in range(0, n):
            if ((idx == 0) and (jdx == 0)):
                path_cost_map[idx][jdx] = map[idx][jdx]
            else:
                if ((idx > 0) and (jdx > 0)):
                    a = path_cost_map[idx-1][jdx] + map[idx][jdx]
                    b = path_cost_map[idx][jdx-1] + map[idx][jdx]
                    if (a < b):
                        path_cost_map[idx][jdx] = a
                    if (a >= b):
                        path_cost_map[idx][jdx] = b

                elif ((idx > 0) and (jdx == 0)):
                    path_cost_map[idx][jdx] = path_cost_map[idx-1][jdx] + map[idx][jdx]
                elif ((idx == 0) and (jdx > 0)):
                    path_cost_map[idx][jdx] = path_cost_map[idx][jdx-1] + map[idx][jdx]

    return path_cost_map[m-1][n-1]

class Solution:
    def minPathSum(self, grid: List[List[int]]) -> int:
        return main(grid)
        
